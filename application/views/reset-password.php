<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body class="tg-home tg-homeone">
    <!--Wrapper Start-->
    <div id="tg-wrapper" class="tg-wrapper tg-haslayout">       
        <main id="tg-main" class="tg-main tg-haslayout">
            <div class="tg-sectionspace tg-haslayout">
              <div class="container">
                <h2 align="center">Reset Password</h2>
                <div class="contact-form">
                  <form id="form" class="tg-formtheme tg-formcontactus" method="post" enctype="multipart/form-data" action="">
                    <?php $this->load->view('authority/common/messages');?>
                    <fieldset>
                      <div class="account_form">
                          <?php $this->load->view('include/messages');?> 
                          <div class="success_message"></div> 
                          <form id="reset-form" class="col-md-12 p-4" method="post" action="">
                              <p>   
                                  <label>New Password <span>*</span></label>
                                  <input type="password" id="n-password" name="password" class="form-control" placeholder="Type Your Password">
                              </p>
                              <p>   
                                  <label>Confirm Password <span>*</span></label>
                                  <input type="password" id="r-password" name="confirm_password" class="form-control" placeholder="Type Your Confirm Password">
                              </p>   
                          </form>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
        </main>
        
    </div>
<?php $this->load->view('include/footer_js');?>
<script>
    /*FORM VALIDATION*/
    $("#reset-form").validate({
            rules: {                
                'password': {required: true}, 
                'confirm_password': {required: true,equalTo: "#n-password"}, 
            },
            messages: {
                'password': "Please enter password",     
                'confirm_password': {required: 'Enter confirm password',equalTo: "Password and confirm password must be same"},
            }
        }); 
</script>
</body>
</html>
<?php

if (!function_exists('get_main_logo')) {
    function get_main_logo($type) {
        $CI = & get_instance();  
        $result = $CI->db->select("profile_photo,white_logo")
            ->from('administrator')
            ->get()->row_array();
        if (!empty($result)) {
            $result = (isset($type) && $type=='d')?$result['profile_photo']:$result['white_logo'];
            return $result;
        } else {
            return '';
        }
    }
}
if (!function_exists('get_social_links')) {
	function get_social_links() {
	    $CI = & get_instance();
	    $conditions = array();
	    $info = $CI->common_model->select_data("social_links", $conditions);
	    if ($info['row_count'] > 0) {
	        return $info['data'][0];
	    } else {
	        return array();
	    }
	}
}

function get_main_address() {
    $CI = & get_instance();  
    $result = $CI->db->select("address")
        ->from('administrator')
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['address'];
        return $result;
    } else {
        return '';
    }
}

function get_main_phone_no1() {
    $CI = & get_instance();  
    $result = $CI->db->select("mobile_number_1")
        ->from('administrator')
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['mobile_number_1'];
        return $result;
    } else {
        return '';
    }
}
function get_main_phone_no2() {
    $CI = & get_instance();  
    $result = $CI->db->select("mobile_number_2")
        ->from('administrator')
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['mobile_number_2'];
        return $result;
    } else {
        return '';
    }
}
function get_main_email_address() {
    $CI = & get_instance();  
    $result = $CI->db->select("email_address")
        ->from('administrator')
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['email_address'];
        return $result;
    } else {
        return '';
    }
}
if (!function_exists('get_statistics')) {
    function get_statistics() {
        $CI = & get_instance();
        $conditions = array();
        $info = $CI->common_model->select_data('statistics', $conditions);
        // echo "<pre>";print_r($info);exit;
        if ($info['row_count'] > 0) {
            return $info['data'][0];
        } else {
            return '';
        }
    }
}
if (!function_exists('get_count')) {
    function get_count($table) {
        $CI = & get_instance();
        $conditions = array();
        $info = $CI->common_model->select_data($table, $conditions);
        // echo "<pre>";print_r($info);exit;
        if ($info['row_count'] > 0) {
            return $info['row_count'];
        } else {
            return 0;
        }
    }
}
if (!function_exists('getYoutubeKey')) {
    function getYoutubeKey($url){
        $url = str_replace('https://www.youtube.com/watch?v=', '', $url);
        $url = str_replace('https://youtu.be/', '', $url);
        $url = str_replace('https://www.youtube.com/embed/', '', $url);
        // preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
        return $url;
    }
}
if (!function_exists('get_quote_type')) {
    function get_quote_type() {        
        $info = array('home/renter/condo' =>'Home / Renter / Condo','auto' =>'Auto' );
        // echo "<pre>";print_r($info);exit;
        if ($info > 0) {
            return $info;
        } else {
            return 0;
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Static_page extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        $data = array();  
        $tmp_data = static_page();
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/static-page/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        $data['details'] = $this->production_model->get_all_with_where_limit('static_page','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        $this->load->view('authority/static_page/view',$data);
    }
    function add()
    {
        $data['details'] = array();
        $this->load->view('authority/static_page/add-edit',$data);
    }
    /*function add_page()
    {
        $data = $this->input->post();
        $this->form_validation->set_rules('title', 'title', 'required', array('required' => 'Please enter title'));
        $this->form_validation->set_rules('description', 'description', 'required|strip_tags', array('required' => 'Please enter description'));
        if ($this->form_validation->run() == FALSE)
        {
            $data['details'] = array();
            $this->load->view('authority/static_page/add-edit',$data);
        }
        else
        {
            if ($_FILES['image']['name'] !='') {
                $data['image'] = $this->production_model->image_upload(STATIC_PAGE_IMAGE,'image');
            }
            $record = $this->production_model->insert_record('static_page',$data);
            if ($record !='') {
                $this->session->set_flashdata('success', 'Added successful.');
                redirect(base_url('authority/static-page')); 
            }
            else
            {
                $this->session->set_flashdata('error', 'Not added.');
                redirect($_SERVER['HTTP_REFERER']);
            }   
        }
    }
    function edit($id)
    {
        $data = $this->input->post();
        $condition = array('id'=>$id);
        $data['details'] = static_page($condition);

        if ($this->input->method() == 'post') {
            $this->form_validation->set_rules('title', 'title', 'required', array('required' => 'Please enter title'));
            $this->form_validation->set_rules('description', 'description', 'required|strip_tags', array('required' => 'Please enter description'));
            if ($this->form_validation->run() == FALSE)
            {
                $tmp = $data['details'];
                $data['details'] = array_merge($tmp,$this->form_validation->error_array());
            }
            else
            {
                if ($_FILES['image']['name'] !='') {
                    $data['image'] = $this->production_model->image_upload(STATIC_PAGE_IMAGE,'image','static_page',$id);
                }               
                unset($data['details']);
                $record = $this->production_model->update_record('static_page',$data,array('id'=>$id));
                if ($record == 1) {
                    $this->session->set_flashdata('success', 'Updated successful.');
                    redirect(base_url('authority/static-page'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Not updated.');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
        $this->load->view('authority/static_page/add-edit',$data);
    }*/
    function add_edit($id='')
    {   
        $data = $this->input->post();

        $data['details'] = array();
        if($id != ""){
            $condition = array('id'=>$id);
            $data['details'] = static_page($condition);
        }

        if($this->input->post()){
            $this->validate($id);
            if ($this->form_validation->run() == FALSE)
            {
                $data['details'] = array();
                $this->load->view('authority/static_page/add-edit',$data);
            }
            else
            {
                if($id == ""){
                    unset($data['details']);
                    if ($_FILES['image']['name'] !='') {
                        $data['image'] = $this->production_model->image_upload(STATIC_PAGE_IMAGE,'image');
                    }
                    $data['seo_slug']=create_slug($data['title']); 
                    $record = $this->production_model->insert_record('static_page',$data);
                    if ($record !='') {
                        $this->session->set_flashdata('success', 'Added successful.');
                        redirect(base_url('authority/static-page')); 
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Not added.');
                        redirect($_SERVER['HTTP_REFERER']);
                    }  
                }else if($id != ""){
                    if ($_FILES['image']['name'] !='') {
                        $data['image'] = $this->production_model->image_upload(STATIC_PAGE_IMAGE,'image','static_page',$id);
                    }               
                    unset($data['details']);
                    $data['seo_slug']=create_slug($data['title']); 
                    $record = $this->production_model->update_record('static_page',$data,array('id'=>$id));
                    if ($record == 1) {
                        $this->session->set_flashdata('success', 'Updated successful.');
                        redirect(base_url('authority/static-page'));
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Not updated.');
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            }
        }
        $this->load->view('authority/static_page/add-edit',$data);
    }
    function validate(){
        $this->form_validation->set_rules('title', 'title', 'required', array('required' => 'Please enter title'));
            $this->form_validation->set_rules('description', 'description', 'required|strip_tags', array('required' => 'Please enter description'));
    }
    function file_selected_test(){
        $this->form_validation->set_message('file_selected_test', 'Image field is required');
        if (empty($_FILES['image']['name'])) {
            return false;
        }else{
            return true;
        }
    }
    
}
?>
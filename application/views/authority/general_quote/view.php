<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">General Quote</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        	<?php $this->load->view('authority/common/messages')?>
        	
            <div class="row">
	        	<div class="col-md-6">
	        		<?php
						if (isset($details) && $details !=null) {
						?>
	        				<button type="button" class="btn btn-danger btn-flat delete_all">Delete all</button>
	        			<?php } 
	        		?>
	        	</div>
	        	<div class="col-md-6">
	        		<?php                            
                        $name = isset($this->session->general_quote_info['name']) && $this->session->general_quote_info['name'] !=null ? $this->session->general_quote_info['name'] : '';
                    ?>
	        		<div class="input-group input-group-sm">
				        <input class="form-control form-control-navbar" type="search" name="name" value="<?= $name;?>" placeholder="Search" aria-label="Search">
				        <div class="input-group-append">
				          	<button class="btn btn-primary btn_filter" type="button"><i class="fas fa-search"></i></button>
				        </div>&nbsp;&nbsp;
		        		<a href="<?= base_url('authority/general-quote?clear-search=1')?>" class="btn btn-primary btn-sm" type="button">Clear</a>
				    </div>
	        	</div>
                <div class="col-md-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    	<th style="width: 10px;">
											<div class="custom-control custom-checkbox">
					                          	<input class="custom-control-input" type="checkbox" id="select_all">
					                          	<label for="select_all" class="custom-control-label"></label>
                        					</div>
										</th>
                                        <th style="width: 10px">No</th>
                                        <th style="width: 20%;">Name</th>
                                        <th style="width: 20%;">Email</th>
                                        <th style="width: 15%;">Phone Number</th>
                                        <th style="width: 10%;">Quote Type</th>
                                        <th style="width: 25%;">Selected Item</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                	<?php
										if (isset($details) && $details !=null){
		                                    foreach ($details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
			                                    <tr>
			                                    	<td style="width: 10px;">
			                                    		<div class="custom-control custom-checkbox">
								                          	<input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id;?>" value="<?= $id?>">
								                          	<label for="customCheckbox<?= $id;?>" class="custom-control-label"></label>
								                        </div>
													</td>
							                        <td><?= $key+1;?></td>
							                        <td><?= $value['first_name']." ".$value['last_name'];?></td>
							                        <td><?= $value['email'];?></td>
							                        <td><?= $value['mobile_number'];?></td>
							                        <td><?= ucfirst($value['quote_type']);?></td>
							                        <td><?= $value['selected_item'];?></td>
							                        <td>
							                        	<a href="<?= base_url('authority/general-quote/details/'.$id);?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-eye"></i></a>												
														<a href="javascript:void(0)" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id;?>"><i class="fa fa-trash-o"></i></a>
							                        </td>
							                    </tr>
			                                <?php } 
			                            }
			                            else{
			                            	?>
			                            		<tr data-expanded="true">
													<td colspan="7" align="center">Records not found</td>
												</tr>
			                            	<?php
			                            }
			                        ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix pagination_filter">
                        	<?php
                                if(isset($pagination) && $pagination !=null){
			                        echo $pagination;    
			                    }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>

<script type="text/javascript">
	$(document).ready(function () {	
		$(document).on('click','.delete_record',function(e){ 
			var id = $(this).attr('id');
			var post_data = {'id':id};
			var current = $(this).closest('tr');
			$.confirm({
			    title: 'Confirm',
			    type:'red',
                content: 'Are you sure you want to delete record?',
			    buttons: {
			        confirm: function () {
			           	$.ajax({
				            url: '<?= base_url('authority/general_quote/delete/')?>',
				            type: 'POST',
				            dataType: 'json',
				            data: post_data,
				            beforeSend: function() {
				                $('.loading').css({'display':'block'});
				            },
				            success: function(response){
				                if (response.success){
				                	current.remove();
					                toastr.success(response.message);
				                }
				            }, 
				            complete: function() {
				                setTimeout(function() {
				                    $('.loading').css({'display':'none'});
				                }, 1000);
				            },
				        });
			        },
			        cancel: function () {				            
			        },
			    }
			});
		});

		// multiple delete //
	    $('.delete_all').on('click', function() {
	    	var id = [];	    	
	    	$(".chk_all:checked").each(function() { 
	            id.push($(this).val());
	        });
			var post_data = {'chk_multi_checkbox':id};
			var boxes = $('.chk_all:checkbox');
	        if(boxes.length > 0) {
	            if($('.chk_all:checkbox:checked').length < 1) {
	                $.alert({
				        title: 'Opps!',
				        type: 'red',
	                    content: 'Please select at least one checkbox',
				    });
	                return false;
	            }
	            else{
	        		$.confirm({
					    title: 'Confirm',
					    type: 'red',
	                    content: 'Are you sure you want to delete record?',
					    buttons: {
					        confirm: function () {
					           	$.ajax({
						            url: '<?= base_url('authority/general_quote/multiple_delete/')?>',
						            type: 'POST',
						            dataType: 'json',
						            data: post_data,
						            beforeSend: function() {
						                $('.loading').css({'display':'block'});
						            },
						            success: function(response){
						                if (response.success){
						                	$(".chk_all:checked").each(function() { 
									            id.push($(this).closest('tr').remove()); 
									        });
							                toastr.success(response.message);
						                }
						            }, 
						            complete: function() {
						                setTimeout(function() {
						                    $('.loading').css({'display':'none'});
						                }, 1000);
						            },
						        });
					        },
					        cancel: function () {				            
					        },
					    }
					});
	                return false;
	            }
	        }
		});

		/*Pagination filter*/
        $(document).on('click','.pagination_filter a',function(e){            
            var url = $(this).attr('href');
            var split_url = url.split('/');
            var page_no = split_url[7];
            e.preventDefault();            
            get_filtered_data(page_no);
        });

        $(document).on('click','.btn_filter',function(){
        	var name = $('input[name="name"]').val();
        	get_filtered_data('',name);
        });
	});

	function get_filtered_data(page_no='',name=''){     
        var post_data = {'page_no':page_no,'name':name};
        $.ajax({
            url: '<?= base_url('authority/general_quote/filter/')?>'+page_no,
            type: 'POST',
            dataType: 'json',
            data: post_data,
            beforeSend: function() {
                $('.loading').css({'display':'block'});
            },
            success: function(response){
                if (response.success){   
                    $('.data-response').html(response.details);
                    $('.pagination_filter').html(response.pagination);
                }
                else if (response.error && response.data_error){
                    $('.data-response').html(response.data_error);
                    $('.pagination_filter').html(response.pagination);
                }
            }, 
            complete: function() {
                setTimeout(function() {
                    $('.loading').css({'display':'none'});
                }, 1000);
            },
        });
    }
</script>
<?php $this->view('authority/common/footer'); ?>									
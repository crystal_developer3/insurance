<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Quote</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($first_name) && $first_name !=null){ ?>
                                                                <tr>
                                                                    <td><b>First Name:</b></td>
                                                                    <td><?= $first_name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($last_name) && $last_name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Last Name:</b></td>
                                                                    <td><?= $last_name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($email) && $email !=null){ ?>
                                                                <tr>
                                                                    <td><b>Email Address:</b></td>
                                                                    <td><?=$email?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($phone_number) && $phone_number !=null){ ?>
                                                                <tr>
                                                                    <td><b>Mobile Number:</b></td>
                                                                    <td><?=$phone_number?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($address) && $address !=null){ ?>
                                                                <tr>
                                                                    <td><b>Address:</b></td>
                                                                    <td><?= isset($address) && $address !=null ? $address : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($preferred_language) && $preferred_language !=null){
                                                                $preferred_language = get_language_titles($preferred_language);               
                                                                ?>
                                                                <tr>
                                                                    <td><b>Preferred language:</b></td>
                                                                    <td><?= $preferred_language?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        
                                                        <?php
                                                            if(isset($business_name) && $business_name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Business name:</b></td>
                                                                    <td><?= $business_name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($business_address) && $business_address !=null){ ?>
                                                                <tr>
                                                                    <td><b>Business address:</b></td>
                                                                    <td><?= $business_address?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($message) && $message !=null){ ?>
                                                                <tr>
                                                                    <td><b>Message:</b></td>
                                                                    <td><?= $message?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($primary_work) && $primary_work !=null){
                                                                             
                                                                ?>
                                                                <tr>
                                                                    <td><b>Primary work:</b></td>
                                                                    <td><?= $primary_work?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($business_structure_id) && $business_structure_id !=null){
                                                                $business_structure = business_structure_title($business_structure_id);               
                                                                ?>
                                                                <tr>
                                                                    <td><b>Business Structure:</b></td>
                                                                    <td><?= $business_structure?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($created_at) && $created_at !=null){ ?>
                                                                <tr>
                                                                    <td><b>Contact date:</b></td>
                                                                    <td><?= format_date_Mdy_time($created_at)?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>
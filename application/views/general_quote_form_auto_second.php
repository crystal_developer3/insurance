<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>
<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Generel Quote Form Auto</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Generel Quote Form Auto</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->
    <section class="contact-area ptb-100">
            <div class="container">
                <div class="contact-form2">
                    <form id="form" action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <?php $this->load->view('include/messages');?>     
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">What’s your Name? <span class="required">*</span></label>
                                    <input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" placeholder="What’s your Name?">
                                    
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Email <span class="required">*</span></label>
                                    <input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" placeholder="Email">
                                    
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group pl-2">
                                    <label class="auto-insurance">Do you currently have auto insurance?</label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="have_auto_insurance" name="have_auto_insurance" class="custom-control-input" value="1" checked="">
                                        <label class="custom-control-label" for="have_auto_insurance">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="have_auto_insurance2" name="have_auto_insurance" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="have_auto_insurance2">No</label>
                                    </div>
                                </div>
                                <div class="file-document-upload file-upload">
                                    <div class="form-group">
                                        <label class="upload-document" for="f02">Upload image/document</label>
                                        <input id="f02" type="file" name="image" placeholder="Upload image/document" />
                                    </div>
                                </div>
                                <div class="green box">
                                    <label style="width: 100%;">(regardless of fault) in the last 5 years? <span class="required">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="customRadio1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="customRadio2">No</label>
                                    </div>
                                </div>
                                <!-- New added -->
                                <div class="green box insurance-box">
                                    <label style="width: 100%;">Accidents (regardless of fault) in the last 5 years? <span class="required">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="accidents_in_last_five_years1" name="accidents_in_last_five_years" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="accidents_in_last_five_years1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="accidents_in_last_five_years2" name="accidents_in_last_five_years" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="accidents_in_last_five_years2">No</label>
                                    </div>
                                </div>
                                <div class="green box insurance-box">
                                    <label style="width: 100%;">Accidents (regardless of fault) by anyone who was not listed on your insurance policy in the last 5 years?  <span class="required">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="accidents_by_anyone_who_was_not_listed1" name="accidents_by_anyone_who_was_not_listed" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="accidents_by_anyone_who_was_not_listed1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="accidents_by_anyone_who_was_not_listed2" name="accidents_by_anyone_who_was_not_listed" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="accidents_by_anyone_who_was_not_listed2">No</label>
                                    </div>
                                </div>
                                <div class="green box insurance-box">
                                    <label style="width: 100%;">Traffic Tickets in the last 5 years?  <span class="required">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="traffic_tickets_in_five_years1" name="traffic_tickets_in_five_years" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="traffic_tickets_in_five_years1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="traffic_tickets_in_five_years2" name="traffic_tickets_in_five_years" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="traffic_tickets_in_five_years2">No</label>
                                    </div>
                                </div>
                                <div class="green box insurance-box">
                                    <label style="width: 100%;">DUIs in the last 10 years?  <span class="required">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="duis_in_ten_years1" name="duis_in_ten_years" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="duis_in_ten_years1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="duis_in_ten_years2" name="duis_in_ten_years" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="duis_in_ten_years2">No</label>
                                    </div>
                                </div>
                                <div class="green box insurance-box">
                                    <label style="width: 100%;">Suspensions or Revocations in the last 10 years?  <span class="required">*</span></label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="suspensions_or_revocations_in_ten_years1" name="suspensions_or_revocations_in_ten_years" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="suspensions_or_revocations_in_ten_years1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="suspensions_or_revocations_in_ten_years2" name="suspensions_or_revocations_in_ten_years" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="suspensions_or_revocations_in_ten_years2">No</label>
                                    </div>
                                </div>

                                <div class="form-group pl-2 row">
                                    <label class="auto-insurance">Have any drivers had any of the following?  </label>
                                    
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="hav_any_of_the_following1" name="hav_any_of_the_following" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="hav_any_of_the_following1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="hav_any_of_the_following2" name="hav_any_of_the_following" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="hav_any_of_the_following2">No</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group pl-2">
                                    <button class="default-btn">Submit <span></span></button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    
                </div>
            </div>
            
            <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
        </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script>        

        var id = [];           
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {

                'name':{required: true},
                'email': {required: true,email:true},
                
            },
            messages: {

                'name':{required:"Please enter name"},
                'email': {required:"Please enter email",email:"Please enter valid email"},
                
                
            }
        }); 
        $(document).on('click','.preferred_language',function(e){
            $(".preferred_language:checked").each(function() { 
                  id.push($(this).val());
            });
            console.log(id);
        });
        var form = $( "#form" );
        form.validate();
        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });  
        $(document).on('click', 'input[name="have_auto_insurance"]', function(event) {
            var name = $('input[name="have_auto_insurance"]:checked').val();
            if (name == 1){
                $('.file-upload').css({'display':'block'});
                $('.insurance-box').css({'display':'none'});
            }
            if (name == 0){
                $('.file-upload').css({'display':'none'});
                $('.insurance-box').css({'display':'block'});
            }
        });
        $("[type=file]").on("change", function(){
          var file = this.files[0].name;
          var dflt = $(this).attr("placeholder");
          if($(this).val()!=""){
            $(this).next().text(file);
          } else {
            $(this).next().text(dflt);
          }
        });
    </script>
    
</body>
</html>
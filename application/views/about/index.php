<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>About Us</h2>
                        <ul>
                            <li><a href="<?=base_url()?>l">Home</a></li>
                            <li>About Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="about-area ptb-100 bg-f8f8f8">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="about-image">
                        <?php
                            if(isset($details) && !empty($details)){ ?>
                                <img src="<?= base_url(ABOUT_IMAGE).$details[0]['back_image']?>" alt="image" >
                                <img src="<?= base_url(ABOUT_IMAGE).$details[0]['image']?>" alt="image" >
                                <?php
                            }
                            ?>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="about-content">
                        <h2><?=(isset($details) && !empty($details))?$details[0]['title']:''?></h2>
                        <?=(isset($details) && !empty($details))?$details[0]['description']:''?>
                    </div>
                </div>
            </div>

            <div class="about-inner-area">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 ml-12">
                        <div class="about-text-box">
                            <a href="<?=base_url('history')?>">
                                <h3><?=(isset($history_details) && !empty($history_details))?$history_details[0]['title']:''?></h3>
                                <p>
                                    <?php
                                        if(isset($history_details) && !empty($history_details)){
                                          $content = strip_tags($history_details[0]['description']);
                                          echo strlen($content) > 150 ? substr($content, 0, 150) . "..." : $content;
                                        }
                                    ?>
                                </p>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3 offset-sm-3 col-sm-6">
                        <div class="about-text-box">
                            <a href="<?=base_url('who-we-are')?>">
                                <h3><?=(isset($who_we_are_details) && !empty($who_we_are_details))?$who_we_are_details[0]['title']:''?></h3>
                                <p>
                                    <?php
                                        if(isset($who_we_are_details) && !empty($who_we_are_details)){
                                          $content = strip_tags($who_we_are_details[0]['description']);
                                          echo strlen($content) > 150 ? substr($content, 0, 150) . "..." : $content;
                                        }
                                    ?>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>
<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Static page</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    //$action = ($details == null) ? base_url('authority/static_page/add_page') : '';
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open('', $attributes);
                                ?>
                                    <input type="hidden" name="id" id="id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-12 col-form-label">Title <span class="required">*</span></label>
                                        <div class="col-sm-12">
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['title']) && $details[0]['title'] !=null ? $details[0]['title'] : '';?>">
                                            <span id="title_error"></span>
                                            <?= form_error("title", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName3" class="col-sm-12 col-form-label">Description <span class="required">*</span></label>
                                        <div class="col-sm-12">
                                            <textarea class="textarea" name="description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= ($details != null) ? $details[0]['description'] : '';?></textarea>
                                            <?= form_error('description', "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    
                                    <!-- <?php
                                        if (isset($details) && $details !=null && ($details[0]['id'] == 3 || $details[0]['id'] == 5 || $details[0]['id'] == 6)) {
                                        ?>
                                            <div class="form-group row">
                                                <label for="profile_photo" class="col-sm-3 col-form-label">Image </label>
                                                <div class="col-sm-9">
                                                    <input type="file" name="image" class="form-control" accept="image/*">
                                                    <span id="image_error"></span>
                                                    <?= form_error("image", "<label class='error'>", "</label>");?>
                                                </div>
                                            </div>
                                            <?php
                                                if (isset($details) && $details != null) {
                                                ?>
                                                    <div class="form-group row">
                                                        <label for="inputName3" class="col-sm-3 col-form-label">Current image</label>
                                                        <div class="col-sm-9">
                                                            <img src="<?= base_url(STATIC_PAGE_IMAGE.'thumbnail/').$details[0]['image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">            
                                                        </div>
                                                    </div>
                                                <?php } 
                                            ?>
                                        <?php } 
                                    ?> -->
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        $('.textarea').summernote()
        var id = $("#id").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            ignore: [],
            rules: {
                title: {required:true}, 
                description: {required:true},  
                title_hindi: {required:true}, 
                description_hindi: {required:true},
            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                if (name == 'description'){
                    error.insertAfter($('.note-editor'));
                } else {
                    error.appendTo($("#" + name + "_error"));
                }
            },
            messages: {     
                title: {required :"Please enter title"}, 
                description:{required:"Please enter description"},  
                title_hindi: {required :"Please enter title"}, 
                description_hindi:{required:"Please enter description"}, 
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Statistics</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        	<?php $this->load->view('authority/common/messages')?>
        	
            <div class="row">
	        	<div class="col-md-6">
	        		<?php
						if ($details ==null) {
						?>
	        				<a href="<?= base_url('authority/statistics/add');?>" class="btn btn-secondary btn-flat">Add</a>
	        			<?php } 
	        		?>
	        	</div>
                <div class="col-md-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th>Statistics</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                	<?php
										if (isset($details) && $details !=null){
		                                    foreach ($details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
			                                    <tr>
							                        
							                        <td>
                                                        <table class="table table-bordered">
						                        	     <tr>
                                                             <td><b>Books we have</b> : </td>
                                                             <td><?= $value['books_we_have'];?></td>
                                                        </tr>
                                                        <tr>
                                                             <td><b>Total members</b> : </td>
                                                             <td><?= $value['total_members'];?></td>
                                                         </tr>

                                                        <tr>
                                                             <td><b>Happy users</b> : </td>
                                                             <td><?= $value['happy_users'];?></td>
                                                         </tr>
                                                    
														</table>
							                        </td>
							                        <td align="center" >
										                <a href="<?= base_url('authority/statistics/edit/'.$id);?>" class="btn bg-gradient-primary btn-flat btn-sm"><i class="fas fa-edit"></i></a>
							                        </td>
							                    </tr>
			                                <?php } 
			                            }
			                            else{
			                            	?>
			                            		<tr data-expanded="true">
													<td colspan="7" align="center">Records not found</td>
												</tr>
			                            	<?php
			                            }
			                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>									
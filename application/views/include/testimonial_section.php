<!-- Testimonials -->
  <section class="tg-parallax tg-bgtestimonials tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="<?= base_url()?>assets/images/parallax/bgparallax-05.jpg">
    <div class="tg-sectionspace tg-haslayout">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2">
            <div id="tg-testimonialsslider" class="tg-testimonialsslider tg-testimonials owl-carousel">
              <?php
                if (isset($testimonial_details) && $testimonial_details !=null) {
                    foreach ($testimonial_details as $key => $value) { ?>    
                      <div class="item tg-testimonial">
                        <blockquote><q><?= $value['description'];?></q></blockquote>
                        <div class="tg-testimonialauthor">
                          <h3><?= $value['name'];?></h3>
                        </div>
                      </div>
                      <?php
                    }
                  }
              ?>
            </div>
            <div class="tg-btns mt-5">
              <a href="#" class="tg-btn tg-active" data-toggle="modal" data-target="#exampleModal">Write Testimonial</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Testimonials --> 
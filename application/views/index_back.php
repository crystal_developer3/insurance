<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>
<section id="banner">
    <div class="container-fluid">
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key => $value) {
                        ?>
                            <li data-target="#myCarousel" data-slide-to="<?= $key;?>" class="<?= $key==0 ? 'active' : '';?>"></li>
                        <?php } 
                    }
                ?>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key => $value) {
                        ?>  
                            <div class="item <?= $key==0 ? 'active' : '';?>">
                                <img src="<?= base_url(HOME_SLIDER.$value['image']);?>" alt="Chania">
                                <div class="carousel-caption">
                                    <h3><?= $value['title'];?></h3>
                                </div>
                            </div>
                        <?php } 
                    }
                ?>
            </div>
            <!-- /.carousel-inner -->
            <!-- Controls -->
            <div class="control-box">
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="control-icon prev fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="control-icon next fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.control-box -->
        </div>
    </div>
</section>
</div>
<div class="clearfix"></div>
<div class="tr2">
    <?php
        if (isset($about_details) && $about_details !=null) {
        ?>
            <div class="col-sm-7" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="treat-inr">
                    <h3><?= $about_details[0]['title'.get_language()];?></h3>
                    <?= $about_details[0]['description'.get_language()];?>
                </div>
            </div>
            <div class="col-sm-5 p00" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <img src="<?= base_url(ABOUT_IMAGE.$about_details[0]['image']);?>" class="img-responsive">
            </div>
        <?php }
    ?>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="tr1">
    <?php
        if (isset($treatments_details) && $treatments_details !=null) {
        ?>
            <div class="col-sm-5 p00" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <img src="<?= base_url(TREATMENTS.$treatments_details[0]['image']);?>" class="img-responsive">
            </div>
            <div class="col-sm-7" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="treat-inr">            
                    <h3><?= $treatments_details[0]['title'.get_language()];?></h3>
                    <?= $treatments_details[0]['description'.get_language()];?>
                    <div><a href="<?= base_url('treatments');?>" class="read-more"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Read More..</a></div>
                </div>
            </div>
        <?php } 
    ?>
</div>
<div class="clearfix"></div>
<div class="tr2">
    <?php
        if (isset($accommodation_details) && $accommodation_details !=null) {
        ?>
            <div class="col-sm-7" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="treat-inr">                    
                    <h3><?= $accommodation_details[0]['title'.get_language()];?></h3>
                    <?= $accommodation_details[0]['description'.get_language()];?>
                    <div><a href="<?= base_url('accommodation');?>" class="read-more"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Read More..</a></div>
                </div>
            </div>
            <div class="col-sm-5 p00" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <img src="<?= base_url(ACCOMMODATION_IMAGE.$accommodation_details[0]['image']);?>" class="img-responsive">
            </div>
        <?php } 
    ?>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="tr1">
    <?php
        if (isset($service_details) && $service_details !=null) {
        ?>
            <div class="col-sm-5 p00" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <img src="<?= base_url(SERVICE_IMAGE.$service_details[0]['image']);?>" class="img-responsive">
            </div>
            <div class="col-sm-7" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="treat-inr">
                    <h3><?= $service_details[0]['title'.get_language()];?></h3>
                    <?= $service_details[0]['description'.get_language()];?>
                    <!-- <div><a href="#" class="read-more"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Read More..</a></div> -->
                </div>
            </div>
        <?php } 
    ?>
</div>
<div class="clearfix"></div>
<div class="tr2">
<div class="col-sm-7" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
    <div class="treat-inr">
        <h3>Testimonials</h3>
        <div class="col-sm-12">
            <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <?php
                        if (isset($testimonial_details) && $testimonial_details !=null) {
                            foreach ($testimonial_details as $key => $value) {
                            ?>
                                <li data-target="#myCarousel1" data-slide-to="<?= $key;?>" class="<?= $key == 0 ? 'active' : '';?>"></li>
                            <?php } 
                        } 
                    ?>
                </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <?php
                        if (isset($testimonial_details) && $testimonial_details !=null) {
                            foreach ($testimonial_details as $key => $value) {
                            ?>
                                <div class="item carousel-item <?= $key == 0 ? 'active' : '';?>">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="media">
                                                <div class="media-left d-flex mr-3">
                                                    <img src="<?= base_url(TESTIMONIAL.$value['image']);?>" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="overview">
                                                        <div class="name"><b><?= $value['name'.get_language()];?></b></div>
                                                        <div class="details"><?= $value['designation'.get_language()];?></div>
                                                        <div class="star-rating">
                                                            <ul class="list-inline">
                                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="testimonial">
                                                <?= $value['description'.get_language()];?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                        }
                    ?>
                    <!-- Carousel controls -->
                    <a class="carousel-control1 left carousel-control-prev" href="#myCarousel1" data-slide="prev">
                    <img src="<?= base_url()?>assets/images/l1.png">
                    </a>
                    <a class="carousel-control1 right carousel-control-next" href="#myCarousel1" data-slide="next">
                    <img src="<?= base_url()?>assets/images/r1.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-5 treat4" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
    <div class="video1" align="center">
        <<a href="#"><img src="<?= base_url()?>assets/images/video.png"></a>
        <div class="vert2" align="center">The Vaidic</div>
        <h3 align="center" class="verti1">Virtual Tour</h3>
    </div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>
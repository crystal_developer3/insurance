<!-- <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php
      if (isset($slider_details) && $slider_details !=null) {
          foreach ($slider_details as $key => $value) { ?> 
            <div class="item <?=($key==0)?'active':''?>">
              <img src="<?= base_url(HOME_SLIDER.$value['image']);?>" alt="Los Angeles" style="width:100%;">
            </div>
            <?php
          }
        }
    ?>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div> -->

<div class="home-area home-slides owl-carousel owl-theme">
    <?php
    // echo "<pre>";print_r($slider_details);exit;
      if (isset($slider_details) && $slider_details !=null) {
          foreach ($slider_details as $key => $value) { ?> 
            <div class="main-banner item-bg1" style="background-image:url(<?=base_url(HOME_SLIDER.$value['image'])?>)">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="main-banner-content">
                                <span class="sub-title"><?=$value['title']?></span>
                                <h1><?=$value['title2']?></h1>
                                <p><?=$value['description']?></p>

                                <div class="btn-box">
                                    <a href="#" class="default-btn">Get A Quote <span></span></a>
                                    <a href="#" class="optional-btn">Contact Us <span></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
          }
        }
    ?>

    <!-- <div class="main-banner item-bg2">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="main-banner-content">
                        <span class="sub-title">Enjoy Your Happiness</span>
                        <h1>Travelling! how<br> thrilling it can be</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>

                        <div class="btn-box">
                            <a href="#" class="default-btn">Get A Quote <span></span></a>
                            <a href="#" class="optional-btn">Contact Us <span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-banner item-bg5">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="main-banner-content">
                        <span class="sub-title">Enjoy Your Happiness</span>
                        <h1>So you're thinking of moving, enjoy the effect</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>

                        <div class="btn-box">
                            <a href="#" class="default-btn">Get A Quote <span></span></a>
                            <a href="#" class="optional-btn">Contact Us <span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('is_valid_mobile')) {

    function is_valid_mobile($mobile = '') {
        $return = false;
        if (strlen(trim($mobile)) >= 10 && strlen(trim($mobile)) < 14) {
            $return = true;
        }
        return $return;
    }

}

if (!function_exists('clear_input')) {

    function clear_input($value) {
        return addslashes($value);
    }

}
if (!function_exists('clear_output')) {

    function clear_output($value) {
        return stripslashes($value);
    }

}
if (!function_exists('format_date_dmy')) {

    function format_date_dmy($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("d M Y", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}
if (!function_exists('format_date_mdy')) {

    function format_date_mdy($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("M d, Y", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}
if (!function_exists('format_date_ymd')) {

    function format_date_ymd($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("Y-m-d", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}

if (!function_exists('blank_errorcheck')) {

    function blank_errorcheck($fieldname) {
        $errMessage = array();
        foreach ($fieldname as $key => $message) {
            if (strlen(trim(@$_POST[$key])) <= 0) {
                $errMessage[$key] = $message;
                $error = true;
            }
        }
        return $errMessage;
    }

}
if (!function_exists('is_url_exist')) {

    function is_url_exist($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

}
if (!function_exists("get_months")) {

    function get_months() {
        return array(
            "01" => "January",
            "02" => "February",
            "03" => "March",
            "04" => "April",
            "05" => "May",
            "06" => "June",
            "07" => "July",
            "08" => "August",
            "09" => "September",
            "10" => "October",
            "11" => "November",
            "12" => "December",
        );
    }

}
if (!function_exists("get_years")) {

    function get_years() {
        $start_year = 1980;
        $end_year = date('Y');
        $years = array();
        for ($i = $start_year; $i <= $end_year; $i++) {
            $years[] = $i;
        }
        return $years;
    }

}
if (!function_exists("days_in_month")) {

    function days_in_month($month, $year = "") {
        if ($year == "") {
            $year = date("Y");
        }
        // calculate number of days in a month
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

}
if (!function_exists("encrypt_cookie")) {

    function encrypt_cookie($value) {
        if (!$value) {
            return false;
        }
        $key = 'htd7ZxUONSQ4NMAd';
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($value, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

}
if (!function_exists("decrypt_cookie")) {

    /**
     * 
     * @param type $value
     * @return boolean
     */
    function decrypt_cookie($value) {
        if (!$value) {
            return false;
        }
        $key = 'htd7ZxUONSQ4NMAd';
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($value), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

}
if (!function_exists('get_pagination_data')) {

    function get_pagination_data($total_records, $current_page = 1, $records_per_page) {
        if ($total_records > 0) {
            if (isset($current_page)) {
                $pn = intval(abs($current_page));
            } else {
                $pn = 1;
            }
            $last_page = ceil($total_records / $records_per_page);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $last_page) {
                $pn = $last_page;
            }
            $center_pages = array();
            $sub1 = $pn - 1;
            //$sub2 = $pn - 2;
            $add1 = $pn + 1;
            //$add2 = $pn + 2;
            if ($pn == 1) {
                //$center_pages['sub2'] = 0;
                $center_pages['sub1'] = 0;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = $add1;
                //$center_pages['add2'] = $add2;
            } else if ($pn == $last_page) {
                //$center_pages['sub2'] = $sub2;
                $center_pages['sub1'] = $sub1;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = 0;
                //$center_pages['add2'] = 0;
            } else if ($pn > 2 && $pn < ($last_page - 1)) {
                //$center_pages['sub2'] = $sub2;
                $center_pages['sub1'] = $sub1;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = $add1;
                //$center_pages['add2'] = $add2;
            } else if ($pn > 1 && $pn < $last_page) {
                //$center_pages['sub2'] = 0;
                $center_pages['sub1'] = $sub1;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = $add1;
                //$center_pages['add2'] = 0;
            }
            $pagination_records = array();
            $limit_start = ($pn - 1) * $records_per_page;
            $limit_end = $records_per_page;
            if ($last_page != "1") {
                if ($pn != 1) {
                    $previous = $pn - 1;
                    //$first = 1;
                    //$pagination_records['first'] = $first;
                    $pagination_records['previous'] = $previous;
                }
                $pagination_records['center_pages'] = $center_pages;
                if ($pn != $last_page) {
                    $nextPage = $pn + 1;
                    //$last = $last_page;
                    $pagination_records['next'] = $nextPage;
                    //$pagination_records['last'] = $last;
                }
            }
            //            if (!isset($pagination_records['first'])) {
            //                $pagination_records['first'] = 0;
            //            }
            if (!isset($pagination_records['previous'])) {
                $pagination_records['previous'] = 0;
            }
            if (!isset($pagination_records['center_pages'])) {
                //$center_pages['sub2'] = 0;
                $center_pages['sub1'] = 0;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = 0;
                //$center_pages['add2'] = 0;
                $pagination_records['center_pages'] = $center_pages;
            }
            if (!isset($pagination_records['next'])) {
                $pagination_records['next'] = 0;
            }
            //            if (!isset($pagination_records['last'])) {
            //                $pagination_records['last'] = 0;
            //            }
        } else {
            $pagination_records = false;
            $last_page = $limit_start = $limit_end = false;
        }
        $return_array = array();
        $return_array['pagination'] = (empty($pagination_records) ? 0 : $pagination_records);
        $return_array['total_pages'] = $last_page;
        $return_array['limit_start'] = $limit_start;
        $return_array['limit_end'] = $limit_end;
        return $return_array;
    }

}
if (!function_exists('force_download_file')) {

    function force_download_file($source_file, $download_name, $mime_type = '') {
        /*
          $source_file = path to a file to output
          $download_name = filename that the browser will see
          $mime_type = MIME type of the file (Optional)
         */
        if (!is_readable($source_file)) {
            die('File not found or inaccessible!');
        }
        $size = filesize($source_file);
        $download_name = rawurldecode($download_name);
        /* Figure out the MIME type (if not specified) */
        $known_mime_types = array(
            "pdf" => "application/pdf",
            "csv" => "application/csv",
            "txt" => "text/plain",
            "html" => "text/html",
            "htm" => "text/html",
            "exe" => "application/octet-stream",
            "zip" => "application/zip",
            "doc" => "application/msword",
            "xls" => "application/vnd.ms-excel",
            "ppt" => "application/vnd.ms-powerpoint",
            "gif" => "image/gif",
            "png" => "image/png",
            "jpeg" => "image/jpg",
            "jpg" => "image/jpg",
            "php" => "text/plain"
        );
        if ($mime_type == '') {
            $file_extension = strtolower(substr(strrchr($source_file, "."), 1));
            if (array_key_exists($file_extension, $known_mime_types)) {
                $mime_type = $known_mime_types[$file_extension];
            } else {
                $mime_type = "application/force-download";
            };
        };
        @ob_end_clean(); //off output buffering to decrease Server usage
        // if IE, otherwise Content-Disposition ignored
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 'Off');
        }
        header('Content-Type: ' . $mime_type);
        header('Content-Disposition: attachment; filename="' . $download_name . '.' . $file_extension . '"');
        header("Content-Transfer-Encoding: binary");
        header('Accept-Ranges: bytes');
        header("Cache-control: private");
        header('Pragma: private');
        header("Expires: Thu, 26 Jul 2012 05:00:00 GMT");
        // multipart-download and download resuming support
        if (isset($_SERVER['HTTP_RANGE'])) {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE'], 2);
            list($range) = explode(",", $range, 2);
            list($range, $range_end) = explode("-", $range);
            $range = intval($range);
            if (!$range_end) {
                $range_end = $size - 1;
            } else {
                $range_end = intval($range_end);
            }
            $new_length = $range_end - $range + 1;
            header("HTTP/1.1 206 Partial Content");
            header("Content-Length: $new_length");
            header("Content-Range: bytes $range-$range_end/$size");
        } else {
            $new_length = $size;
            header("Content-Length: " . $size);
        }
        /* output the file itself */
        $chunksize = 1 * (1024 * 1024); //you may want to change this
        $bytes_send = 0;
        if ($source_file = fopen($source_file, 'r')) {
            if (isset($_SERVER['HTTP_RANGE'])) {
                fseek($source_file, $range);
            }
            while (!feof($source_file) && (!connection_aborted()) && ($bytes_send < $new_length)) {
                $buffer = fread($source_file, $chunksize);
                print($buffer); //echo($buffer); // is also possible
                flush();
                $bytes_send += strlen($buffer);
            }
            fclose($source_file);
        } else {
            die('Error - can not open file.');
        }
        die();
    }

}
if (!function_exists('get_time_ago')) {

    function get_time_ago($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

}
if (!function_exists('resize_url')) {

    function resize_url($path, $width, $height) {
        return base_url() . 'resize.php?img=' . $path . '&width=' . $width . '&h=' . $height;
    }

}
if (!function_exists('convert_string')) {

    function convert_string($string) {
        return (string) $string;
    }

}
if (!function_exists('is_json_string')) {

    function is_json_string($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}
if (!function_exists('check_api_key')) {

    function check_api_key($headers) {

        if (isset($headers['apikey']) && $headers['apikey'] == API_KEY) {
            return true;
        }elseif (isset($headers['Apikey']) && $headers['Apikey'] == API_KEY) {
            return true;
        } else {
            return false;
        }
    }

}


if (!function_exists("get_client_ip")) {

    // Function to get the client IP address
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
if (!function_exists("get_current_url")) {

    function get_current_url() {
        $currentURL = current_url();
        $params = $_SERVER['QUERY_STRING'];
        return $currentURL . '?' . $params;
    }

}

if (!function_exists("create_directory")) {

    function create_directory($path) {
        $path = explode('/', $path);
        $string = '';
        foreach ($path as $key => $value) {
            $string .= $value;
            if (!is_dir($string)) {
                mkdir($string);
                @chmod($string, 0777);
            }
            $string .= '/';
        }
    }

}
function get_currency_type(){
    $CI = & get_instance();
    if($CI->session->userdata('currency_type') == 'dollar'){
        return '_dollar';
    }
    if($CI->session->userdata('currency_type') == 'inr'){
        return '';
    }
}
/*Editor content get*/
function closetags($html) {
    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i=0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)){
            $html .= '</'.$openedtags[$i].'>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}
function print_query() {
    $CI = & get_instance();
    echo $CI->db->last_query();
}

function get_language(){
    $CI = & get_instance();
    if($CI->session->userdata('language') == 'hindi'){
        return '_hindi';
    } else {
        return '';
    }
}
if(!function_exists('create_slug')){
    function create_slug($string) {
       $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
       $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

       return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
if (!function_exists('returnResponse')) {
    function returnResponse($result,$message,$data=null){
        $return =array();
        $return['result'] = $result;
        $return['message'] = $message;
        $return['data'] = $data;
        return $return;
    }
}
if (!function_exists('getCurrentDateWithTime')) {
    function getCurrentDateWithTime() {
       date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d h:i:s');
    }
}
if (!function_exists('generate_otp')) {

    function generate_otp($size) {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 1; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 1;

        $key = '';
        $keys = range(0, 9);
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $alpha_key . $key;
    }

}
if (!function_exists('format_date_Mdy_time')) {

    function format_date_Mdy_time($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("F d, Y h:i A", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}
if (!function_exists('format_date_Mdy')) {

    function format_date_Mdy_time($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("F d, Y", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}

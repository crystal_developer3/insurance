<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">User</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-primary card-outline">   
                        <div class="card-body box-profile">
                            <?php
                                if ($details != null) {

                                ?>
                                    <div class="text-center">
                                        <label>Current Banner</label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?=isset($details[0]['image']) && $details[0]['image'] !=null ? base_url(PROFILE_PICTURE).$details[0]['image'] : base_url().'assets/uploads/default_img.png'    ?>" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-8">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                
                                <!-- <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <tr>
                                                            <td><b>Name:</b></td>
                                                            <td><?= isset($details[0]['first_name']) && $details[0]['first_name'] !=null ? $details[0]['first_name'] : '';?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Email Address:</b></td>
                                                            <td><?= isset($details[0]['email']) && $details[0]['email'] !=null ? $details[0]['email'] : '';?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Mobile Number:</b></td>
                                                            <td><?= isset($details[0]['phone_number']) && $details[0]['phone_number'] !=null ? $details[0]['phone_number'] : '';?></td>
                                                        </tr>
                                                            
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <?php
                                    // $action = ($details == null) ? base_url('authority/home_slider/add_slider') : '';
                                    $action ="";
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open($action, $attributes);
                                ?>
                                    <input type="hidden" name="id" id="user_id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <label for="first_name" class="col-sm-3 col-form-label">First name<span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="first_name" placeholder="Enter first_name" class="form-control" value="<?= isset($details[0]['first_name']) && $details[0]['first_name'] !=null ? $details[0]['first_name'] : '';?>">
                                            <?= form_error("first_name", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="last_name" class="col-sm-3 col-form-label">Last name<span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="last_name" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['last_name']) && $details[0]['last_name'] !=null ? $details[0]['last_name'] : '';?>">
                                            <?= form_error("last_name", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-3 col-form-label">Email<span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="email" placeholder="Enter email address" class="form-control" value="<?= isset($details[0]['email']) && $details[0]['email'] !=null ? $details[0]['email'] : '';?>">
                                            <?= form_error("email", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="inputName3" class="col-sm-3 col-form-label">Address<span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="textarea" name="address" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= ($details != null) ? $details[0]['address'] : '';?></textarea>
                                            <?= form_error('address', "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>

                                    <!-- <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label">Image:<span class="required">*</span> (Upload by 2007&#215;804)</label>
                                        <div class="col-sm-9">
                                            <input type="file" name="image" class="form-control" accept="image/*">
                                            <span id="image_error"></span>
                                            <?= form_error("image", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div> -->
                                     
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        var user_id = $("#user_id").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                first_name: {required:true,maxlength:100},              
                last_name: {required:true,maxlength:100},              
                // image: { 
                //     required: function(element) {
                //         if (user_id == '') {  
                //             return true;
                //         }
                //         else {
                //             return false;
                //         }
                //     }, 
                // },                  
            },
            messages: {     
                first_name: {required :"Please enter first name",maxlength:"Allowd only 100 character"},   
                last_name: {required :"Please enter last name",maxlength:"Allowd only 100 character"},   
                // image: {required :"Image field is required"},       
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>
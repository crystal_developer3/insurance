<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class About extends CI_Controller {
	public function index()
	{
		$condition = array('status'=>'1');
		$data['details'] = about($condition);

		$condition['id']=7;
		$data['history_details'] = static_page($condition);

		$condition['id']=8;
		$data['who_we_are_details'] = static_page($condition);
		$this->load->view('about/index',$data);
	}
}
?>
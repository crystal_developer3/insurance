<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('get_active_class')) {

    function get_active_class($key) {

        $ci = & get_instance();
        $link = $ci->router->fetch_class() . '/' . $ci->router->fetch_method();

        $menu = array(
            'administrator/add' => array('my-account','profile'),
            'administrator/view' => array('my-account','profile'),
            'administrator/edit' => array('my-account','profile'),
            'account/changepassword' => array('my-account', 'change-password'),
            'subscriber/index' => array('subscriber'),

            'faq/index' => array('faq'),
            'faq/add_edit' => array('faq'),


            'types_of_insurance/index' => array('types-of-insurance'),
            'types_of_insurance/add_edit' => array('types-of-insurance'),

            'about/index' => array('about'),
            'about/add_edit' => array('about'),

            'language/index' => array('forms','language'),
            'language/add_edit' => array('forms','language'),

            'business_structure/index' => array('forms','business-structure'),
            'business_structure/add_edit' => array('forms','business-structure'),

            'category/index' => array('forms','category'),
            'category/add_edit' => array('forms','category'),

            'product/index' => array('forms','product'),
            'product/add_edit' => array('forms','product'),

            'general_contact/index' => array('forms','general-contact'),
            'general_contact/details' => array('forms','general-contact'),
            
            'general_quote/index' => array('forms','general-quote'),
            'general_quote/details' => array('forms','general-quote'),

            'general_quote_auto/index' => array('forms','general-quote-auto'),
            'general_quote_auto/details' => array('forms','general-quote-auto'),

            'quote/index' => array('forms','quote'),
            'quote/details' => array('forms','quote'),

            'free_quote/index' => array('forms','free-quote'),
            'free_quote/details' => array('forms','free-quote'),

            'year/index' => array('forms','year'),
            'year/add_edit' => array('forms','year'),            

            'make/index' => array('forms','make'),
            'make/add_edit' => array('forms','make'),            

            'models/index' => array('forms','models'),
            'models/add_edit' => array('forms','models'),            

            'home_slider/index' => array('home-slider'),
            'home_slider/add_edit' => array('home-slider'),

            'user/index' => array('user'),
            'user/add_edit' => array('user'),

            'static_page/index' => array('page','static-page'),
            'static_page/add_edit' => array('page','static-page'),
            
            'contact_us/index' => array('contact-us'),

            'social_links/index' => array('social-links'),
            'social_links/edit' => array('social-links'),
            
            'p_and_c/index' => array('p-and-c-menu','p-and-c'),
            'p_and_c/add_edit' => array('p-and-c-menu','p-and-c'),

            'p_and_c_sub/index' => array('p-and-c-menu','p-and-c-sub'),
            'p_and_c_sub/add_edit' => array('p-and-c-menu','p-and-c-sub'),

            'p_and_c_sub_sub/index' => array('p-and-c-menu','p-and-c-sub-sub'),
            'p_and_c_sub_sub/add_edit' => array('p-and-c-menu','p-and-c-sub-sub'),

            'life_and_health/index' => array('life-and-health-menu','life-and-health'),
            'life_and_health/add_edit' => array('life-and-health-menu','life-and-health'),

            'life_and_health_sub/index' => array('life-and-health-menu','life-and-health-sub'),
            'life_and_health_sub/add_edit' => array('life-and-health-menu','life-and-health-sub'),

            'life_and_health_sub_sub/index' => array('life-and-health-menu','life-and-health-sub-sub'),
            'life_and_health_sub_sub/add_edit' => array('life-and-health-menu','life-and-health-sub-sub'),

            'bonds/index' => array('bonds-menu','bonds'),
            'bonds/add_edit' => array('bonds-menu','bonds'),

            'bonds_sub/index' => array('bonds-menu','bonds-sub'),
            'bonds_sub/add_edit' => array('bonds-menu','bonds-sub'),

            'bonds_sub_sub/index' => array('bonds-menu','bonds-sub-sub'),
            'bonds_sub_sub/add_edit' => array('bonds-menu','bonds-sub-sub'),
            
            'settings/index' => array('settings'),   
            /*Front side*/
            'home/index' => array('home'),
            'contact_us/index' => array('contact-us'),            
        );
        if (array_key_exists($link, $menu)) {
            if (in_array($key, $menu[$link])) {
                return array('main_class' => 'menu-open', 'sub_class' => 'active', 'open_class'=> 'display:block');
            }
        } else {
            return array('main_class' => '', 'sub_class' => '');
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class About extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        $data = array();  
        $tmp_data = about();
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/about/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        $data['details'] = $this->production_model->get_all_with_where_limit('about','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        $this->load->view('authority/about/view',$data);
    }
    function add()
    {
        $data['details'] = array();
        $this->load->view('authority/about/add-edit',$data);
    }
    // function add_about()
    // {
    //     $data = $this->input->post();
    //     $this->form_validation->set_rules('title', 'title', 'required', array('required' => 'Please enter title'));
    //     $this->form_validation->set_rules('description', 'description', 'required|strip_tags', array('required' => 'Please enter description'));
        
    //     $this->form_validation->set_rules('image', 'image', 'callback_file_selected_test');
    //     if ($this->form_validation->run() == FALSE)
    //     {
    //         $data['details'] = array();
    //         $this->load->view('authority/about/add-edit',$data);
    //     }
    //     else
    //     {
    //         $data['image'] = $this->production_model->image_upload(ABOUT_IMAGE,'image');
    //         $record = $this->production_model->insert_record('about',$data);
    //         if ($record !='') {
    //             $this->session->set_flashdata('success', 'Added successful.');
    //             redirect(base_url('authority/author')); 
    //         }
    //         else
    //         {
    //             $this->session->set_flashdata('error', 'Not added.');
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }   
    //     }
    // }
    function edit($id)
    {
        $data = $this->input->post();
        $condition = array('id'=>$id);
        $data['details'] = about($condition);

        if ($this->input->method() == 'post') {
            $this->form_validation->set_rules('title', 'title', 'required', array('required' => 'Please enter title'));
            $this->form_validation->set_rules('description', 'description', 'required|strip_tags', array('required' => 'Please enter description'));
            if ($this->form_validation->run() == FALSE)
            {
                $tmp = $data['details'];
                $data['details'] = array_merge($tmp,$this->form_validation->error_array());
            }
            else
            {
                if ($_FILES['image']['name'] !='') {
                    $data['image'] = $this->production_model->image_upload(ABOUT_IMAGE,'image','about',$id);
                } 
                if ($_FILES['back_image']['name'] !='') {
                    $data['back_image'] = $this->production_model->image_upload(ABOUT_IMAGE,'back_image','about',$id);
                }               
                unset($data['details']);
                $record = $this->production_model->update_record('about',$data,array('id'=>$id));
                if ($record == 1) {
                    $this->session->set_flashdata('success', 'Updated successful.');
                    redirect(base_url('authority/about'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Not updated.');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
        $this->load->view('authority/about/add-edit',$data);
    }
    function file_selected_test(){
        $this->form_validation->set_message('file_selected_test', 'Image field is required');
        if (empty($_FILES['image']['name'])) {
            return false;
        }else{
            return true;
        }
    }
    
}
?>
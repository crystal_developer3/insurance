<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . '/libraries/class.phpmailer.php');

class Production_model extends CI_Model {
	function insert_record($table_name,$data)
	{
		$record = $this->db->insert($table_name,$data);
		return $this->db->insert_id();
	}
	function get_all_with_where($table_name,$order_by_column="",$order_by_value="",$where_array)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->result_array();
	}
	function get_all_with_where_in($table_name, $order_by_column = "", $order_by_value = "", $column_name, $where_in_array, $where_array = array()) {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($order_by_column, $order_by_value);
        $this->db->where_in($column_name, $where_in_array);
        $this->db->where($where_array);
        $record = $this->db->get();
        return $record->result_array();
    }
    function get_delete_where_in($table_name, $column_name, $where_in_array) {
        $this->db->where_in($column_name, $where_in_array);
        $result = $this->db->delete($table_name);
        return $result;
    }
	function get_where_or($table_name,$order_by_column="",$order_by_value="",$or_where,$where_array)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->or_where($or_where);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->result_array();
	}
	function get_all_with_where_limit($table_name,$order_by_column="",$order_by_value="",$where_array = array(),$limit,$start)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where($where_array);
		if (strval($limit) !='' && strval($start) !='') {
			$this->db->limit($limit,$start);		
    	}		
		$record = $this->db->get();
		return $record->result_array();
	}

	function get_all_with_where_limit_between($table_name,$order_by_column="",$order_by_value="",$start_date,$end_date,$limit,$start)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') >= '".$start_date."'");
    	$this->db->where("DATE_FORMAT(created_at,'%Y-%m-%d') <= '".$end_date."'");
    	if ($limit !='' && $start !='') {
			$this->db->limit($limit,$start);		
    	}
		$record = $this->db->get();
		return $record->result_array();
	}

	function get_all_with_where_limit_like($column="",$table_name,$like_array='',$where_array = array(),$limit,$start,$order_by_column="",$order_by_value="")
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		if($like_array != ""){
			$this->db->like($like_array);
		}		
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where($where_array);
		if ($limit !='' && $start !='') {
			$this->db->limit($limit,$start);		
    	}
		$record = $this->db->get();
		return $record->result_array();
	}

	function get_count_with_like($column="",$table_name,$like_array='',$where_array = array())
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		if($like_array != ""){
			$this->db->like($like_array);
		}		
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->num_rows();
	}

	function get_where_user($column="",$table_name,$order_by_column="",$order_by_value="",$where_array = array())
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->result_array();
	}
	
	function check_social_login($social_type,$social_id,$email_id){
		$this->db->select("*");
		$this->db->from("register");
		$this->db->where("(email = '".$email_id."' OR '".$social_type."' = '".$social_id."')");
		$this->db->where('status','1');

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}else {
			return false;
		}
	}

	function check_login_user($user_id,$password){
		$this->db->select("user_register.*");
		$this->db->from("user_register");
		$this->db->where("(email = '".$user_id."' OR mobile_no = '".$user_id."')");
		$this->db->where('status','0');

		if($password !='')
			$this->db->where("password",$password);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}else {
			return false;
		}
	}

	function email_or_mobile_login($table_name,$user_id){
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where("(email = '".$user_id."' OR phone_number = '".$user_id."')");		
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}else {
			return false;
		}
	}
	
	function get_all_record($column="", $table_name, $group_by="")
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		$this->db->group_by($group_by);
		$record = $this->db->get();
		return $record->result_array();
	}
	function count_num_of_rows($column="",$table_name,$where_array)
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table_name);
		$this->db->where($where_array);
		$record = $this->db->get();
		return $record->num_rows();
	}
	function update_record($table_name,$data_array,$where_array)
	{
		$this->db->where($where_array);
		$this->db->update($table_name,$data_array);
		// return $this->db->affected_rows();
		return 1;

	}
	function jointable($table,$join = "",$where = "",$or = "",$group_by = '')
	{
		$this->db->select('*');
		$this->db->from($table);
		if($where != ""){
			$this->db->where($where);
		}
		if($or != "")
		{
			$this->db->or_where($or);
		}
		if($group_by != ""){
			$this->db->group_by($group_by);
		}
		if($join != ""){
			foreach ($join as $join_row) {
				$this->db->join($join_row['table_name'],$join_row['column_name'],$join_row['type']);
			}
		}
		$record = $this->db->get();
		return $record->result_array();
	}
	function jointable_descending($column="",$table, $like_array="", $join = "",$order_by_column = "", $order_by_value = "", $where = "", $or_where = array() , $group_by = array(), $limit = "",$start = "",$or_like_array=array(),$where_in_array = "")
	{
		if ($column =='') {
			$column = '*';
		}
		$this->db->select($column);
		$this->db->from($table);
		if($like_array != ""){
			$this->db->like($like_array);
		}
		if ($group_by !='') {
			$this->db->group_by($group_by);
		}
		if($where != ""){
			$this->db->where($where);
		}
		if($or_where != ""){
			$this->db->or_where($or_where);
		}
		if(!empty($or_like_array) &&  $or_like_array != ""){
			$this->db->group_start();
			$this->db->or_like($or_like_array);
            $this->db->group_end();
		}
		if($order_by_column != "" || $order_by_value !="")
		{
			$this->db->order_by($order_by_column,$order_by_value);
		}
		if(!empty($where_in_array)){
			foreach ($where_in_array as $key => $value) {
				if(!empty($value)){
					$this->db->where_in($key, $value);
				}
			}
		}
		if($join != ""){
			foreach ($join as $join_row) {
				$this->db->join($join_row['table_name'],$join_row['column_name'],$join_row['type']);
			}
		}
		if($limit != "" && $start != ""){
			$this->db->limit($limit,$start);		
		} else if ($limit != ''){
			$this->db->limit($limit);
		}
		$record = $this->db->get();
		return $record->result_array();
	}
	//============ End ==========//

	function delete_record($table_name,$where_array){      
        $result = $this->db->delete($table_name,$where_array);
        return $result;
    }
    function get_all_with_like($table_name,$like_array,$where_array,$order_by_column,$order_by_value){
        $this->db->select('*');
		$this->db->from($table_name);
		if($like_array != ""){
			$this->db->like($like_array);
		}		
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$record = $this->db->get();
		return $record->result_array();
    }
    function send_email($subject,$to,$simple_msg,$view,$data,$attech_file){

    	$config = array('mailtype' => 'html','charset'  => 'utf-8','priority' => '1');
        $this->email->initialize($config);
		$this->load->library('email',$config);
		$this->email->from($this->config->item('from_email'));
		$this->email->to($to);	
		$this->email->subject($subject);

		if ($simple_msg != '') {
			$body = $simple_msg;
			if ($attech_file !='') {
				$this->email->attach($attech_file);	
			}
		}
		else{
			$body = $this->load->view($view,$data,TRUE);
		}
		$this->email->message($body);			

		if($this->email->send() == 1){ 
		  	return true;
		}
		else{
			$this->email->print_debugger();
			return false;
		}
    }
 	
 	function mail_send($subject,$to,$simple_msg,$view,$data,$attech_file,$newsletter_id=''){ // php mailer uses.
 		if(! is_array($to)){
 			$to = array($to);
 		}
		$mail = new PHPMailer;
        $mail->IsSMTP();         // Set mailer to use SMTP

		if (isset($newsletter_id) && $newsletter_id !=null) {
			$get_details = $this->Production_model->get_all_with_where('news_letter','','',array('id'=> $newsletter_id));
			
			$mail->Host = $get_details[0]['email']; // Specify main and backup server
	        $mail->Port = SMTP_PORT; // Set the SMTP port
	        $mail->SMTPAuth = true;  // Enable SMTP authentication
	        $mail->Username = $get_details[0]['name']; // SMTP username
	        $mail->Password = $get_details[0]['password']; // SMTP password
		}
		else{
	        $mail->Host = SMTP_HOST; // Specify main and backup server
	        $mail->Port = SMTP_PORT; // Set the SMTP port
	        $mail->SMTPAuth = true;  // Enable SMTP authentication
	        $mail->Username = SMTP_USERNAME; // SMTP username
	        $mail->Password = SMTP_PASSWORD; // SMTP password
		}
        //$mail->SMTPSecure = 'tls'; // Enable encryption, 'ssl' also accepted
        $mail->From = FROM_EMAIL; // SMTP username
        $mail->FromName = FROM_EMAIL_TITLE;
        foreach ($to as $email) {
			$mail->AddAddress($email);
		}
        // $mail->AddAddress($to);
        $mail->IsHTML(true);       // Set email format to HTML
        $mail->Subject = $subject;

        if ($simple_msg != '') {
			//$body = $simple_msg;
			if(is_array($attech_file)){
				$mail->Body = $simple_msg;
				foreach ($attech_file as $key => $value) {
					$mail->addAttachment($value);
				}
	 		}
	 		else{
				$mail->Body = $simple_msg;
				if ($attech_file !='') {
					$mail->addAttachment($attech_file);
					$mail->Body = $simple_msg;
				}
	 		}
	 		$body = $this->load->view($view,$data,TRUE);
			$mail->Body = $body;
		}
		else{
			$body = $this->load->view($view,$data,TRUE);
			$mail->Body = $body;
        	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
		}
        if (!$mail->Send()) {
          	// echo 'Message could not be sent.';
          	// echo 'Mailer Error: ' . $mail->ErrorInfo;
        	return false;
            //   exit;
        } else {
            // echo 'Message sent.';
            return true;
        }
 	}

    public function generate_thumbnail($path,$file_name) {
    	if (!empty($file_name) &&  !file_exists(FCPATH . $path.'thumbnail/' . $file_name)) { 

	        $source_path = $path.$file_name;
	        $target_path = $path.'thumbnail/';
	        if (!is_dir($target_path)) {
	            mkdir($target_path);
	            @chmod($target_path, 0777);
	        }
	        $config_manip = array(
	            'image_library' => 'gd2',
	            'source_image' => $source_path,
	            'new_image' => $target_path,
	            'maintain_ratio' => FALSE,
	            'create_thumb' => TRUE,
	            'thumb_marker' => '',
	            'width' => 60,
	            'height' => 60
	        );
	        // $this->load->library('image_lib', $config_manip);
	        $this->image_lib->initialize($config_manip);
	        
	        if (!$this->image_lib->resize()) {
	           $this->image_lib->display_errors();
	        }
	        // clear //
	        $this->image_lib->clear();
	    }
    }

    public function get_total($table_name) 
    {
        return $this->db->count_all($table_name);
    }

    //============ pagination Start ==============//

    public function only_pagination($settings = array()) {
        $response = array();

        $this->load->library('pagination');
        $config_pagination = array();
        $config_pagination['base_url'] = isset($settings['url']) ? $settings['url'] : site_url();
        $config_pagination['total_rows'] = $settings['total_record'];
        $config_pagination['per_page'] = isset($settings['per_page']) ? $settings['per_page'] : RECORDS_PER_PAGE;
        if ($this->uri->segment(3) == 'jd-form' || $this->uri->segment(3) == 'filter_jd_form' || $this->uri->segment(3) == 'review') {
        	$config_pagination["uri_segment"] = 5;
        }
        else{
        	$config_pagination["uri_segment"] = 4;
        }
        // $config_pagination['num_links'] = 4;
        $config_pagination['use_page_numbers'] = TRUE;
        $config_pagination['display_pages'] = TRUE;
        $config_pagination['full_tag_open'] = '<ul class="pagination pagination-sm m-0 float-right">';
        $config_pagination['full_tag_close'] = '</ul>';
        $config_pagination['first_link'] = 'First';
        $config_pagination['first_tag_open'] = '<li class="page-item"><a class="page-link" href="">';
        $config_pagination['first_tag_close'] = '</a></li>';
        $config_pagination['last_link'] = 'Last';
        $config_pagination['last_tag_open'] = '<li class="page-item"><a class="page-link" href="">';
        $config_pagination['last_tag_close'] = '</a></li>';
        $config_pagination['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config_pagination['next_tag_open'] = '<li class="page-item">';
        $config_pagination['next_tag_close'] = '</li>';
        $config_pagination['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config_pagination['prev_tag_open'] = '<li class="page-item">';
        $config_pagination['prev_tag_close'] = '</li>';
        $config_pagination['cur_tag_open'] = '<li class="page-item"><a href="" class="page-link active" style="background:#343A40;color:#fff;">';
        $config_pagination['cur_tag_close'] = '</a></li>';
        $config_pagination['num_tag_open'] = '<li class="page-item">';
        $config_pagination['num_tag_close'] = '</li>';
        $config_pagination['attributes'] = array('class'=>"page-link");

        if ($this->uri->segment(3) == 'jd-form' || $this->uri->segment(3) == 'filter_jd_form') {
        	$page_number = (isset($settings['page_no']) && $settings['page_no'] != '') ? $settings['page_no'] : (($this->uri->segment(5)) ? $this->uri->segment(5) : 0);    
        }
        else{
        	$page_number = (isset($settings['page_no']) && $settings['page_no'] != '') ? $settings['page_no'] : (($this->uri->segment(4)) ? $this->uri->segment(4) : 0);        
        }
        $config_pagination['cur_page'] = $page_number;
        $this->pagination->initialize($config_pagination);
        $response['pagination'] = $this->pagination->create_links();
        // echo"<pre>"; print_r($response); exit;

        //$page_number = isset($settings['page_no']) ? $settings['page_no'] : (($this->uri->segment(4)) ? $this->uri->segment(4) : 0);

        if ($page_number > ceil(($config_pagination['total_rows'] / $config_pagination['per_page']))) {
            /* Redirect when page limit exceeded */
            redirect($config_pagination['base_url']);
        }

        /* GETTING A OFFSET */
        $offset = ($page_number == 0) ? 0 : ($page_number * $config_pagination['per_page']) - $config_pagination['per_page'];

        /* FOR SELECTING DATA WITH START AND END LIMIT */
        $response['start'] = $offset;
        $response['limit'] = $settings['per_page'];
        $response['no'] =  $response['start']+1;
        return $response;
    }

    public function only_pagination_in_front($settings = array()) {
        $response = array();
        $this->load->library('pagination');
        $config_pagination = array();
        $config_pagination['base_url'] = isset($settings['url']) ? $settings['url'] : site_url();
        $config_pagination['total_rows'] = $settings['total_record'];
        $config_pagination['per_page'] = isset($settings['per_page']) ? $settings['per_page'] : RECORDS_PER_PAGE;
        $config_pagination["uri_segment"] = 3;
        $config_pagination['use_page_numbers'] = TRUE;
        $config_pagination['display_pages'] = TRUE;
        $config_pagination['full_tag_open'] = '<ul class="pagination mt-5">';
        $config_pagination['full_tag_close'] = '</ul>';
        $config_pagination['first_link'] = 'First';
        $config_pagination['first_tag_open'] = '<li class="page-item active">';
        $config_pagination['first_tag_close'] = '</li>';
        $config_pagination['last_link'] = 'Last';
        $config_pagination['last_tag_open'] = '<li class="page-item">';
        $config_pagination['last_tag_close'] = '</li>';
        $config_pagination['next_link'] = '<span><i class="fa fa-chevron-right"></i></span>';
        $config_pagination['next_tag_open'] = '<li class="page-item">';
        $config_pagination['next_tag_close'] = '</li>';
        $config_pagination['prev_link'] = '<span><i class="fa fa-chevron-left"></i></span>';
        $config_pagination['prev_tag_open'] = '<li class="page-item">';
        $config_pagination['prev_tag_close'] = '</li>';
        $config_pagination['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#" aria-label="Previous">';
        $config_pagination['cur_tag_close'] = '</a></li>';
        $config_pagination['num_tag_open'] = '<li class="page-item">';
        $config_pagination['num_tag_close'] = '</li>';
        $config_pagination['attributes'] = array('class'=>"page-link");
        
        if ($this->uri->segment(2)=='category-product') {
        	$page_number = (isset($settings['page_no']) && $settings['page_no'] != '') ? $settings['page_no'] : (($this->uri->segment(4)) ? $this->uri->segment(4) : 0);
        }
        else{
        	$page_number = (isset($settings['page_no']) && $settings['page_no'] != '') ? $settings['page_no'] : (($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
        }
        $config_pagination['cur_page'] = $page_number;
        $this->pagination->initialize($config_pagination);
        $response['pagination'] = $this->pagination->create_links();

        if ($page_number > ceil(($config_pagination['total_rows'] / $config_pagination['per_page']))) {
            /* Redirect when page limit exceeded */
            redirect($config_pagination['base_url']);
        }
        /* GETTING A OFFSET */
        $offset = ($page_number == 0) ? 0 : ($page_number * $config_pagination['per_page']) - $config_pagination['per_page'];

        /* FOR SELECTING DATA WITH START AND END LIMIT */
        $response['start'] = $offset;
        $response['limit'] = $settings['per_page'];
        $response['no'] =  $response['start']+1;
        return $response;
    }

    public function pagination_create($url,$table_name,$like_array='',$odr_by_column,$odr_by_value,$where_array,$search_value='')
    {
    	if (isset($search_value) && $search_value !=null) {
    		$this->db->group_start();
    		$count = 0;
    		foreach ($search_value as $key => $value) {
            	$count+=1;
            	if ($count == 1) {
            		$this->db->like($key,$value);
            	}
            	if ($count > 1) {
            		$this->db->or_like($key,$value);
            	}
    		}
            $this->db->group_end();
        }
    	$total_records = $this->production_model->count_num_of_rows('',$table_name,$where_array);
	    $config = array();
        $config["base_url"] = $url;
        $config["total_rows"] = $total_records;
        $config["per_page"] = RECORDS_PER_PAGE;
        $config['use_page_numbers'] = TRUE;
        $config['display_pages'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        if ($page_number > ceil(($config['total_rows'] / $config['per_page']))) {
            /* Redirect when page limit exceeded */
            redirect($config['base_url']);
        }
        /* GETTING A OFFSET */
        $page = ($page_number == 0) ? 0 : ($page_number * $config['per_page']) - $config['per_page'];        
        if (isset($search_value) && $search_value !=null) {
    		$this->db->group_start();
    		$count = 0;
    		foreach ($search_value as $key => $value) {
            	$count+=1;
            	if ($count == 1) {
            		$this->db->like($key,$value);
            	}
            	if ($count > 1) {
            		$this->db->or_like($key,$value);
            	}
    		}
            $this->db->group_end();
        }
        $response['data']  = $this->production_model->get_all_with_where_limit($table_name,$odr_by_column,$odr_by_value,$where_array,$config["per_page"], $page); 

        $response['links'] = $this->pagination->create_links();  
        $response['no'] = $page+1; 
        // $response['total_records'] = $total_records;
        return $response; 
	}
    //============= pagination End ==============//
	function image_upload($image_path,$filename,$table_name='',$id=''){
		if (!is_dir($image_path)) {
            mkdir($image_path);
            @chmod($image_path,0777);
        }
		if($_FILES[$filename]['name'] !='')
        {
        	if (isset($table_name) && $table_name !=null) {
	        	$get_image = $this->production_model->get_all_with_where($table_name,'','',array('id'=>$id));
	            if ($get_image !=null && $get_image[0][$filename] !=null && !empty($get_image[0][$filename]))
	            {
	                @unlink($image_path.$get_image[0][$filename]);
	                @unlink($image_path.'thumbnail/'.$get_image[0][$filename]);
	            }
	        }
        	if ($_FILES[$filename]["size"] >= MAX_FILE_SIZE) {
                $this->session->set_flashdata('error', 'Maxfile upload'. (MAX_FILE_SIZE / 1000000) . 'MB');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $config['upload_path']   = $image_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name']  = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($filename))
            {                        
                $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
            }
            $imageDetailArray = $this->upload->data();          
            $this->production_model->generate_thumbnail($image_path,$imageDetailArray['file_name']);
            return $imageDetailArray['file_name'];
        }else{
        	return '';
        }
	}
	function other_file_upload($image_path,$filename,$table_name='',$id=''){
		if (!is_dir($image_path)) {
            mkdir($image_path);
            @chmod($image_path,0777);
        }
		if($_FILES[$filename]['name'] !='')
        {
        	if (isset($table_name) && $table_name !=null) {
	        	$get_image = $this->production_model->get_all_with_where($table_name,'','',array('id'=>$id));
	            if ($get_image !=null && $get_image[0][$filename] !=null && !empty($get_image[0][$filename]))
	            {
	                @unlink($image_path.$get_image[0][$filename]);
	            }
	        }
        	if ($_FILES[$filename]["size"] >= MAX_FILE_SIZE) {
                $this->session->set_flashdata('error', 'Maxfile upload'. (MAX_FILE_SIZE / 1000000) . 'MB');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $config['upload_path']   = $image_path;
            $config['allowed_types'] = 'doc|docx|pdf|jpg|png|jpeg';
            $config['encrypt_name']  = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($filename))
            {                        
                $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
            }
            $imageDetailArray = $this->upload->data();          
            return $imageDetailArray['file_name'];
        }else{
        	return '';
        }
	}
	function multiple_image($file_name,$image_path,$table_name,$count,$product_id=''){
        if (!is_dir($image_path)) {
            mkdir($image_path);
            @chmod($image_path,0777);
        }
        $config['upload_path']   = $image_path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
    	$this->upload->initialize($config);

        $files = $_FILES;    
        if($files[$file_name]['name'] && $files[$file_name]['name'] != "")
        {
        	$return_msg = FALSE;
            for($i=0; $i<$count; $i++)
            {   
                $_FILES[$file_name]['name']= $files[$file_name]['name'][$i];
                $_FILES[$file_name]['type']= $files[$file_name]['type'][$i];
                $_FILES[$file_name]['tmp_name']= $files[$file_name]['tmp_name'][$i];
                $_FILES[$file_name]['error']= $files[$file_name]['error'][$i];
                $_FILES[$file_name]['size']= $files[$file_name]['size'][$i];  

                $this->upload->do_upload($file_name);
                $image_name = $this->upload->data();
                $image_name = $image_name['file_name'];
                $this->production_model->generate_thumbnail($image_path,$image_name);
                
                if ($product_id !='') {
                	$imagedata['page_id'] = $product_id;
                }
                $imagedata[$file_name] = $image_name;
                if (isset($image_name) && $image_name !=null) {                	
                    $image_id = $this->production_model->insert_record($table_name, $imagedata);
                	if ($image_id !='') {
                		$return_msg = TRUE;
                	}
                }
            }
            echo $return_msg = TRUE;       
        }
    }
    function other_documents($file_name,$image_path,$table_name,$count){
        if (!is_dir($image_path)) {
            mkdir($image_path);
            @chmod($image_path,0777);
        }
        $config['upload_path']   = $image_path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
    	$this->upload->initialize($config);

        $files = $_FILES;    
        if($files[$file_name]['name'] && $files[$file_name]['name'] != "")
        {
        	$return_msg = array();
            for($i=0; $i<$count; $i++)
            {   
                $_FILES[$file_name]['name']= $files[$file_name]['name'][$i];
                $_FILES[$file_name]['type']= $files[$file_name]['type'][$i];
                $_FILES[$file_name]['tmp_name']= $files[$file_name]['tmp_name'][$i];
                $_FILES[$file_name]['error']= $files[$file_name]['error'][$i];
                $_FILES[$file_name]['size']= $files[$file_name]['size'][$i];  

                $this->upload->do_upload($file_name);
                $image_name = $this->upload->data();
                $image_name = $image_name['file_name'];

                $return_msg[] = $image_name;
            }
            return $return_msg;       
        }
    }
    function multiple_pdf_file($image_path,$table_name,$count,$product_id=''){
        if (!is_dir($image_path)) {
            mkdir($image_path);
            @chmod($image_path,0777);
        }
        $config['upload_path']   = $image_path;
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
    	$this->upload->initialize($config);

        $files = $_FILES;    
        if($files['corporate_document']['name'] && $files['corporate_document']['name'] != "")
        {
        	$return_msg = FALSE;
            for($i=0; $i<$count; $i++)
            {   
                $_FILES['corporate_document']['name']= $files['corporate_document']['name'][$i];
                $_FILES['corporate_document']['type']= $files['corporate_document']['type'][$i];
                $_FILES['corporate_document']['tmp_name']= $files['corporate_document']['tmp_name'][$i];
                $_FILES['corporate_document']['error']= $files['corporate_document']['error'][$i];
                $_FILES['corporate_document']['size']= $files['corporate_document']['size'][$i];  

                $this->upload->do_upload('corporate_document');
                $image_name = $this->upload->data();
                $image_name = $image_name['file_name'];
                
                if ($product_id !='') {
                	$imagedata['about_menu_id'] = $product_id;
                }
                $imagedata['corporate_document'] = $image_name;
                if (isset($image_name) && $image_name !=null) {
                    $image_id = $this->production_model->insert_record($table_name, $imagedata);
                	if ($image_id !='') {
                		$return_msg = TRUE;
                	}
                }
            }
            echo $return_msg = TRUE;       
        }
    }

    /*Android push notification start*/
    function sendFcmNotification($type,$pushMessage,$user_id,$fcm,$insert,$user_type='') {
		$counter=0;
		foreach ($fcm as $fcmdata){
			$fcmRegIds[] = $fcmdata;
			$counter++;
			$fcm_msg = json_decode($pushMessage);
			$img_name = '';
			if (filter_var($fcm_msg->image, FILTER_VALIDATE_URL)) { 
			  	$img_name = $fcm_msg->image; // to get only filename from fullpath.
			}
			else{
				$img_name = $fcm_msg->image;
			}
			if($counter == 999){
				if (count($fcmRegIds) > 0) {		
					$this->load->model('production_model', 'Common');
					$pushStatus = $this->Common->sendPushNotificationToFCM($fcmRegIds, $fcm_msg);
				}				
				unset($fcmRegIds);
				$counter = 0;
			}
		}
		if (isset($fcmRegIds) && count($fcmRegIds) > 0) {
			$this->load->model('production_model', 'Common');
			$pushStatus =$this->Common->sendPushNotificationToFCM($fcmRegIds, $fcm_msg);
		}
		if(count($fcm)>0 && $insert==1){
			$createddate = date('Y-m-d H:i:s');			
			$post_data = json_decode($pushMessage);
			$post_data->image;
			
			$img_name = '';
			if (filter_var($post_data->image, FILTER_VALIDATE_URL)) { 
			  	$img_name = pathinfo($post_data->image, PATHINFO_BASENAME); //get only filename from fullpath.
			  	$fcm_msg->image = $img_name;
			}
			else{
				$img_name = $post_data->image;
			}
			$description = $post_data->message; 
			$description_turkish = isset($post_data->message_turkish) && $post_data->message_turkish !=null ? $post_data->message_turkish : ''; 
			$image = $img_name; 
			$title = $post_data->title; 
			$title_turkish = isset($post_data->title_turkish) && $post_data->title_turkish !=null ? $post_data->title_turkish : ''; 

			$notificationdata = array(
				'user_id'=>$user_id,
				'description'=>$description,
				'image'=>$image,
				'title'=>$title,
				'type'=>$type,
				'created_at'=>$createddate
			);
			$insertfcmnotification = $this->db->insert('notification',$notificationdata);
		}
	}
	function sendPushNotificationToFCM($fcm, $message) {
		$msg = array
		(
			'message' 	=> $message
		);
	    $url = 'https://fcm.googleapis.com/fcm/send';
	    /*api_key available in:
	    Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/
	    $api_key = FCMKey;
	    
	    if(isset($fcm[1]) && !empty($fcm[1]) && $fcm[1] =='android'){
	    	$fields = array (
		        'registration_ids' => $fcm,
		        'data' => $msg
		    );
		}else{ // used for ios
			$title = '';
			$msg = '';
			if (isset($message->user_id) && $message->user_id !=null) {
				$details = $this->production_model->get_all_with_where('register','','',array("id"=>$message->user_id));
				if (isset($details) && $details !=null) {
					if ($details[0]['language_code'] == '1') {
						$title = $message->title;
						$msg = $message->message;
					}
					else if($details[0]['language_code'] == '2') {
						$title = $message->title_turkish;
						$msg = $message->message_turkish;		
					}
				}
			}
			$fields = array (
		        'registration_ids' => $fcm,
		        'priority' => 10,
		        'data'=> $message,
		        'notification' => array('title' => $title, 'body' => $msg ,'Time' => "06:00" ,'sound'=>'Default'),
		    );
		}
	    //header includes Content type and api key
	    $headers = array(
	        'Content-Type:application/json',
	        'Authorization:key='.$api_key
	    );
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	    $result = curl_exec($ch);
	    if ($result === FALSE) {
	        die('FCM Send Error: ' . curl_error($ch));
	    }
	    curl_close($ch);
	    return $message;
	}
	function uploadFile($ElementFileName){
		$str = $FileNM = "";
		$File = $_FILES[$ElementFileName]["name"];
		$fileName = $File; 
		$fileTmpLoc = $_FILES[$ElementFileName]["tmp_name"];
		$temp = explode(".", $File);
		$extension = end($temp);
		$temp = explode('.', $fileName);
		$ext = array_pop($temp);
		$name = preg_replace("/[^a-zA-Z0-9-]/", "",implode('.', $temp ));
		$time1 = time();
		//return $FileNM = $name.$time1.".".$extension;
		return $time1.".".$extension;
	}
}
/* End of file Product_model.php */
/* Location: ./application/models/Product_model.php */
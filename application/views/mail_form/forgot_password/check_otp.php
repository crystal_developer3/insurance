<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=SITE_TITLE?></title>
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>assets/image/favicon.png">
    
    <!-- all css here -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css">
    
    
    
    <link rel="stylesheet" href="<?= base_url()?>assets/css/responsive.css">

    <!--New add-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/developer.css">
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
    <style type="text/css">
        .footer_social ul li a i{
            padding: 0.8rem 0px;
        }
        .single_footer h2 span font{
            line-height: 27px;
        }
    </style>
</head>
<body>
    <section class="main_content_area">
        <div class="container"><br><br>
            <center><h4>Check Otp</h4></center>
            <div class="account_form">
                <?php $this->load->view('include/messages');?> 
                <div class="success_message"></div> 
                <form id="form" method="post" action="">
                    <p>   
                        <label>Email </label>
                        <input type="text" name="email" class="form-control email" value="<?= $email;?>" readonly>
                    </p>
                    <p>   
                        <label>Enter Otp <span>*</span></label>
                        <input type="text" name="otp" maxlength="6" class="form-control only_digits otp" placeholder="Enter your otp">
                    </p>   
                    <div class="login_submit">
                        <button type="submit">Check</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    
<?php $this->load->view('include/footer_js');?>    
    
    <script>
        /*FORM VALIDATION*/                    
        $("#form").validate({
            rules: {                
                'otp': {required: true}, 
            },
            messages: {
                'otp': "Please enter your otp", 
            },
            submitHandler: function(form) {
                var email = $('.email').val();
                var otp = $('.otp').val();

                $.ajax({
                    url: '<?php echo base_url('login/check_otp');?>',
                    type: "POST",
                    data: {email:email,otp:otp}, // serializes the form's elements.
                    dataType:'json',
                    success: function(response) {
                        if (response.error == false){
                            $(".success_message").html('<div class="col-md-12 text-center offset4 alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong> Error </strong>' + response.message + '</div>');
                        }
                        else{
                            window.location.href = '<?= base_url('login/reset-password/')?>';
                        }
                    }
                });
            }
        });
    </script>

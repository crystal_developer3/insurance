<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Agent_details extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			
		}
		
		public function index($id)
		{
			$data= array();
			// $where['id']=$this->input->post('id');
			$where['id']=$id;
			$user_details = $this->production_model->get_all_with_where('user','','',$where);
			if($user_details > 0){
				$data['user_details'] = $user_details[0];
			}
			$where1['agent_id']=$id;
			$where1['status']=1;
			$data['user_reviews']= user_review_front($where1);
			$types = explode(",", $user_details[0]['types_of_insurance']);
			foreach ($types as &$i) $i = (int) $i;
			$this->db->where_in("id",$types);
			$data['types_of_insurance_data'] = $this->production_model->get_all_with_where('types_of_insurance','','',array());
			$where1['user_id']=$this->session->userdata('login_id');
			$data['agency_connected'] = $this->production_model->get_all_with_where('selected_agency','','',$where1);
			// echo "<pre>";print_r($data['user_reviews']);exit;
			$this->load->view('agent_details',$data);
		}
		public function review($agent_id=""){
			$data = array();
			if(!empty($agent_id) && $agent_id != ""){
				$data['agent_id'] =$agent_id;
			}
			// if($this->input->post()){
			// 	$data = $this->input->post();
			// 	if(!empty($agent_id) && $agent_id != ""){
			// 		$data['agent_id'] =$agent_id;
			// 	}
			// 	// echo "<pre>";print_r($data);exit;
			// }
			$this->load->view('review',$data);
		}	
		public function addreview(){
			$data = array();
			if (!$this->input->is_ajax_request()) {
	            exit('No direct script access allowed');
	        }

			if($this->input->post()){
				$data = $this->input->post();
				$data['user_id'] =$this->session->userdata('login_id');

				$record=$this->production_model->insert_record('user_review',$data);
				if($record){
					echo json_encode(returnResponse('Success','Review added successfully!!',$data['agent_id']));
				}else{
					echo json_encode(returnResponse('Fail','Review not added !!',array($data['agent_id'])));
				}
			}
		}		
	}
?>
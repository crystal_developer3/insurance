<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body class="tg-home tg-homeone">
    <!--Wrapper Start-->
    <div id="tg-wrapper" class="tg-wrapper tg-haslayout">       
        <main id="tg-main" class="tg-main tg-haslayout">
            <div class="tg-sectionspace tg-haslayout">
              <div class="container">
                <div class="contact-form">
                  <form id="form" class="tg-formtheme tg-formcontactus" method="post" enctype="multipart/form-data" action="<?= base_url('contact_us/add')?>">
                    <?php $this->load->view('authority/common/messages');?>
                    <fieldset>
                      <div class="form-group">
                          <label for="category_id" class="col-form-label">Category <span class="required">*</span></label>
                          <div class="">
                              <select class="form-control" id="category_id" name="category_id">
                                  <option>--Main Menu--</option>
                                  <?php 
                                      $p_and_c = p_and_c();
                                      if (isset($p_and_c) && $p_and_c != null) {
                                          foreach ($p_and_c as $key => $value) { ?>
                                              <option value="<?=$value['id']?>" <?=(isset($details[0]['category_id']) && $value['id']==$details[0]['category_id'])?'selected':''?>><?=$value['title']?></option>
                                              <?php        
                                          }
                                      }
                                  ?>
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="product_id" class="col-form-label">Product <span class="required">*</span></label>
                          <div class="">
                              <select class="form-control" id="product_id" name="product_id">
                                  <option>--Product--</option>
                                  <?php 
                                      $p_and_c = p_and_c();
                                      if (isset($p_and_c) && $p_and_c != null) {
                                          foreach ($p_and_c as $key => $value) { ?>
                                              <option value="<?=$value['id']?>" <?=(isset($details[0]['product_id']) && $value['id']==$details[0]['product_id'])?'selected':''?>><?=$value['title']?></option>
                                              <?php        
                                          }
                                      }
                                  ?>
                              </select>
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="product_id" class="col-form-label">Preferred Language <span class="required">*</span></label>
                          <div class="">
                            <?php 
                              $language = language();
                              // print_r($language);exit;
                              foreach ($language as $key => $value) { ?>
                                <input type="checkbox" class="preferred_language" name="preferred_language[]" value="<?=$value['id']?>"> <?=$value['title']?>
                                <!-- <label for="" ><?=$value['title']?></label> -->
                              <?php
                              }
                            ?>
                              <!-- <select class="form-control" id="preferred_language" name="preferred_language" multiple="">
                                  <option>--Preferred Language--</option>
                                  <?php 
                                      $language = language();
                                      if (isset($language) && $language != null) {
                                          foreach ($language as $key => $value) { ?>
                                              <option value="<?=$value['id']?>" <?=(isset($details[0]['language_id']) && $value['id']==$details[0]['language_id'])?'selected':''?>><?=$value['title']?></option>
                                              <?php        
                                          }
                                      }
                                  ?>
                              </select> -->
                          </div>
                      </div>
                      <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name*">
                        <?= form_error("name", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email Address*">
                            <?= form_error("email", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile number*">
                            <?= form_error("mobile_number", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group tg-hastextarea">
                        <textarea class="form-control" name="message" id="message" rows="1" placeholder="Your Message*"></textarea>
                            <?= form_error("message", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="tg-btn tg-active check">Submit</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
        </main>
        
    </div>
<?php $this->load->view('include/footer_js');?>
<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            'name': {required: true}, 
            'subject': {required: true}, 
            'email': {required: true,email:true}, 
            'mobile_number': {required: true,minlength:10}, 
            'message': {required: true}, 
        },
        messages: {
            'name': {required:"Please enter last name"}, 
            'subject': {required:"Please enter subject"}, 
            'email': {required:"Please enter email",email:"Please enter valid email"}, 
            'mobile_number': {required:"Please enter mobile number",minlength:"Enter minimum 10 digits mobile number"}, 
            'message': {required:"Please type your message"}, 
        }
    });  
    $(document).on('click','.preferred_language',function(e){
        var id = [];        
        $(".preferred_language:checked").each(function() { 
              id.push($(this).val());
        });
        console.log(id);
    });

</script>
</body>
</html>
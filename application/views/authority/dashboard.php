<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->view('authority/common/header');
    $this->view('authority/common/sidebar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">                
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('authority/contact-us')?>">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Contacts</span>
                                <span class="info-box-number"><?=get_count('contact_us')?></span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <a href="<?= base_url('authority/home-slider')?>">
                        <div class="info-box mb-3">
                            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-image"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Sliders</span>
                                <span class="info-box-number"><?=get_count('home_slider')?></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>
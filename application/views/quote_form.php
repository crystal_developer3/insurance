<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>
<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Quote Form</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Quote Form</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <section class="contact-area ptb-100">
        <div class="container">
            <div class="contact-form2">
                <form id="form" action="" method="post">
                    <div class="row">
                        <?php $this->load->view('include/messages');?>                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="type_of_business">Type Of Business <span class="required">*</span></label>
                                <input type="text" name="type_of_business" id="type_of_business" class="form-control" placeholder="Type Of Business*" value="<?=(isset($type_of_business))?$type_of_business:''?>" readonly>
                                <?= form_error("name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group language-box">
                                <label class="language">Preferred Language <span class="required">*</span></label>
                                <?php 
                                  $language = language_front();
                                  // print_r($language);exit;
                                  foreach ($language as $key => $value) { ?>
                                    <label class="container-checkbox"><?=$value['title']?>
                                    <input type="checkbox" class="preferred_language" name="preferred_language[]" value="<?=$value['id']?>" > 
                                    <span class="checkmark"></span>
                                  <?php
                                  }
                                ?>
                                <label id="preferred_language[]-error" class="error" for="preferred_language[]"></label>
                                <?= form_error("preferred_language", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="first_name">First Name <span class="required">*</span></label>
                                <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*">
                                <?= form_error("first_name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="last_name">Last Name <span class="required">*</span></label>
                                <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*">
                                <?= form_error("last_name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="business_name">Business Name</label>
                                <input type="text" name="business_name" id="business_name" class="form-control" placeholder="Business Name">
                                <?= form_error("business_name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group pl-1">
                                <label for="email">Email <span class="required">*</span></label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email Address*">
                                <?= form_error("email", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="cell-phone-number">Mobile number <span class="required">*</span></label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile number*">
                            <?= form_error("mobile_number", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="business_structure_id">What is the Legal structure of business? <span class="required">*</span></label>
                                <select class="form-control " id="business_structure_id" name="business_structure_id" >
                                    <option value="0">-- Select Legal Structure Of Busssiness --</option>  
                                <?php 
                                  $business_structure = business_structure();
                                  // print_r($business_structure);exit;
                                  if (isset($business_structure) && $business_structure != null) {
                                      foreach ($business_structure as $key => $value) { ?>
                                          <option value="<?=$value['id']?>"><?=$value['title']?></option>
                                          <?php        
                                      }
                                  }
                                ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group ">
                                <label for="business_address">Business Address</label>
                                <textarea name="business_address" class="form-control" id="business_address" cols="30" rows="6" placeholder="Business Address"></textarea>
                                <?= form_error("business_address", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="primary_work">What is the primary type of work your business does? </label>
                                <input type="text" name="primary_work" id="primary_work" class="form-control" placeholder="What is the primary type of work your business does? ">
                                <?= form_error("primary_work", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="default-btn check">Send Message <span></span></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
    </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script>        

        var id = [];   
        /*FORM VALIDATION*/
        $("#form").validate({
            
            rules: {
                'first_name' :{required: true}, 
                'last_name' :{required: true}, 
                'business_name' :{required: true}, 
                'email': {required: true,email:true}, 
                'business_address' :{required: true}, 
                'business_structure_id' :{required: true},
                'mobile_number': {required: true,minlength:10}, 
                'preferred_language[]': {required: true},
            },
            messages: {
                'first_name': {required:"Please enter first_name"}, 
                'last_name': {required:"Please enter last_name"}, 
                'business_name': {required:"Please enter business name"}, 
                'email': {required:"Please enter email",email:"Please enter valid email"}, 
                'business_address': {required:"Please enter business address"}, 
                'business_structure_id': {required:"Please select business structure"}, 
                'mobile_number': {required:"Please enter mobile number",minlength:"Enter minimum 10 digits mobile number"}, 
                'preferred_language[]': {required:"Please select at least one language"}, 
            }
             
        }); 
            
        $(document).on('click','.preferred_language',function(e){
            $(".preferred_language:checked").each(function() { 
                  id.push($(this).val());
            });
            console.log(id);
        });
        var form = $( "#form" );
        form.validate();
        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });  
    </script>
</body>
</html>
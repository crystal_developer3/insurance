<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
    <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/summernote-bs4.css">
</head>
<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Agency Profile</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Agency Profile</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <section class="contact-area ptb-100">
        <div class="container">
            <div class="contact-form2">
                <form id="form" action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <?php $this->load->view('include/messages');?>
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <img src="<?=base_url(PROFILE_PICTURE.$user_details['image'])?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" class="rounded-circle img-responsive" height="150" width="150">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="name">Agenacy Name <span class="required">*</span></label>
                                <input type="text" name="agency_name" id="agency_name" class="form-control" placeholder="Agenacy Name" value="<?=(isset($user_details['agency_name'])?$user_details['agency_name']:'')?>">
                                <?= form_error("agency_name", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group pl-1">
                                <label for="email">Email <span class="required">*</span></label>
                                <input type="email" name="email" id="email" class="form-control" required  placeholder="Email" value="<?=(isset($user_details['email'])?$user_details['email']:'')?>" readonly title="email can't be changed">
                                <?= form_error("email", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="phone-number">Phone number <span class="required">*</span></label>
                                <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Contact Number" value="<?=(isset($user_details['phone_number'])?$user_details['phone_number']:'')?>" readonly title="phone number can't be changed">
                                <?= form_error("phone_number", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="website-url">Website Url</label>
                                <input type="text" id="website_url" name="website_url" class="form-control" placeholder="Website Url" value="<?=(isset($user_details['website_url'])?$user_details['website_url']:'')?>">
                                <?= form_error("website_url", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="zip_code">Zipcode</label>
                                <input type="text" id="website_url" name="zip_code" class="form-control" placeholder="Website Url" value="<?=(isset($user_details['zip_code'])?$user_details['zip_code']:'')?>">
                                <?= form_error("zip_code", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea type="text" name="address" id="address" class="form-control address" placeholder="Address" rows="6" cols="30"><?=(isset($user_details['address'])?$user_details['address']:'')?></textarea>
                                <?= form_error("address", "<label class='error'>", "</label>");?>
                            </div>
                        </div>

                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="about">About</label>
                                <textarea type="text" name="about" id="about" class="form-control about" placeholder="About" rows="6" cols="30"><?=(isset($user_details['about'])?$user_details['about']:'')?></textarea>
                                <?= form_error("about", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group ">
                                <label for="coverage">Coverage</label>
                                <textarea name="coverage" class="form-control coverage" id="coverage" cols="30" rows="6" placeholder="Coverage" ><?=(isset($user_details['coverage'])?$user_details['coverage']:'')?></textarea>
                                <?= form_error("coverage", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group pl-1">
                                <label for="types_of_insurance">Types Of Insurance</label>
                                <select data-placeholder="Types Of Insurance" multiple class="chosen-select" name="types_of_insurance[]">
                                    <option value=""></option>
                                    <?php 
                                    $temp_types = explode(',', $user_details['types_of_insurance']);

                                      foreach ($types_of_insurance_data as $key => $value) { ?>
                                        <option value="<?=$value['id']?>" <?=(in_array($value['id'], $temp_types))?'selected':'';?>><?=$value['title']?></option>
                                      <?php
                                      }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group ">
                                <label for="service_offer">Service Offer</label>
                                <textarea name="service_offer" class="form-control service_offer" id="service_offer" cols="30" rows="6" placeholder="Service Offer" ><?=(isset($user_details['service_offer'])?$user_details['service_offer']:'')?></textarea>
                                <?= form_error("service_offer", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 pl-1">
                            <div class="form-group">
                                <label for="working_hours">Working Hours</label>
                                <textarea name="working_hours" class="form-control working_hours" id="working_hours" cols="30" rows="6" placeholder="Working Hourse" ><?=(isset($user_details['working_hours'])?$user_details['working_hours']:'')?></textarea>
                                <?= form_error("working_hours", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="youtube_video_link">YouTube Video Link</label>
                                <input type="text" name="youtube_video_link" id="youtube_video_link"  class="form-control" style="opacity: 1;position: relative;padding-top: 3%" value="<?=(isset($user_details['youtube_video_link'])?$user_details['youtube_video_link']:'')?>">
                                <?= form_error("youtube_video_link", "<label class='error'>", "</label>");?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="image">Upload Image</label>
                                <input type="file" name="image" id="image" class="form-control" style="opacity: 1;position: relative;padding-top: 3%" accept="image/*">
                                
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Social Link</label>
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 col-sm-12 pl-2">
                                        <label for="facebook_link">Facebook</label>
                                        <input type="text" name="facebook_link" id="facebook_link" class="form-control" placeholder="Facebook Link" value="<?=(isset($user_details['facebook_link'])?$user_details['facebook_link']:'')?>">
                                        <?= form_error("facebook_link", "<label class='error'>", "</label>");?>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-12 pl-2">
                                        <label for="twitter_link">Twitter</label>
                                        <input type="text" name="twitter_link" id="twitter_link" class="form-control" placeholder="Twitter Link" value="<?=(isset($user_details['twitter_link'])?$user_details['twitter_link']:'')?>">
                                        <?= form_error("twitter_link", "<label class='error'>", "</label>");?>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-12 pl-2">
                                        <label for="linkedin_link">Linkedin</label>
                                        <input type="text" name="linkedin_link" id="linkedin_link" class="form-control" placeholder="Linkedin Link" value="<?=(isset($user_details['linkedin_link'])?$user_details['linkedin_link']:'')?>">
                                        <?= form_error("linkedin_link", "<label class='error'>", "</label>");?>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-12 pl-2">
                                        <label for="youtube_link">Youtube</label>
                                        <input type="text" name="youtube_link" id="youtube_link"  class="form-control" placeholder="Youtube Link" value="<?=(isset($user_details['youtube_link'])?$user_details['youtube_link']:'')?>">
                                        <?= form_error("youtube_link", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="default-btn check">Submit <span></span></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
    </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script src="<?= base_url()?>assets/authority/js/summernote-bs4.min.js"></script>
    <script>
        $(document).ready(function(){
            
            $('.about').summernote({
              height: 250,
              toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view'],
                ]
            });

            $('.coverage').summernote({
              height: 250,
              toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view'],
                ]
            });
            $('.service_offer').summernote({
              height: 250,
              toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view'],
                ]
            });
            $('.working_hours').summernote({
              height: 250,
              toolbar: [
                    //[groupname, [button list]]
                    ['style', ['bold', 'italic', 'underline']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view'],
                ]
            });
            
        });
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                'agency_name': {required: true}, 
                'email': {required: true,email:true}, 
                'phone_number': {required: true,minlength:10}, 
                // 'address': {required: true},
            },
            messages: {
                'agency_name': {required:"Please enter agency name"}, 
                 
                'email': {required:"Please enter email",email:"Please enter valid email"}, 
                'phone_number': {required:"Please enter mobile number",minlength:"Enter minimum 10 digits mobile number"},
                // 'address': {required:"Please type your address"},
                
            }
        });    
        var form = $( "#form" );
        form.validate();

        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });
    </script>
</body>
</html>
<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>
<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Generel Quote Form Auto</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Generel Quote Form Auto</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <section class="contact-area ptb-100">
        <div class="container">
            <div class="contact-form2">
                <form id="form" action="" method="post">
                    <div class="row">
                        <?php $this->load->view('include/messages');?>                        
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group language-box">
                                <input type="hidden" name="selected_item"  value="<?=(isset($selected_item))?$selected_item:''?>" readonly>
                                <label class="language">Preferred Language <span class="required">*</span></label>
                                <?php 
                                  $language = language_front();
                                  // print_r($language);exit;
                                  foreach ($language as $key => $value) { ?>
                                    <label class="container-checkbox"><?=$value['title']?>
                                    <input type="checkbox" class="preferred_language" name="preferred_language[]" value="<?=$value['id']?>" > 
                                    <span class="checkmark"></span>
                                  <?php
                                  }
                                ?>
                                <label id="preferred_language[]-error" class="error" for="preferred_language[]"></label>
                                <?= form_error("preferred_language", "<label class='error'>", "</label>");?>
                            </div>
                        </div>                                               
                        <div class="col-lg-12">
                                <div class="vechile-title  pl-2">
                                    <h4><b>Let's Add This Vehicle</b></h4>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="row mt-3">
                                    <div class="col-md-12 pl-3">
                                        <label for="email">What's the year, make, and model? <span class="required">*</span></label>
                                    </div>
                                    <div class="input-group control-group form_append after-add-more">
                                        
                                        <?php $this->load->view('general_quote_vehical');?>
                                        <div class="col-md-2 col-lg-2 col-sm-12">
                                            <div class="input-group-btn"> 
                                                <button class="btn add-more" type="button"><i class="fa fa-plus"></i> Add</button>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <div class="form-group">
                                                <label for="vin">What is the VIN for this vehicle? <span class="required">*</span></label>
                                                <input type="text" name="vin[]" class="form-control" placeholder="What is the VIN for this vehicle? ">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group pl-2">
                                    <button type="submit" class="check default-btn" ">Next</button>
                                    <!-- <a href="<?=base_url('')?>" class="default-btn">Next <span></span></a> -->
                                </div>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                </form>
                <div class="copy hide">
                  <div class="control-group input-group form_append" style="margin-top:10px">
                    <div class="row" style="width: 100%;">
                       <?php $this->load->view('general_quote_vehical');?>
                        <div class="col-md-3 col-lg-3 col-sm-12 pl-4">
                            <div class="input-group-btn"> 
                              <button class="btn btn-danger remove" type="button"><i class="fa fa-trash"></i> Remove</button>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <div class="form-group">
                                <label for="vin">What is the VIN for this vehicle? <span class="required">*</span></label>
                                <input type="text" name="vin[]" class="form-control" placeholder="What is the VIN for this vehicle? ">
                                
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        
        <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
    </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script>        

        var id = [];           
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {

                'year_id[]':{required: true},
                'make_id[]':{required: true},
                'models_id[]':{required: true},
                'vin[]':{required: true},
                'preferred_language[]': {required: true},
            },
            messages: {

                'year_id[]':{required:"Please enter year"},
                'make_id[]':{required:"Please enter make"},
                'models_id[]':{required:"Please enter models"}, 
                'vin[]':{required:"Please enter vin"},
                'preferred_language[]': {required:"Please select at least one language"},
                
            }
        }); 
        $(document).on('click','.preferred_language',function(e){
            $(".preferred_language:checked").each(function() { 
                  id.push($(this).val());
            });
            console.log(id);
        });
        var form = $( "#form" );
        form.validate();
        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });  
        $(document).on('change','.year_id',function(){
            var year_id = $(this).val();
            var container = $(this);
            $.ajax({
              type: "POST",
              data:{year_id:year_id},
              url: BASE_URL + 'general_quote_form_auto/get_make_list',
              async: false,
              success: function (response) {
                
                if (response) {
                    // $('.make_id').html(response);
                    container.closest('.form_append').find(".make_id").html(response);
                 }else{
                    container.closest('.form_append').find(".make_id").html("");
                    container.closest('.form_append').find(".models_id").html("");
                }
              },
            });
        }).trigger('change');
        $(document).on('change','.make_id',function(){
            var make_id = $(this).val();
            var container = $(this);
            // console.log($(this).closest('.form_append').find(".models_id").html());
            $.ajax({
              type: "POST",
              data:{make_id:make_id},
              url: BASE_URL + 'general_quote_form_auto/get_models_list',
              async: false,
              success: function (response) {
                
                if (response) {
                    console.log(response);
                    // $(this).closest('.form_append').find(".models_id").html()
                    // $('.models_id').html(response);
                    // $(this).closest('.form_append').find(".models_id:last").html('<option>123456</option>');
                    container.closest('.form_append').find(".models_id").html(response);

                    // console.log($(this).closest(".form_append").find('.models_id'));
                    
                 }else{
                    container.closest('.form_append').find(".models_id").html("");
                }
              },
            });
        }).trigger('change');

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
          $(".add-more").click(function(){ 
              var html = $(".copy").html();
              $(".after-add-more").after(html);
          });
          $("body").on("click",".remove",function(){ 
              $(this).parents(".control-group").remove();
          });
        });
    </script>
</body>
</html>
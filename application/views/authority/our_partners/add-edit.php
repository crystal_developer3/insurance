<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Our partners</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $action = ($details == null) ? base_url('authority/our_partners/add_partner') : '';
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open($action, $attributes);
                                ?>
                                    <input type="hidden" name="id" id="slider_id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    
                                    
                                    <div class="form-group row">
                                        <label for="inputName2" class="col-sm-3 col-form-label">Position</label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control only_digits',
                                                    'name' => 'position',
                                                    'maxlength' => '3',
                                                    'placeholder' => 'Enter position',
                                                    'value' => (isset($details[0]['position']) && $details[0]['position'] !=null ? $details[0]['position'] : ''),
                                                );
                                                echo form_input($attributes);
                                            ?>
                                            <?= form_error("position", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label">Image:<span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="file" name="image" class="form-control" accept="image/*">
                                            <span id="image_error"></span>
                                            <?= form_error("image", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <?php
                                        if (isset($details) && $details != null) {
                                        ?>
                                            <div class="form-group row">
                                                <label for="inputName3" class="col-sm-3 col-form-label">Current image</label>
                                                <div class="col-sm-9">
                                                    <img src="<?= base_url(PARTNERS_LOGO.'thumbnail/').$details[0]['image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">                
                                                </div>
                                            </div>
                                        <?php } 
                                    ?>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        var slider_id = $("#slider_id").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                image: { 
                    required: function(element) {
                        if (slider_id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },                  
            },
            messages: {     
                image: {required :"Image field is required"},       
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>
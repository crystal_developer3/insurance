<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Subscriber extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->subscriber_info = array();
            redirect(base_url('authority/subscriber'));
        } 
        $data = array();  
        $tmp_data = $this->production_model->count_num_of_rows('','subscriber',array()); 
        $tmp_array['total_record'] = $tmp_data;
        $tmp_array['url'] = base_url('authority/subscriber/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('subscriber','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        $this->load->view('authority/subscriber/view',$data);
    }
    function delete()
    {
        $id = $this->input->post('id');
        $record = $this->production_model->delete_record('subscriber',array('id'=>$id));
        if ($record == 1) {
            $response_array['success'] = true;
            $response_array['message'] = 'Deleted successful.';
        }
        echo json_encode($response_array); exit;
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        $record = $this->production_model->get_delete_where_in('subscriber','id',$chkbox_id);
        if ($record != 0) {
            $response_array['success'] = true;
            $response_array['message'] = 'Deleted successful.';
        }
        echo json_encode($response_array); exit;
    } 
    /* new letter send email*/
    function get_details()
    {
        $data = $this->input->post(); 

        if ($data['email'] !=null) {
            $details = $this->production_model->get_all_with_where('news_letter','','',array('id'=> $data['news_id']));
            if ($details !=null) {
                $data = array_merge($details[0],$data);
                // $this->load->view('mail_form/news_letter/form',$data);
                $send_mail = $this->production_model->mail_send(SITE_TITLE.' news-letter',$data['email'],'','mail_form/news_letter/form',$data,'');
                
                $this->session->set_flashdata('success', 'Mail send successfully.');
                redirect($_SERVER['HTTP_REFERER']);               
            }
        }
        else{
            $this->session->set_flashdata('error', 'Select one email address after submit.');
            redirect($_SERVER['HTTP_REFERER']);
        }     
    }
    function filter()
    {
        $this->session->subscriber_info = $_POST;
        $name = isset($this->session->subscriber_info['name']) ? $this->session->subscriber_info['name'] : '';
        if (isset($name) && $name !=null) {
            $this->db->group_start();
            $this->db->like('subscribe_email', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post(); 
        $tmp_data = $this->production_model->count_num_of_rows('','subscriber',array());
        $tmp_array['total_record'] = $tmp_data;
        $tmp_array['url'] = base_url('authority/subscriber/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name !=null) {
            $this->db->group_start();
            $this->db->like('subscribe_email', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('subscriber','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        ob_start();
        if (isset($filteredData) && !empty($filteredData) ) { 
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                    <tr>
                        <td style="width: 10px;">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id;?>" value="<?= $id?>">
                                <label for="customCheckbox<?= $id;?>" class="custom-control-label"></label>
                            </div>
                        </td>
                        <td><?= $key+1;?></td>
                        <td><?= $value['subscribe_email'];?></td>
                        <td>
                            <a href="javascript:void(0)" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id;?>"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php
            }   
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();                
            $response_array['pagination'] = $data['pagination'];                
        }else{
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">Records not found</td>
                                            </tr>'; 
            $response_array['pagination'] = '';                     
        }           
        echo json_encode($response_array); exit;
    } 
}
?>
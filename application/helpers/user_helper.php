<?php
	function user($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('user','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
?>
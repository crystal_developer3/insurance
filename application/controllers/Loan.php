<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Loan extends CI_Controller {
	public function index()
	{		
		$this->load->view('blog');
	}
	public function loan_details($slug){
		$condition = array('status'=>'1','seo_slug'=>$slug);
		$data['loan_details'] = loan_page($condition);

		$conditions = array('status'=>'1','loan_id'=>$data['loan_details'][0]['id']);
		$data['loan_faq_details'] = faq($conditions);
		// echo "<pre>";print_r($data);exit;
		$this->load->view('loan_details',$data);
	}
	
}
?>
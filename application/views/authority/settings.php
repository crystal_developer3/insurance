<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active"><?= $form_title; ?></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open("", $attributes);
                                ?>
                                    <?php
                                        if (isset($site_settings['row_count']) && $site_settings['row_count'] > 0) {
                                            foreach ($site_settings['data'] as $value) {
                                            ?>
                                                <div class="form-group row">
                                                    <label for="<?= $value['setting_key'];?>" class="col-sm-3 col-form-label"><?= $value['setting_key'];?> <?= ($value['is_required'] == 1) ? "<span class='required'>*</span>" : ""; ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="<?= $value['setting_key'];?>" id="<?= $value['setting_key'];?>" class="form-control" value="<?= $value['setting_value'];?>">
                                                        <?= form_error($value['setting_key'], "<label class='error'>", "</label>"); ?>
                                                    </div>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<?php $this->view('authority/common/copyright'); ?>
<script>    
    // $(document).ready(function(){
    //     /*FORM VALIDATION*/
    //     $("#form1").validate({
    //        rules: {
    //            designation: "required",
    //        },
    //        messages: {
    //            designation: "Please enter designation",
    //        }
    //     });
    // });
</script>
<?php $this->view('authority/common/footer'); ?>
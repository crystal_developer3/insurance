<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact_us extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        if ($this->input->get('clear-search') == 1) {
            $this->session->contact_info = array();
            redirect(base_url('authority/contact-us'));
        } 
        $data = array();  
        $tmp_data = $this->production_model->count_num_of_rows('','contact_us',array()); 
        $tmp_array['total_record'] = $tmp_data;
        $tmp_array['url'] = base_url('authority/contact-us/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);
        $data['details'] = $this->production_model->get_all_with_where_limit('contact_us','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        $this->load->view('authority/contact_us/view',$data);
    }
    function delete()
    {
        $id = $this->input->post('id');
        $record = $this->production_model->delete_record('contact_us',array('id'=>$id));
        if ($record == 1) {
            $response_array['success'] = true;
            $response_array['message'] = 'Deleted successful.';
        }
        echo json_encode($response_array); exit;
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        $record = $this->production_model->get_delete_where_in('contact_us','id',$chkbox_id);
        if ($record != 0) {
            $response_array['success'] = true;
            $response_array['message'] = 'Deleted successful.';
        }
        echo json_encode($response_array); exit;
    } 
    function filter()
    {
        $this->session->contact_info = $_POST;
        $name = isset($this->session->contact_info['name']) ? $this->session->contact_info['name'] : '';
        if (isset($name) && $name !=null) {
            $this->db->group_start();
            $this->db->like('email', $name);
            $this->db->or_like('name', $name);
            $this->db->or_like('mobile_number', $name);
            $this->db->or_like('message', $name);
            $this->db->group_end();
        }
        $data[] = $this->input->post(); 
        $tmp_data = $this->production_model->count_num_of_rows('','contact_us',array());
        $tmp_array['total_record'] = $tmp_data;
        $tmp_array['url'] = base_url('authority/contact-us/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->production_model->only_pagination($tmp_array);

        if (isset($name) && $name !=null) {
            $this->db->group_start();
            $this->db->like('email', $name);
            $this->db->or_like('name', $name);
            $this->db->or_like('mobile_number', $name);
            $this->db->or_like('message', $name);
            $this->db->group_end();
        }
        $filteredData = $this->production_model->get_all_with_where_limit('contact_us','id','desc',array(),$record['limit'],$record['start']); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        ob_start();
        if (isset($filteredData) && !empty($filteredData) ) { 
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                    <tr>
                        <td style="width: 10px;">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id;?>" value="<?= $id?>">
                                <label for="customCheckbox<?= $id;?>" class="custom-control-label"></label>
                            </div>
                        </td>
                        <td><?= $key+$record['no'];?></td>
                        <td><?= $value['name'];?></td>
                        <td><?= $value['email'];?></td>
                        <td><?= $value['mobile_number'];?></td>
                        <td><?= $value['message'];?></td>
                        <td>
                            <a href="javascript:void(0)" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id;?>"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                <?php
            }   
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();                
            $response_array['pagination'] = $data['pagination'];                
        }else{
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="7" align="center">Records not found</td>
                                            </tr>'; 
            $response_array['pagination'] = '';                     
        }           
        echo json_encode($response_array); exit;
    } 
}
?>
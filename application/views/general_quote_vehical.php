<div class="col-md-3 col-lg-3 col-sm-12 pl-3">
    <div class="form-group">
        <label for="year_id">Year <span class="required">*</span></label>
        <select class="form-control year_id"  name="year_id[]" >
            <option>-- Select Year --</option>
            <?php 
                $year = year();
                if (isset($year) && $year != null) {
                    foreach ($year as $key => $value) { ?>
                        <option value="<?=$value['id']?>" <?=(isset($details[0]['year_id']) && $value['id']==$details[0]['year_id'])?'selected':''?>><?=$value['title']?></option>
                        <?php        
                    }
                }
            ?>
        </select>
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-12 pl-2">
    <div class="form-group">
        <label for="make_id">Make <span class="required">*</span></label>
        <select class="form-control make_id" name="make_id[]">
            <option>-- Select Make --</option>
        </select>
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-12 pl-1">
    <div class="form-group">
        <label for="models_id">Model <span class="required">*</span></label>
        <select class="form-control models_id" name="models_id[]" >
            <option>-- Select Model --</option>
        </select>
    </div>
</div>
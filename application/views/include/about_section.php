<!-- <section class="tg-about-ustg-haslayout tg-sectionspace">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="tg-sectionhead">
          <h2>Author</h2>
        </div>
        <?php
          if (isset($author_details) && $author_details !=null) { ?>

            <div class="tg-authordetail">
              <div class="col-md-6">
                <figure class="tg-authorimg">
                  <div class="images">
                    <img src="<?= base_url(AUTHOR_IMAGE.$author_details[0]['image'])?>" alt="image description">
                  </div>
                </figure>
              </div>
              <div class="col-md-6">
                <figure class="tg-authorimg">
                  <div class="images">
                    <img src="<?= base_url(AUTHOR_IMAGE.'author2.jpg')?>" alt="image description">
                  </div>
                </figure>
              </div>
            </div>
            
            <div class="tg-description" style="margin-top: 5rem;">
              <?=$author_details[0]['description']?>
              <?php
                $ci = & get_instance();
                $link = $ci->router->fetch_class();
                if ($link!='author') { ?>
                    <div class="tg-btns">
                      <a class="tg-btn tg-active" href="<?=base_url('author')?>">Read More</a>
                    </div>
                    <?php
                  }
                ?>
            </div>
            
            <?php 
          }
        ?>
      </div>
    </div>
  </div>
</section> -->

<section class="about-area ptb-100 bg-f8f8f8">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
                <div class="about-title">
                    <span>About Us</span>
                    <h2><?=(isset($about_details) && !empty($about_details))?$about_details[0]['title']:''?></h2>
                        
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="about-text">
                    <p>
                      <?php
                        if(isset($about_details) && !empty($about_details)){
                          $content = strip_tags($about_details[0]['description']);
                          echo strlen($content) > 160 ? substr($content, 0, 160) . "..." : $content;
                        }
                      ?>
                    </p>

                    <a href="<?=base_url('about')?>" class="read-more-btn">More About Us <i class="flaticon-right-chevron"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

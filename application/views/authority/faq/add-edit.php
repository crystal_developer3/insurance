<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Faq</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    //$action = ($details == null) ? base_url('authority/faq/add_faq') : '';
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open("", $attributes);
                                ?>
                                    <input type="hidden" name="id" id="id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">

                                   
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-12 col-form-label">Question<span class="required">*</span></label>
                                        <div class="col-sm-12">
                                            <input type="text" name="question" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['question']) && $details[0]['question'] !=null ? $details[0]['question'] : '';?>">
                                            <span id="question_error"></span>
                                            <?= form_error("question", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName3" class="col-sm-12 col-form-label">Answer<span class="required">*</span></label>
                                        <div class="col-sm-12">
                                            <textarea class="textarea" name="answer" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= ($details != null) ? $details[0]['answer'] : '';?></textarea>
                                            <span id="answer_error"></span>
                                            <?= form_error('answer', "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-12">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        $('.textarea').summernote()
        var id = $("#id").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            ignore: [],
            rules: {
                question: {required:true}, 
                answer: {required:true},  
            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                error.appendTo($("#" + name + "_error"));
            },
            messages: {     
                question: {required :"Please enter title"}, 
                answer:{required:"Please enter description"},  

            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>
<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <?php
                                $profile_photo = $this->production_model->get_all_with_where('administrator','','',array('id'=>$this->session->user_info['id']));
                                if ($profile_photo != null) {
                                ?>
                                    <div class="text-center">
                                        <label>Current photo</label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?= $profile_photo !=null ? base_url(PROFILE_PICTURE).$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>

                        </div>
                         <div class="card card-primary card-outline" style="background-color: #343a40">
                            <div class="card-body box-profile">
                                <?php
                                    $white_logo = $this->production_model->get_all_with_where('administrator','','',array('id'=>$this->session->user_info['id']));
                                    if ($white_logo != null) {
                                    ?>
                                        <div class="text-center">
                                            <label style="color: white">White Logo</label><br>
                                            <img class="profile-user-img img-fluid img-circle" src="<?= $white_logo !=null ? base_url(PROFILE_PICTURE).$white_logo[0]['white_logo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" alt="profile picture">
                                        </div>
                                    <?php } 
                                ?>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open("", $attributes);
                                ?>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Name <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php 
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'name' => 'full_name',
                                                    'placeholder' => 'Enter full name',
                                                    'value' => (isset($full_name) ? $full_name : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("full_name", "<div class='error'>", "</div>"); 
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-3 col-form-label">Email address <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'email',
                                                    'class' => 'form-control',
                                                    'name' => 'email_address',
                                                    'placeholder' => 'Enter email address',
                                                    'value' => (isset($email_address) ? $email_address : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("email_address", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName2" class="col-sm-3 col-form-label">Mobile number <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control',
                                                    'name' => 'mobile_number_1',
                                                    'placeholder' => 'Enter mobile number',
                                                    'maxlength' =>'15',
                                                    'value' => (isset($mobile_number_1) ? $mobile_number_1 : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("mobile_number_1", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputExperience" class="col-sm-3 col-form-label">Mobile number2</label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'text',
                                                    'class' => 'form-control only_digits',
                                                    'name' => 'mobile_number_2',
                                                    'placeholder' => 'Enter mobile number',
                                                    'maxlength' =>'15',
                                                    'value' => (isset($mobile_number_2) ? $mobile_number_2 : ""),
                                                );
                                                echo form_input($attributes);
                                                echo form_error("mobile_number_2", "<div class='error'>", "</div>");
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputSkills" class="col-sm-3 col-form-label">Address</label>
                                        <div class="col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'class' => 'form-control',
                                                    'name' => 'address',
                                                    'placeholder' => 'Enter address',
                                                    'rows' => '5',
                                                    'value' => (isset($address) ? $address : ""),
                                                );
                                                echo form_textarea($attributes);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label">Profile photo:</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <?php
                                                        $attributes = array(
                                                            'type' => 'file',
                                                            'name' => 'profile_photo',
                                                            'class' => 'custom-file-input',
                                                            'accept' => 'image/*',
                                                        );
                                                        echo form_input($attributes);
                                                    ?>
                                                    <label class="custom-file-label" for="exampleInputFile">Upload photo</label>
                                                </div>
                                            </div>
                                            <?php 
                                                echo form_error("profile_photo", "<div class='error'>", "</div>");
                                                if (isset($profile_photo_error) && $profile_photo_error != "") {
                                                    echo "<div class='error'>" . $profile_photo_error . "</div>";
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                            <label for="white_logo" class="col-sm-3 col-form-label">White Logo:<!-- (Upload by 200&#215;50) --></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <?php
                                                            $attributes = array(
                                                                'type' => 'file',
                                                                'name' => 'white_logo',
                                                                'class' => 'custom-file-input',
                                                                'accept' => 'image/*',
                                                            );
                                                            echo form_input($attributes);
                                                            echo form_error("white_logo", "<div class='error'>", "</div>");
                                                            if (isset($white_logo_error) && $white_logo_error != "") {
                                                                echo "<div class='error'>" . $white_logo_error . "</div>";
                                                            }
                                                        ?>
                                                        <label class="custom-file-label" for="exampleInputFile">Upload logo</label>
                                                    </div>
                                                    <!-- <div class="input-group-append">
                                                        <span class="input-group-text" id="">Upload</span>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function () {
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                full_name: {required:true},
                email_address: {required: true, email: true},
                mobile_number_1: {required: true, maxlength: 15, minlength: 10},
            },
            messages: {
                full_name: {required: "Please enter full name"},
                email_address: {required: "Please enter email address", email: "Please enter valid email address"},
                mobile_number_1: {required: "Please enter mobile number", maxlength: "Please enter no more than 15 digits", minlength: "Please enter at least 10 digits"},
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>
<div class="footer">
    <div class="col-sm-3 ft1">
        <div class="foot1">
            <h4>Links</h4>
            <ul>
                <li><a href="<?= base_url();?>"><?= $this->lang->line('home');?></a></li>
                <li><a href="<?= base_url('about-us');?>"><?= $this->lang->line('about_us');?></a></li>
                <li><a href="#">Brochure</a></li>
                <li><a href="blog.html">Blog </a></li>
                <li><a href="<?= base_url('faq');?>"><?= $this->lang->line('faq');?> </a></li>
                <li><a href="contactus.html">Contact Us </a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="foot1">
            <h4>Quick Links</h4>
            <ul>
                <li><a href="<?= base_url('treatments');?>"><?= $this->lang->line('treatments');?></a></li>
                <li><a href="<?= base_url('accommodation');?>"><?= $this->lang->line('accommodation');?></a></li>
                <li><a href="Apply-admission.html">Admission</a></li>
                <li><a href="#">Services </a></li>
                <li><a href="<?= base_url('gallery');?>"><?= $this->lang->line('gallery');?> </a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="foot1">
            <h4>Explore</h4>
            <ul>
                <li><a href="demo.html">Rules</a></li>
                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                <li><a href="Terms-conditions.html">Terms & Conditions</a></li>
                <li><a href="declimer.html">Disclaimer </a></li>
                <li><a href="#">Sitemap </a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-3 ft2">
        <div class="foot1">
            <h4>Email Updates</h4>
            <p>Be the first to hear about our offers and announcements</p>
            <form class="form-widget" method="post" id="form_tmp">
                <div class="field-group subscribe-form email0">
                    <input type="email" name="subscribe_email" class="form-control fot-email pull-left" placeholder="Enter Your Email">
                    <span class="subscribe_message" style="color: #e81123;"></span>
                    <button type="button" id="subscribe_form" class="btn pull-right email1"><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="msg-wrp"></div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="footer-buttom">
    <div class="col-sm-6 pull-left"> &copy; Copyrights @ <?= SITE_TITLE;?></div>
    <div class="col-sm-6 pull-right fot2"> <a href="#">Privacy Policy</a> &nbsp;|&nbsp; <a href="Terms-conditions.html">Tems & Conditions</a></div>
    <div class="clearfix"></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/js/aos.js"></script>
<script src="<?= base_url()?>assets/js/baguetteBox.min.js"></script>
<script src="<?= base_url()?>assets/js/custom.js"></script>

<!--new added -->
<script src="<?= base_url(); ?>assets/validation/common.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="<?= base_url(); ?>assets/validation/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/address.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.loading').css({'display':'none'});
        
        /*FORM VALIDATION*/
        $("#form_tmp").validate({
            rules: {
                subscribe_email: {required: true, email: true},
            },
            messages: {
                subscribe_email: {required: "Please enter email address", email: "Please enter valid email address"},
            }
        });
        $('#subscribe_form').click(function(){
            $("#form_tmp").valid();
            var email = $('input[name="subscribe_email"]').val(); 
            if ($("#form_tmp").valid()){
                $.ajax({
                    url: "<?php echo base_url('home/add_subscribe')?>",
                    data: {subscribe_email:email},
                    type: "POST",
                    dataType:'json',                    
                    success: function(response) {
                        if (response.validation_error) {
                            $('.subscribe_message').text(response.message).css({'font-size': '14px'});
                        }
                        if (response.email_allredy) {
                            $('.subscribe_message').text(response.message).css({'font-size': '14px'});
                        }
                        if (response.success) {
                            $('.subscribe_message').text(response.message).css({'font-size': '14px','color':'green'});
                            $('input[name="subscribe_email"]').val('');
                        }
                        if (response.error){
                            $('.subscribe_message').text(response.message).css({'font-size': '14px'});
                        }
                    }
                });
            }          
        });

        /*language selection*/
        $(".language_change").change(function(){
            var languageid = $(this).val();
            $.ajax({
                url: "<?= base_url('language/setlanguage')?>",
                type: 'POST',
                dataType: 'html',
                data: {language_type:languageid},
                success: function(data) {
                   window.location.reload();
                }
            });
        });
    });
</script>
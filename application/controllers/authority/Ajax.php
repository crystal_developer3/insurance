<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* This class is use for all the common ajax request.
*/
class Ajax extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }
    public function change_status(){ 
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);
			$response_array['success'] = true;
		}
		echo json_encode($response_array);exit;
	}
	public function change_category_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);
			$this->production_model->update_record('blog_image', $records, array("category_id" => $this->input->post('id'))); 
			$response_array['success'] = true;
		}
		echo json_encode($response_array);exit;
	}
	public function change_image_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			/*Check parent category is active or not*/
			if ($this->input->post('parent_id') !=null) {
				$get_details = $this->production_model->get_all_with_where('blog_category','','',array("id" => $this->input->post('parent_id'))); 
				if (isset($get_details) && $get_details !=null) {
					if ($get_details[0]['status'] == '0') {
						$response_array['error'] = true;
					}
					else{
						$this->common_model->update_data($this->input->post('table'), $records, $conditions);
						$response_array['success'] = true;
					}
				}
			}
			$response_array['success'] = true;
		}
		echo json_encode($response_array);exit;
	}
	
}
?>
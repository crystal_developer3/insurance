<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Author</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-primary card-outline">   
                        <div class="card-body box-profile">
                            <?php
                                if ($details != null) {
                                ?>
                                    <div class="text-center">
                                        <label>Front Image</label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?= base_url(ABOUT_IMAGE).$details[0]['image']?>" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                        <div class="card-body box-profile">
                            <?php
                                if ($details != null) {
                                ?>
                                    <div class="text-center">
                                        <label>Back Image</label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?= base_url(ABOUT_IMAGE).$details[0]['back_image']?>" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-8">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $action = ($details == null) ? base_url('authority/author/add_author') : '';
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open($action, $attributes);
                                ?>
                                    <input type="hidden" name="id" id="id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-12 col-form-label">Title <span class="required">*</span></label>
                                        <div class="col-sm-12">
                                            <input type="text" name="title" placeholder="Enter title" class="form-control" value="<?= isset($details[0]['title']) && $details[0]['title'] !=null ? $details[0]['title'] : '';?>">
                                            <span id="title_error"></span>
                                            <?= form_error("title", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName3" class="col-sm-12 col-form-label">Description<span class="required">*</span></label>
                                        <div class="col-sm-12">
                                            <textarea class="textarea" name="description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= ($details != null) ? $details[0]['description'] : '';?></textarea>
                                            <?= form_error('description', "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label">Front Image </label>
                                        <div class="col-sm-9">
                                            <input type="file" name="image" class="form-control" accept="image/*">
                                            <span id="image_error"></span>
                                            <?= form_error("image", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="profile_photo" class="col-sm-3 col-form-label">Back Image </label>
                                        <div class="col-sm-9">
                                            <input type="file" name="back_image" class="form-control" accept="image/*">
                                            <span id="back_image_error"></span>
                                            <?= form_error("back_image", "<label class='error'>", "</label>");?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<script>
    $(document).ready(function(){
        $('.textarea').summernote();
        var id = $("#id").val();
        /*FORM VALIDATION*/
        $("#form").validate({
            ignore: [],
            rules: {
                title: {required:true}, 
                description: {required:true},  
                
            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                if (name == 'description'){
                    error.insertAfter($('.note-editor'));
                } else {
                    error.appendTo($("#" + name + "_error"));
                }
            },
            messages: {     
                title: {required :"Please enter title"}, 
                description:{required:"Please enter description"},  
                image:{required:"This fields is required"},   
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>
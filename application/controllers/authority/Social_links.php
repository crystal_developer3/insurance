<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Social_links extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        $data['details'] = $this->production_model->get_all_with_where('social_links','','',array());
        $this->load->view('authority/social_links/view',$data);
    }

    function add()
    {
        $data['details'] = array();
        $this->load->view('authority/social_links/add-edit',$data);
    }

    function add_links()
    {
        $data = $this->input->post();        
        $record = $this->production_model->insert_record('social_links',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'Added successful.');
            redirect(base_url('authority/social-links')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Not added.');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data = $this->input->post();
        $data['details'] = $this->production_model->get_all_with_where('social_links','','',array('id'=>$id));
        if ($this->input->method() == 'post') {
            unset($data['details']);
            $record = $this->production_model->update_record('social_links',$data,array('id'=>$data['id']));
            if ($record == 1) {
                $this->session->set_flashdata('success', 'Update successful.');
                redirect(base_url('authority/social-links'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Not updated.');
                redirect($_SERVER['HTTP_REFERER']);
            } 
        }
        $this->load->view('authority/social_links/add-edit',$data);
    }
    function delete($id)
    {        
        $record = $this->production_model->delete_record('social_links',array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Deleted successful.');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Not deleted.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
?>
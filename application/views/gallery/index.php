<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>
<section id="banner">
    <div class="container-fluid">
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key1 => $value1) {
                        ?>
                            <li data-target="#myCarousel" data-slide-to="<?= $key1;?>" class="<?= $key1==0 ? 'active' : '';?>"></li>
                        <?php } 
                    }
                ?>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <?php
                    if (isset($slider_details) && $slider_details !=null) {
                        foreach ($slider_details as $key2 => $value2) {
                        ?>  
                            <div class="item <?= $key2==0 ? 'active' : '';?>">
                                <img src="<?= base_url(GALLERY_SLIDER.$value2['image']);?>" alt="Chania">
                                <div class="carousel-caption">
                                    <h3><?= $value2['title'.get_language()];?></h3>
                                </div>
                            </div>
                        <?php } 
                    }
                ?>
            </div>
            <!-- /.carousel-inner -->
            <!-- Controls -->
            <div class="control-box">
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="control-icon prev fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="control-icon next fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.control-box -->
        </div>
    </div>
</section>
</div>
<div class="clearfix"></div>
<div class="container">
    <div class="row">
        <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="gallery-title" align="center" style="text-transform: uppercase;"><?= $this->lang->line('gallery');?></h1>
        </div>
        <div align="center" class="photo-tab-">
            <button class="btn btn-default filter-button" data-filter="all">All</button>
            <?php
                if (isset($gallery_category_details) && $gallery_category_details !=null) {
                    foreach ($gallery_category_details as $key3 => $value3) { ?>
                        <button class="btn btn-default filter-button" data-filter="<?= $value3['id'];?>"><?= $value3['title'.get_language()];?></button>
                        <?php 
                    }
                } 
            ?>
                <!-- <button class="btn btn-default filter-button" data-filter="2">TREATMENTS</button>
                <button class="btn btn-default filter-button" data-filter="3">FACILITIES</button>
                <button class="btn btn-default filter-button" data-filter="4">EVENTS</button> -->
        </div>
        <br/>
        <div class="tz-gallery gallery" id="gallery">
        	 <?php
                if (isset($details) && $details !=null) {
                    foreach ($details as $key4 => $value4) {
                    ?>  
			            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter <?= $value4['category_id'];?>">
			                <a class="lightbox" href="<?= base_url(GALLERY.$value4['image']);?>">
			                <img src="<?= base_url(GALLERY.$value4['image']);?>" alt="<?= $value4['title'.get_language()];?>" class="img-responsive">
			                </a>
			                <span><?= $value4['title'.get_language()];?></span>
			            </div>
			        <?php } 
			    }
			?>
        </div>
        <div class="clearfix"></div>
        <br>
    </div>
</div>
<div class="clearfix"></div>
<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>
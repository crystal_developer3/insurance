<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= SITE_TITLE;?></title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/authority/images/favicon-icon.ico" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/authority/images/favicon-icon.ico" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/authority/images/favicon-icon.ico" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/icheck-bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <!-- New added -->
        <link rel="stylesheet" href="<?= base_url()?>assets/css/developer.css">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <?php
                    $profile_photo = $this->production_model->get_all_with_where('administrator','','',array());
                ?>   
                <img src="<?= $profile_photo !=null ? base_url(PROFILE_PICTURE).$profile_photo[0]['profile_photo'] : base_url('assets/uploads/default_img.png')?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="100" width="100">
            </div>
            <?php 
                if (isset($success) && $success != "") {
                    echo "<h5 class='text-success'>" . $success . "</h5>";
                }
            ?>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>
                    <form action="" method="POST" id="form">
                        <div class="input-group mb-3">
                            <?php
                                $attributes = array(
                                    'type' => 'email',
                                    'class' => 'form-control',
                                    'name' => 'email_address',
                                    'placeholder' => 'Enter email address',
                                    'value' => (isset($email_address) ? $email_address : ""),
                                );
                                echo form_input($attributes);
                            ?>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <?php echo form_error('email_address', '<label class="error">', '</label>'); ?>
                        <span id="email_address_error"></span>

                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary btn-block">Reset password</button>
                            </div>
                        </div>
                    </form>
                    <p class="mt-3 mb-1"><a href="<?= base_url('authority');?>">Login</a></p>
                </div>
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="<?= base_url()?>assets/authority/js/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<?= base_url()?>assets/authority/js/bootstrap.bundle.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url()?>assets/authority/js/adminlte.min.js"></script>

        <!-- New added -->
        <script type="text/javascript" src="<?= base_url(); ?>assets/validation/jquery.validate.js"></script>
        <script>
            $(document).ready(function() {
                /*FORM VALIDATION*/
                $("#form").validate({
                    rules: {
                        email_address: {required: true},
                    },
                    errorPlacement: function (error, element) {
                        var name = $(element).attr("name");
                        error.appendTo($("#" + name + "_error"));
                    },
                    messages: {
                        email_address: {required: "Please enter email address"},
                    }
                });
            });
        </script>
    </body>
</html>
<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <!--Wrapper Start-->
    <?php /*<div id="tg-wrapper" class="tg-wrapper tg-haslayout">       
        <main id="tg-main" class="tg-main tg-haslayout">
            <div class="tg-sectionspace tg-haslayout">
              <div class="container">
                <h2 align="center">Login</h2>
                <div class="contact-form">
                  <form id="form" class="tg-formtheme tg-formcontactus" method="post" enctype="multipart/form-data" action="">
                    <?php $this->load->view('authority/common/messages');?>
                    <fieldset>
                      
                      <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email Address*" value="<?=(isset($email)?$email:'')?>">
                            <?= form_error("email", "<label class='error'>", "</label>");?>
                      </div>
                      <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password*">
                        <?= form_error("password", "<label class='error'>", "</label>");?>
                      </div>
                      <label for="remember">
                          <input id="remember" type="checkbox">
                          Remember me
                      </label>

                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                      
                      <div class="form-group">
                        <button type="button" data-toggle="modal" data-target="#ForgetPassword">Lost your password?</button>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
        </main>
        
    </div>*/?>

    <!-- forgot password start-->
    <!-- <div id="ForgetPassword" class="modal fade" role="document" tabindex='-1' style="margin-top: 5%;">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <h6 class="modal-title">Recover your password</h6>
              </div>

              <div class="modal-body">
                  <form method="post">
                      <div class="form-group" style="margin: 0 auto;">
                        <input type="tesxt" name="UserEmail" id="UserEmail" class="form-control" placeholder="Email Address">
                        <label id="UserEmail-error" class="text-danger"></label>
                      </div>
                      <div class="form-group mb-0">
                          <button type="submit" class="btn-md btn-block btn btn-success frgt_pwd_check" formaction="<?= base_url('login/forgot_password')?>">Send Me Email</button>
                      </div>
                  </form>
              </div>
            </div>
        </div>
    </div> -->

    <!-- Login Content Strat Here -->
    <section class="hero-section full-screen gray-light-bg">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-md-7 col-lg-6 col-xl-8 d-none d-lg-block">
                    <div class="bg-cover vh-100 ml-n3 gradient-overlay" style="background: url(<?=base_url('assets/img/achievements-img2.jpg')?>);">
                        
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div class="login-signup-wrap px-4 my-5">
                        <h1>Sign In</h1>
                          <form id="loginForm" class="login-signup-form" method="post" enctype="multipart/form-data" action="">
                            <?php $this->load->view('authority/common/messages');?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email " placeholder="Email " value="<?=(isset($email)?$email:'')?>">
                                        <div class="help-block with-errors"></div>
                                        <?= form_error("email", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control" required data-error="Please enter your password" placeholder="Password">
                                        <div class="help-block with-errors"></div>
                                        <?= form_error("password", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                  <label for="remember" class="container-checkbox">Remember me
                                      <input type="checkbox" name="remember" id="remember">
                                      <span class="checkmark"></span>
                                    </label>

                                </div>
                                <div class="col-lg-6">
                                    <div class="forgot-password">
                                        <a href="#"  data-toggle="modal" data-target="#ForgetPassword">Forgot Password ?</a>
                                    </div>
                                </div>

                                <!-- <div class="col-lg-12 col-md-12 text-center">
                                    <button type="submit" class="default-btn">Login <span></span></button>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-lg-12 col-md-12 text-center mt-5">
                                    <a href="<?=base_url('register')?>" class="default-btn">Become an Agent <span></span></a>
                                </div> -->

                                <div class="col-lg-4 col-md-4">
                                    <button type="submit" class="default-btn check">Login <span></span></button>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-lg-8 col-md-8 text-right">
                                    <a href="<?=base_url('register')?>" class="default-btn">Become an Agent <span></span></a>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Login Content End Here -->

    <div id="ForgetPassword" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Forgot password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form id="forgot_password" method="post">
                <div class="form-group">
                  <input type="tesxt" name="UserEmail" id="UserEmail" class="form-control" placeholder="Email Address">
                  <label id="UserEmail-error" class="text-danger"></label>
                </div>
                <div class="form-group mb-0">
                    <button type="submit" class="btn-md btn-block default-btn frgt_pwd_check" formaction="<?= base_url('login/forgot_password')?>">Send Me Email</button>
                </div>
            </form>
          </div>
          <div class="modal-footer">
          </div>
        </div>

      </div>
    </div>
    <!-- forgot password end -->
<?php $this->load->view('include/footer_js');?>
<script>
    /*FORM VALIDATION*/
    $("#loginForm").validate({
        rules: {
            'email': {required: true,email:true}, 
            'password': {required: true}, 
        },
        messages: {
            'email': {required:"Please enter email",email:"Please enter valid email"},
            'password': "Please enter password",
        }
    }); 
    $("#forgot_password").validate({
        rules: {
            'UserEmail': {required: true,email:true}, 
        },
        messages: {
            'UserEmail': {required:"Please enter email",email:"Please enter valid email"},
        }
    });  

    var loginForm = $( "#loginForm" );
        loginForm.validate();

        $(document).on('click','.check',function(e){
          if(!loginForm.valid()){
            $("#loginForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });  
</script>
</body>
</html>
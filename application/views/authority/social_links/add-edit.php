<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Social-links</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-1"></div>
                <!-- /.col -->
                <div class="col-md-10">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <?php
                                    $action = ($details == null) ? base_url('authority/social-links/add_links') : '';
                                    
                                    $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data", "class" => "form-horizontal");
                                    echo form_open($action, $attributes);
                                ?>
                                    <input type="hidden" name="id" id="id" value="<?= ($details != null) ? $details[0]['id'] : '';?>">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Facebook-link:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="facebook_link" placeholder="Facebook link" class="form-control" value="<?= ($details != null) ? $details[0]['facebook_link'] : '';?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Instagram link:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="instagram_link" placeholder="Instagram link" class="form-control" value="<?= ($details != null) ? $details[0]['instagram_link'] : '';?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Twiter link:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="twiter_link" placeholder="Twiter link" class="form-control" value="<?= ($details != null) ? $details[0]['twiter_link'] : '';?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Google plus:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="google_plus_link" placeholder="Google plus" class="form-control" value="<?= ($details != null) ? $details[0]['google_plus_link'] : '';?>">
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Skype link:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="skype_link" placeholder="Skype link" class="form-control" value="<?= ($details != null) ? $details[0]['skype_link'] : '';?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Pinterest link:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="pinterest_link" placeholder="Pinterest link" class="form-control" value="<?= ($details != null) ? $details[0]['pinterest_link'] : '';?>">
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Linked in:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="link_in_link" placeholder="Linked in" class="form-control" value="<?= ($details != null) ? $details[0]['link_in_link'] : '';?>">
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        <label for="inputName" class="col-sm-3 col-form-label">Youtube link:</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="youtube_link" placeholder="Youtube link" class="form-control" value="<?= ($details != null) ? $details[0]['youtube_link'] : '';?>">
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        <div class="offset-sm-3 col-sm-9">
                                            <?php
                                                $attributes = array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-success',
                                                    'value' => 'Submit',
                                                );
                                                echo form_input($attributes);
                                            ?>
                                        </div>
                                    </div>
                                <?= form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>
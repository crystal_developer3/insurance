<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">User</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-primary card-outline">   
                        <div class="card-body box-profile">
                            <?php
                                if ($image){

                                ?>
                                    <div class="text-center">
                                        <label>Profile Picture</label><br>
                                        <img class="profile-user-img img-fluid img-circle" src="<?=isset($image) && $image !=null ? base_url(PROFILE_PICTURE).$image : base_url().'assets/uploads/default_img.png'    ?>" alt="profile picture">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($agency_name) && $agency_name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Name:</b></td>
                                                                    <td><?= $agency_name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($email) && $email !=null){ ?>
                                                                <tr>
                                                                    <td><b>Email Address:</b></td>
                                                                    <td><?=$email?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($phone_number) && $phone_number !=null){ ?>
                                                                <tr>
                                                                    <td><b>Mobile Number:</b></td>
                                                                    <td><?=$phone_number?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($address) && $address !=null){ ?>
                                                                <tr>
                                                                    <td><b>Address:</b></td>
                                                                    <td><?= isset($address) && $address !=null ? $address : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($zip_code) && $zip_code !=null){ ?>
                                                                <tr>
                                                                    <td><b>Zipcode:</b></td>
                                                                    <td><?= isset($zip_code) && $zip_code !=null ? $zip_code : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($types_of_insurance) && $types_of_insurance !=null){ ?>
                                                                <tr>
                                                                    <td><b>Types of insurance:</b></td>
                                                                    <td><?= isset($types_of_insurance) && $types_of_insurance !=null ? $types_of_insurance : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($facebook_link) && $facebook_link !=null){ ?>
                                                                <tr>
                                                                    <td><b>Facebook link:</b></td>
                                                                    <td><?= isset($facebook_link) && $facebook_link !=null ? $facebook_link : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($twitter_link) && $twitter_link !=null){ ?>
                                                                <tr>
                                                                    <td><b>Twitter link:</b></td>
                                                                    <td><?= isset($twitter_link) && $twitter_link !=null ? $twitter_link : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($linkedin_link) && $linkedin_link !=null){ ?>
                                                                <tr>
                                                                    <td><b>Linkedin link:</b></td>
                                                                    <td><?= isset($linkedin_link) && $linkedin_link !=null ? $linkedin_link : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($youtube_link) && $youtube_link !=null){ ?>
                                                                <tr>
                                                                    <td><b>YouTube link:</b></td>
                                                                    <td><?= isset($youtube_link) && $youtube_link !=null ? $youtube_link : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        
                                                        <?php
                                                            if(isset($youtube_link) && $youtube_link !=null){ ?>
                                                                <tr>
                                                                    <td><b>YouTube link:</b></td>
                                                                    <td><?= isset($youtube_link) && $youtube_link !=null ? $youtube_link : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($about) && $about !=null){ ?>
                                                                <tr>
                                                                    <td><b>About:</b></td>
                                                                    <td><?= isset($about) && $about !=null ? $about : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($coverage) && $coverage !=null){ ?>
                                                                <tr class="col-12">
                                                                    <td><b>Coverage:</b></td>
                                                                    <td><?= isset($coverage) && $coverage !=null ? $coverage : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($service_offer) && $service_offer !=null){ ?>
                                                                <tr>
                                                                    <td><b>Service offered:</b></td>
                                                                    <td><?= isset($service_offer) && $service_offer !=null ? $service_offer : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($working_hours) && $working_hours !=null){ ?>
                                                                <tr>
                                                                    <td><b>Wrking hours:</b></td>
                                                                    <td><?= isset($working_hours) && $working_hours !=null ? $working_hours : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>
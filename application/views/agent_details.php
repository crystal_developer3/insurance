<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  

</head>
<body>
    <?php $this->load->view('include/header');?>
    <?php 
        $disabled = ($agency_connected !=null)?'disabled':'';
    ?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Agency Detail</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Agency Detail</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->
    <section class="about-area ptb-70 bg-f8f8f8">
        <div class="container">
            <div class="about-content agancy-detail">
                <div class="row">
                    <?php
                        if(isset($user_details) && $user_details !=null){ ?>
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <div class="title mb-3">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <h3><?=$user_details['agency_name']?></h3>
                                    <div class="mb-4 d-flex align-items-center review">
                                        <span style="display: block;">Be the first to review this agency.</span>
                                        <a href="<?=base_url('agent-details/review/'.$user_details['id'])?>" class="default-btn">Leave A Review</a>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-md-6 col-lg-6 col-xs-12">
                                            <div class="agancy-email ">
                                                <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> 
                                                    <?=($disabled == 'disabled')?$user_details['email']:'*******'?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-xs-12">
                                            <div class="agancy-email">
                                                <a href="<?=$user_details['website_url']?>"><i class="fa fa-globe" aria-hidden="true"></i> Website Url</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-and-follow-btn">
                                        <div class="row">
                                            <div class="col-md-3 col-lg-3 col-sm-6">
                                                <a href="#" class="like-btn"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Like 490</a>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-6 mobile-margin-top">
                                                <a href="#" class="twitter-btn"><i class="fa fa-twitter" aria-hidden="true"></i> Follow 143</a>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-6 mobile-margin-top">
                                                <a href="#" class="youtube-btn"><i class="fa fa-youtube" aria-hidden="true"></i> Youtube 143</a>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-6 mobile-margin-top">
                                                <a href="#" class="linkedin-btn"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin 143</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="agent-detail-overview-content">
                                    <section id="tabs">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12">
                                                    <nav>
                                                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                            <a class="nav-item nav-link active" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">About</a>
                                                            <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-coverages" role="tab" aria-controls="nav-about" aria-selected="false">Coverages</a>
                                                            <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-about" aria-selected="false">Reviews</a>
                                                        </div>
                                                    </nav>
                                                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                                        <div class="tab-pane fade show active" id="nav-about" role="tabpanel" aria-labelledby="nav-home-tab">
                                                            <?=$user_details['about']?>
                                                            <div class="faq-accordion" style="padding: 0px;">
                                                                <h4><b>Ask me about these types of insurance</b></h4>
                                                                <ul class="accordion">
                                                                    <?php
                                                                        if (isset($types_of_insurance_data) && $types_of_insurance_data !=null) {
                                                                            $i = 0;
                                                                            foreach ($types_of_insurance_data as $key => $value) { ?>
                                                                                <li class="accordion-item">
                                                                                    <a class="accordion-title <?=($i==0)?'active':''?>" href="javascript:void(0)">
                                                                                        <i class="fa fa-plus"></i>
                                                                                        <?=$value['title']?>
                                                                                    </a>

                                                                                    <p class="accordion-content <?=($i==0)?'show':''?>"><?=$value['description']?></p>
                                                                                </li>
                                                                                <?php $i++;
                                                                            }
                                                                        }
                                                                    ?>
                                                                </ul>
                                                            </div>
                                                            <?php
                                                                if(!empty($user_details['youtube_video_link'])){?>
                                                                    <div class="video">
                                                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?=getYoutubeKey($user_details['youtube_video_link'])?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                    </div>    
                                                                    <?php 
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="tab-pane fade covrage-tab" id="nav-coverages" role="tabpanel" aria-labelledby="nav-home-tab"style="width: 685px;">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <?=$user_details['coverage']?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="nav-home-tab">
                                                            
                                                            <div class="review-content">
                                                                <div class="row">
                                                                    <div class="col-md-7 agancy-detail-page-review-title mt-2">
                                                                        <h4>Customer Review</h4>
                                                                    </div> 
                                                                    <div class="col-md-5 col-lg-5 col-sm-12 mt-2 text-right">
                                                                        <a href="<?=base_url('agent-details/review/'.$user_details['id'])?>" class="default-btn">Leave a review</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <hr>
                                                            <?php
                                                                if (isset($user_reviews) && $user_reviews !=null) {
                                                                    $i = 0;
                                                                    foreach ($user_reviews as $key => $value) { ?>    
                                                                        <div class="list-group list-group-flush mb-3">
                                                                            <div class="d-flex align-items-center review-content">
                                                                                <?php for ($i=0; $i < $value['ratting']; $i++) { 
                                                                                    # code...
                                                                                ?>
                                                                                        <span class="fa fa-star"></span>
                                                                                    <?php
                                                                                }?>
                                                                             
                                                                                <span class="ml-auto"><?=format_date_Mdy($value['created_at'])?></span>
                                                                            </div>
                                                                            <p><?=$value['message']?></p>
                                                                        </div>
                                                                        <hr>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                            <!-- <div class="list-group list-group-flush mb-3">
                                                                <div class="d-flex align-items-center review-content">
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="ml-auto">December 14, 2019</span>
                                                                </div>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                            </div>
                                                            <hr>
                                                            <div class="list-group list-group-flush mb-3">
                                                                <div class="d-flex align-items-center review-content">
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="ml-auto">December 14, 2019</span>
                                                                </div>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                            </div>
                                                            <hr>
                                                            <div class="list-group list-group-flush mb-3">
                                                                <div class="d-flex align-items-center review-content">
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="ml-auto">December 14, 2019</span>
                                                                </div>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                                            </div>
                                                            <hr>
                                                            <div class="list-group list-group-flush mb-3">
                                                                <div class="d-flex align-items-center review-content">
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="ml-auto">December 14, 2019</span>
                                                                </div>
                                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                                            </div> -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 col-lg-6 col-xs-12">
                                                                <div class="coverage-title">
                                                                    <h3><?=$user_details['agency_name']?></h3>
                                                                </div>
                                                            </div>
                                                            <?php
                                                                $login_id = $this->session->userdata('login_id');
                                                                if(isset($login_id) && $login_id !=  $user_details['id']){ ?>

                                                                <div class="col-md-6 col-lg-6 col-xs-12 text-right">
                                                                    <div class="agancy-email">
                                                                        <button class="default-btn  select_agency <?=($disabled == 'disabled')?'btn-danger':''?>" <?=$disabled?> data-agent_id="<?=$user_details['id']?>"> <?=($disabled == 'disabled')?'Connected':'select agency'?></button>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                }elseif(!isset($login_id)){ ?>
                                                                    <div class="col-md-6 col-lg-6 col-xs-12 text-right">
                                                                        <div class="agancy-email">
                                                                            <a href="<?=base_url('login')?>" class="default-btn">Select agency</a>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="sidebar">
                                    <div class="text-center">
                                        <img src="<?=base_url(PROFILE_PICTURE.$user_details['image'])?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" class="img-responsive" >
                                    </div>
                                    <?php
                                        $login_id = $this->session->userdata('login_id');
                                        if(isset($login_id) && $login_id != $user_details['id']){ ?>
                                            <div class="select-agency-btn">
                                                
                                                <button class="default-btn  select_agency <?=($disabled == 'disabled')?'btn-danger':''?>" <?=$disabled?> data-agent_id="<?=$user_details['id']?>"> <?=($disabled == 'disabled')?'Connected':'select agency'?></button>
                                            </div>
                                        <?php
                                        }else if(!isset($login_id)){ ?>
                                            <div class="select-agency-btn">
                                                <a href="<?=base_url('login')?>" class="default-btn">Select Agency</a>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                    
                                    <div class="top-review">
                                        <div class=" d-flex align-items-center top-review-star pt-2">
                                            <i class="fa fa-phone mr-2"></i>
                                            <span class="tel">
                                                <a href="tel:<?=($disabled == 'disabled')?$user_details['phone_number']:'**********'?>"> 
                                                    <?=($disabled == 'disabled')?$user_details['phone_number']:'**********'?>
                                            </a></span>
                                        </div>
                                    </div>
                                    <!-- <div class="top-review">
                                        <div class="top-review-title">
                                            <h5>Top Review</h5>
                                        </div>
                                        <div class="mb-3 d-flex align-items-center top-review-star">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="ml-auto">December 14, 2019</span>
                                        </div>
                                        <div class="mb-3">
                                            <p>Great</p>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <a href="#" class="btn btn-sm btn-link">See all reviews</a>
                                            <a href="#" class="default-btn">leave a review</a>
                                        </div>
                                    </div> -->
                                    <div class="top-review">
                                        <div class="top-review-title">
                                            <h5>Connect With Us</h5>
                                        </div>
                                        <a href="<?=$user_details['facebook_link']?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="<?=$user_details['twitter_link']?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="<?=$user_details['linkedin_link']?>"><i class="fa  fa-linkedin" aria-hidden="true"></i></a>
                                        <a href="<?=$user_details['youtube_link']?>"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="top-review">
                                        <div class="top-review-title">
                                            <h5>Location</h5>
                                        </div>
                                        <div class="mb-3">
                                            <p><?=$user_details['address']?></p>
                                        </div>
                                        <div class="sidebar-map">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d118147.68202024497!2d70.75125541081155!3d22.27363079405453!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959c98ac71cdf0f%3A0x76dd15cfbe93ad3b!2sRajkot%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1582176210063!5m2!1sen!2sin" width="100%" height="300"frameborder="0" style="border: 0;" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <div class="top-review">
                                        <div class="top-review-title">
                                            <h5>Hours</h5>
                                        </div>
                                        <!-- <div class="row mx-n3 mb-0">
                                            <dt class="col-2 col-lg-3 dt">Mon</dt>
                                            <dd class="col-10 col-lg-9 dd">8:30 AM - 5:00 PM</dd>
                                            <dt class="col-2 col-lg-3 dt">Tue</dt>
                                            <dd class="col-10 col-lg-9 dd">8:30 AM - 5:00 PM</dd>
                                            <dt class="col-2 col-lg-3 dt">Wed</dt>
                                            <dd class="col-10 col-lg-9 dd">8:30 AM - 5:00 PM</dd>
                                            <dt class="col-2 col-lg-3 dt">Thu</dt>
                                            <dd class="col-10 col-lg-9 dd">8:30 AM - 5:00 PM</dd>
                                            <dt class="col-2 col-lg-3 dt">Fri</dt>
                                            <dd class="col-10 col-lg-9 dd">8:30 AM - 5:00 PM</dd>
                                            <dt class="col-2 col-lg-3 dt">Sat</dt>
                                            <dd class="col-10 col-lg-9 dd">Closed</dd>
                                            <dt class="col-2 col-lg-3 dt">Sun</dt>
                                            <dd class="col-10 col-lg-9 dd">Closed</dd>
                                        </div> -->
                                        <?=$user_details['working_hours']?>
                                    </div>
                                    <div class="top-review">
                                        <div class="top-review-title">
                                            <h5>Services Offered</h5>
                                        </div>
                                        <!-- <p><i class="fa fa-check"></i> Minority/Women Owned</p>
                                        <p><i class="fa fa-check"></i> 10+ Years in Business</p>
                                        <p><i class="fa fa-check"></i> DA/Handicap Accessible</p>
                                        <p><i class="fa fa-check"></i> Free Parking</p>
                                        <p><i class="fa fa-check"></i> Languages: English, Espanol</p> -->
                                        <?=$user_details['service_offer']?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
<script>
    
</script>
</body>
</html>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2><?=(isset($details) && !empty($details))?$details[0]['title']:''?></h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li><?=(isset($details) && !empty($details))?$details[0]['title']:''?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Area -->
    <section class="privacy-policy ptb-50">
        <div class="container">
           <p><?=(isset($details) && !empty($details))?$details[0]['description']:''?></p>
        </div>
    </section>
    <!-- End Contact Area -->
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>
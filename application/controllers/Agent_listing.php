<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Agent_listing extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			
		}
		
		public function index()
		{
			$data= array();
			$where['status']=1;
			if($this->input->post()){
				$array = array(
					'zip_code' => $this->input->post('zip_code')
				);
				$this->session->set_userdata($array);
				$where['zip_code']=$this->session->userdata('zip_code');
			}elseif($this->session->userdata('zip_code')){
				$where['zip_code']=$this->session->userdata('zip_code');
			}
			$data['user_details'] = $this->production_model->get_all_with_where('user','','',$where);
			
			$data['agency_connected'] = $this->production_model->get_all_with_where('selected_agency','','',array('user_id'=>$this->session->userdata('login_id')));
			$this->load->view('agent_listing',$data);
		}
		public function clear(){
			if (!$this->input->is_ajax_request()) {
			   exit('No direct script access allowed');
			}
			
			if ($this->session->userdata('zip_code')) {
				$this->session->unset_userdata('zip_code');
				$response_array['success'] = true;
			}
			$response_array['success'] = true;
			
			echo json_encode($response_array);exit;
		}
		public function select_agency(){
			$data = array();
			if (!$this->input->is_ajax_request()) {
	            exit('No direct script access allowed');
	        }

			if($this->input->post()){
				$data = $this->input->post();
				$data['user_id'] =$this->session->userdata('login_id');
				$record=$this->production_model->insert_record('selected_agency',$data);
				if($record){
					echo json_encode(returnResponse('Success','Agency selected successfully!!',$data['agent_id']));
				}else{
					echo json_encode(returnResponse('Fail','Agency not selected !!',array($data['agent_id'])));
				}
			}
		}		
	}
?>
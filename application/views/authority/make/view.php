<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<style type="text/css">
	table tr td:nth-child(3){
		
	}

	table tr td img{
		width: 100%;
		padding: 0px;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Make</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        	<?php $this->load->view('authority/common/messages')?>
            <div class="row">
            	<div class="col-md-6">
            		<?php
						if (isset($details) && $details !=null) {
						?>
            				<button type="button" class="btn btn-danger btn-flat delete_all">Delete all</button>
            			<?php } 
            		?>
            		<a href="<?= base_url('authority/make/add-edit');?>" class="btn btn-secondary btn-flat">Add</a>
            	</div>
            	<div class="col-md-6">
	        		<?php                            
                        $name = isset($this->session->make_info['name']) && $this->session->make_info['name'] !=null ? $this->session->make_info['name'] : '';
                    ?>
	        		<div class="input-group input-group-sm">
				        <input class="form-control form-control-navbar" type="search" name="name" value="<?= $name;?>" placeholder="Search" aria-label="Search">
				        <div class="input-group-append">
				          	<button class="btn btn-primary btn_filter" type="button"><i class="fas fa-search"></i></button>
				        </div>&nbsp;&nbsp;
		        		<a href="<?= base_url('authority/make?clear-search=1')?>" class="btn btn-primary btn-sm" type="button">Clear</a>
				    </div>
	        	</div>
                <div class="col-md-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    	<th style="width: 10px;">
											<div class="custom-control custom-checkbox">
					                          	<input class="custom-control-input" type="checkbox" id="select_all">
					                          	<label for="select_all" class="custom-control-label"></label>
                        					</div>
										</th>
                                        <th style="width: 10px">No</th>
                                        <th>Main menu </th>
                                        <th>Title </th>
                                        <th style="min-width: 100px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                	<?php
										if (isset($details) && $details !=null){
		                                    foreach ($details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
			                                    <tr>
			                                    	<td style="width: 10px;">
			                                    		<div class="custom-control custom-checkbox">
								                          	<input class="custom-control-input chk_all" type="checkbox" id="customCheckbox<?= $id;?>" value="<?= $id?>">
								                          	<label for="customCheckbox<?= $id;?>" class="custom-control-label"></label>
								                        </div>
													</td>
							                        <td><?= $key+1;?></td>
							                        <td><?= get_year_title($value['year_id']);?></td>
							                        <td><?= $value['title'];?></td>
							                        <td>
										                <a href="<?= base_url('authority/make/add-edit/'.$id);?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>
														
														<a href="javascript:void(0)" class="btn bg-gradient-danger btn-flat btn-xs delete_record" id="<?= $id;?>"><i class="fa fa-trash-o"></i></a>

														<?php 
															if($value['status'] == '1'){
																echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="make" data-id="'.$id.'" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
																} else {
																echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="make" data-id="'.$id.'" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
															} 
														?>
							                        </td>
							                    </tr>
			                                <?php } 
			                            }
			                            else{
			                            	?>
			                            		<tr data-expanded="true">
													<td colspan="7" align="center">Records not found</td>
												</tr>
			                            	<?php
			                            }
			                        ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix pagination_filter">
                        	<?php
                                if(isset($pagination) && $pagination !=null){
			                        echo $pagination;    
			                    }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>

<script type="text/javascript">
	$(document).ready(function () {			
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_status',
				data: post_data,
				async: false,
				beforeSend: function() {
	                $('.loading').css({'display':'block'});
	            },
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('bg-gradient-danger bg-gradient-success');
						if(current_element.hasClass('bg-gradient-success')){
							current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
							current_element.attr('data-current-status','1');
							} else {
							current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
							current_element.attr('data-current-status','0');
						}
						} else {
						window.location = window.location.href;
					}
				},
				complete: function() {
	                setTimeout(function() {
	                    $('.loading').css({'display':'none'});
	                }, 100);
	            },
			});
		});

		$(document).on('click','.delete_record',function(e){ 
			var id = [];	    	
	        id.push($(this).attr('id'));
	        console.log(id);
			var post_data = {'id':id};
			var current = $(this).closest('tr');
			$.confirm({
			    title: 'Confirm',
			    type:'red',
                content: 'Are you sure you want to delete record?',
			    buttons: {
			        confirm: function () {
			           	$.ajax({
				            url: '<?= base_url('authority/make/delete/')?>',
				            type: 'POST',
				            dataType: 'json',
				            data: post_data,
				            beforeSend: function() {
				                $('.loading').css({'display':'block'});
				            },
				            success: function(response){
				                if (response.success){
				                	current.remove();
					                toastr.success(response.message);
				                }
				            }, 
				            complete: function() {
				                setTimeout(function() {
				                    $('.loading').css({'display':'none'});
				                }, 1000);
				            },
				        });
			        },
			        cancel: function () {				            
			        },
			    }
			});
		});

		// multiple delete //
		$(document).on('click','.delete_all',function(e){
	    	var id = [];	    	
	    	$(".chk_all:checked").each(function() { 
	            id.push($(this).val());
	        });
	        console.log(id);
			var post_data = {'chk_multi_checkbox':id};
			var boxes = $('.chk_all:checkbox');
	        if(boxes.length > 0) {
	            if($('.chk_all:checkbox:checked').length < 1) {
	                $.alert({
				        title: 'Opps!',
				        type: 'red',
	                    content: 'Please select at least one checkbox',
				    });
	                return false;
	            }
	            else{
	        		$.confirm({
					    title: 'Confirm',
					    type: 'red',
	                    content: 'Are you sure you want to delete record?',
					    buttons: {
					        confirm: function () {
					           	$.ajax({
						            url: '<?= base_url('authority/make/delete/')?>',
						            type: 'POST',
						            dataType: 'json',
						            data: post_data,
						            beforeSend: function() {
						                $('.loading').css({'display':'block'});
						            },
						            success: function(response){
						                if (response.success){
						                	$(".chk_all:checked").each(function() { 
									            id.push($(this).closest('tr').remove()); 
									        });
							                toastr.success(response.message);
						                }
						            }, 
						            complete: function() {
						                setTimeout(function() {
						                    $('.loading').css({'display':'none'});
						                }, 1000);
						            },
						        });
					        },
					        cancel: function () {				            
					        },
					    }
					});
	                return false;
	            }
	        }
		});

		/*Pagination filter*/
        $(document).on('click','.pagination_filter a',function(e){            
            var url = $(this).attr('href');
            var split_url = url.split('/');
            var page_no = split_url[7];
            e.preventDefault();
            
            var name = $('input[name="name"]').val();
            get_filtered_data(page_no,name);
        });

        $(document).on('click','.btn_filter',function(){
        	var name = $('input[name="name"]').val();
        	get_filtered_data('',name);
        });
	});

	function get_filtered_data(page_no='',name=''){     
        var post_data = {'page_no':page_no,'name':name};
        $.ajax({
            url: '<?= base_url('authority/make/filter/')?>'+page_no,
            type: 'POST',
            dataType: 'json',
            data: post_data,
            beforeSend: function() {
                $('.loading').css({'display':'block'});
            },
            success: function(response){
                if (response.success){   
                    $('.data-response').html(response.details);
                    $('.pagination_filter').html(response.pagination);
                }
                else if (response.error && response.data_error){
                    $('.data-response').html(response.data_error);
                    $('.pagination_filter').html(response.pagination);
                }
            }, 
            complete: function() {
                setTimeout(function() {
                    $('.loading').css({'display':'none'});
                }, 1000);
            },
        });
    }
</script>
<?php $this->view('authority/common/footer'); ?>									
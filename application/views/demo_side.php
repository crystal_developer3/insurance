<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              
              <img src="<?php echo base_url(); ?>assets/img/Crystal_Infoway.png" class="circle" alt="User Image" style="width:135px;" />

            </div>
            <div class="pull-left info">
              <p><?php echo  $this->session->userdata('email');?></p>

          <!--     <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
          </div>
          <!-- search form -->
         <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
        
          <ul class="sidebar-menu">
           <!--  <li class="header">MAIN NAVIGATION</li> -->
            <li class="active treeview">
              <a href="<?php echo base_url(); ?>admin/dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <!-- <ul class="treeview-menu">
                <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul> -->
            </li>
			<li class="<?php if($this->uri->segment(2)=='slider'){ ?> active <?php } ?> treeview">
                <a href="#">
                 <i class="fa fa-table"></i> <span>Slider</span>
                   <i class="fa fa-angle-left pull-right"></i>
                </a>  
                <ul class="treeview-menu">
                  <li class="active">
                    <a href="<?php echo base_url(); ?>admin/slider/add">
                      <i class="fa fa-circle-o"></i> 
                      Add Slider </a>
                  </li>
                  <li class="active">
                    <a href="<?php echo base_url(); ?>admin/slider">
                      <i class="fa fa-circle-o"></i> 
                      Manage Slider </a>
                  </li>
                </ul> 
              </li>
			  <li class="<?php if($this->uri->segment(2)=='about_us'){ ?> active <?php } ?> treeview">
          <a href="<?php echo base_url(); ?>admin/about_us">
           <i class="fa fa-table"></i> <span>About Us</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
        </li>
			  <li class="<?php if($this->uri->segment(2)=='services' || $this->uri->segment(2)=='subservices'){ ?> active <?php } ?> treeview">
          <a href="#">
           <i class="fa fa-table"></i> <span>Services</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
          <ul class="treeview-menu">
            <li class="<?=($this->uri->segment(2)=='services' && $this->uri->segment(3)=='add')?'active':''?>">
              <a href="<?php echo base_url(); ?>admin/services/add">
                <i class="fa fa-circle-o"></i> 
                Add Services</a>
            </li>
            <li class="<?=($this->uri->segment(2)=='services' && $this->uri->segment(3)=='')?'active':''?>">
              <a href="<?php echo base_url(); ?>admin/services">
                <i class="fa fa-circle-o"></i> 
                Manage Services </a>
            </li>
            <li class="<?=($this->uri->segment(2)=='subservices' && $this->uri->segment(3)=='add')?'active':''?>">
              <a href="<?php echo base_url(); ?>admin/subservices/add">
                <i class="fa fa-circle-o"></i> 
                 Add Subservices</a>
            </li>
            <li class="<?=($this->uri->segment(2)=='subservices' && $this->uri->segment(3)=='')?'active':''?>">
              <a href="<?php echo base_url(); ?>admin/subservices">
                <i class="fa fa-circle-o"></i> 
                Manage Subservices</a>
            </li>
          </ul> 
        </li>
			  <li class="<?php if($this->uri->segment(2)=='testimonial'){ ?> active <?php } ?> treeview">
          <a href="<?php echo base_url(); ?>admin/testimonial">
           <i class="fa fa-table"></i> <span>Testimonial</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
        </li>
			  <li class="<?php if($this->uri->segment(2)=='video'){ ?> active <?php } ?> treeview">
          <a href="#">
           <i class="fa fa-table"></i> <span>Video</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
          <ul class="treeview-menu">
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/video/add">
                <i class="fa fa-circle-o"></i> 
                Add Video</a>
            </li>
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/video">
                <i class="fa fa-circle-o"></i> 
                Manage Video </a>
            </li>
          </ul> 
        </li>
			  <li class="<?php if($this->uri->segment(2)=='products'){ ?> active <?php } ?> treeview">
          <a href="#">
           <i class="fa fa-table"></i> <span>Products</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
          <ul class="treeview-menu">
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/products/add">
                <i class="fa fa-circle-o"></i> 
                Add Products</a>
            </li>
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/products">
                <i class="fa fa-circle-o"></i> 
                Manage Products </a>
            </li>
          </ul> 
        </li>
			  <li class="<?php if($this->uri->segment(2)=='contact_us'){ ?> active <?php } ?> treeview">
          <a href="<?php echo base_url(); ?>admin/contact_us">
           <i class="fa fa-table"></i> <span>Contact Us</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
        </li>
			  <li class="<?php if($this->uri->segment(2)=='blog'){ ?> active <?php } ?> treeview">
          <a href="#">
           <i class="fa fa-table"></i> <span>Blog</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
          <ul class="treeview-menu">
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/blog/add">
                <i class="fa fa-circle-o"></i> 
                Add Blog</a>
            </li>
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/blog">
                <i class="fa fa-circle-o"></i> 
                Manage Blog</a>
            </li>
          </ul> 
        </li>
			  <li class="<?php if($this->uri->segment(2)=='comment'){ ?> active <?php } ?> treeview">
          <a href="<?php echo base_url(); ?>admin/comment">
           <i class="fa fa-table"></i> <span>Manage Comment</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
          <!-- <ul class="treeview-menu">
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/comment">
                <i class="fa fa-circle-o"></i> 
                Manage Comment</a>
            </li>
          </ul>  -->
        </li>
			  <li class="<?php if($this->uri->segment(2)=='gallery_category' || $this->uri->segment(2)=='gallery'){ ?> active <?php } ?> treeview">
          <a href="#">
           <i class="fa fa-table"></i> <span>Gallery</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
          <ul class="treeview-menu">
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/gallery_category/add">
                <i class="fa fa-circle-o"></i> 
                Add Gallery Category</a>
            </li>
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/gallery_category">
                <i class="fa fa-circle-o"></i> 
                Manage Gallery Category</a>
            </li>
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/gallery/add">
                <i class="fa fa-circle-o"></i> 
                Add Gallery</a>
            </li>
            <li class="active">
              <a href="<?php echo base_url(); ?>admin/gallery">
                <i class="fa fa-circle-o"></i> 
                Manage Gallery</a>
            </li>
          </ul> 
        </li>
			  <li class="<?php if($this->uri->segment(2)=='appointment'){ ?> active <?php } ?> treeview">
          <a href="<?php echo base_url(); ?>admin/appointment">
           <i class="fa fa-table"></i> <span>Appointment</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
        </li>
			  <li class="<?php if($this->uri->segment(2)=='programs'){ ?> active <?php } ?> treeview">
                <a href="#">
                 <i class="fa fa-table"></i> <span>Programs</span>
                   <i class="fa fa-angle-left pull-right"></i>
                </a>  
                <ul class="treeview-menu">
                  <li class="active">
                    <a href="<?php echo base_url(); ?>admin/programs/add">
                      <i class="fa fa-circle-o"></i> 
                      Add Programs</a>
                  </li>
                  <li class="active">
                    <a href="<?php echo base_url(); ?>admin/programs">
                      <i class="fa fa-circle-o"></i> 
                      Manage Programs</a>
                  </li>
                </ul> 
              </li>
			  <li class="<?php if($this->uri->segment(2)=='social_activities'){ ?> active <?php } ?> treeview">
          <a href="<?php echo base_url(); ?>admin/social_activities">
           <i class="fa fa-table"></i> <span>Social Activities</span>
             <i class="fa fa-angle-left pull-right"></i>
          </a>  
        </li>
			  <!--<li class="<?php if($this->uri->segment(2)=='users'){ ?> active <?php } ?> treeview">
                <a href="#">
                 <i class="fa fa-table"></i> <span>Manage Users</span>
                   <i class="fa fa-angle-left pull-right"></i>
                </a>  
                <ul class="treeview-menu">
                  <li class="active">
                    <a href="<?php echo base_url(); ?>admin/users">
                      <i class="fa fa-circle-o"></i> 
                      Manage Users </a>
                  </li>
                  <li class="active">
                    <a href="<?php echo base_url(); ?>admin/users">
                      <i class="fa fa-circle-o"></i> 
                      Add Users </a>
                  </li>
                </ul> 
              </li>-->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
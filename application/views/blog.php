<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <section class="inner-intro section-padding dark-overlay">
        <div class="container">
            <div class="inner-text white-text z-index text-center">
                <h1>Our Blog</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
                        <li class="breadcrumb-item active">Our Blog</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>
    <?php $this->load->view('include/blog_section');?>   
    
<?php $this->load->view('include/footer');?>
<?php $this->load->view('include/footer_js');?>

</body>
</html>
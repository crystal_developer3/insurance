<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>New Review</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>New Review</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Area -->
    <section class="contact-area ptb-100">
        <div class="container">
            <div class="section-title">
                <h2>Leave a review for Agency</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>

            <div class="row">
                <div class="col-lg-8 col-md-12 offset-md-2">
                    <div class="contact-form">
                        <form id="form" method="post" action="" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <?php $this->load->view('include/messages');?>
                                    <div class="form-group form-margin-bottom">
                                        <div class="rating-title">
                                            <h4><span>1</span> Rate This Agency</h4>
                                        </div>
                                        <section class='rating-widget'>
                                          <div class='rating-stars'>
                                            <ul id='stars'>
                                            
                                              <li class='star' title='Poor' data-value='1'>
                                                <i class='fa fa-star fa-fw'></i>
                                              </li>
                                              <li class='star' title='Fair' data-value='2'>
                                                <i class='fa fa-star fa-fw'></i>
                                              </li>
                                              <li class='star' title='Good' data-value='3'>
                                                <i class='fa fa-star fa-fw'></i>
                                              </li>
                                              <li class='star' title='Excellent' data-value='4'>
                                                <i class='fa fa-star fa-fw'></i>
                                              </li>
                                              <li class='star' title='WOW!!!' data-value='5'>
                                                <i class='fa fa-star fa-fw'></i>
                                              </li>
                                            </ul>
                                            <input type="hidden" name="ratting" id="ratting">
                                          </div>
                                        </section>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group form-margin-bottom">
                                        <div class="rating-title">
                                            <h4><span>2</span> Tell us about your experience</h4>
                                        </div>
                                        <input type="hidden" name="agent_id" id="agent_id" value="<?=(isset($agent_id) && $agent_id !="")?$agent_id:''?>">
                                        
                                        <textarea name="message" class="form-control" id="message" cols="30" rows="6" required data-error="Write your Tell us about your experience" placeholder="Tell us about your experience"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="rating-title">
                                        <h4><span>3</span> Your information</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" placeholder="Name">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" placeholder="Email">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-text">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="default-btn check">Submit Your Review <span></span></button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
    </section>
    <!-- End Contact Area -->
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
        var onStar;
        $(document).ready(function(){

            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });
                
            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                      $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                $('#ratting').val(onStar);
                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                var msg = "";
                if (ratingValue > 1) {
                    msg = "Thanks! You rated this " + ratingValue + " stars.";
                }
                else {
                    msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
                }
                // alert(msg);
            });              
        });
    </script>
    <script type="text/javascript">
        $("#form").validate({
            ignore: [],
            rules: {
                'name': {required: true},
                'ratting': {required: true},
                'email': {required: true,email:true},                 
                'message': {required: true},
            },
            // errorPlacement: function (error, element) {
            //     var name = $(element).attr("name");
            //     error.appendTo($("#" + name + "_error"));
            // },
            messages: {
                'name': {required:"Please enter name"}, 
                'ratting': {required: "Please rate this agency"},
                'email': {required:"Please enter email",email:"Please enter valid email"}, 
                'message': {required:"Please type your message"},                
            }
        }); 

        var form = $( "#form" );
        form.validate();
        $(document).on('submit','#form',function(e){
            
          if(form.valid()){
            e.preventDefault();
            add_review();
          }else{
            return false;
          }
     
        });
        function add_review(){
          errormsg = '';

          var formData = new FormData($('#form')[0]);
          
          var uurl = BASE_URL+"agent_details/addreview";
           $.ajax({
               url: uurl,
               type: 'POST',
               data: formData,
               dataType:'json',
               //async: false,
               beforeSend: function(){
                // $('.preloader').show();
               },
               success: function(response){
                 if (response.result=="Success") {
                   // setTimeout(function() { window.location.reload(); }, 2000);
                    $(function() {
                        toastr.options = {
                        positionClass : "toast-bottom-right"
                        };
                        toastr.success('added');
                    });
                    setTimeout(function() { window.location.href = BASE_URL+"agent-details/"+"<?=(isset($agent_id) && $agent_id !="")?$agent_id:''?>" }, 2000);
                 }else{
                    $(function() {
                        toastr.options = {
                        positionClass : "toast-bottom-right"
                        };
                        toastr.error('not added');
                    });
                 }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 // $('.preloader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
           });
        }
    </script>
</body>
</html>
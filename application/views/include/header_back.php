<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= SITE_TITLE;?></title>
        <link href="<?= base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
        <link href="<?= base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url()?>assets/css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url()?>assets/css/aos.css" />
        <link rel="stylesheet" href="<?= base_url()?>assets/css/baguetteBox.min.css">

        <!--New add-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <link rel="stylesheet" href="<?= base_url()?>assets/css/developer.css"/>
        <script>
            var BASE_URL = '<?= base_url();?>';
        </script>
    </head>
    <body>
        <div class="header">
            <div class="loading" style="display: none;"></div><!-- Loader -->
            <div class="top-menu1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <?php
                                $details = $this->production_model->get_all_with_where('administrator','','',array());
                            ?>
                            <div class="logo">
                                <?php
                                    if (isset($details) && $details !=null) {
                                    ?>
                                        <a href="<?= base_url();?>"><img src="<?= base_url(PROFILE_PICTURE.$details[0]['profile_photo']);?>"></a>
                                    <?php } 
                                ?>
                            </div>
                        </div>
                        <div class="top-menu col-sm-8">
                            <div class="menu-text"><img src="<?= base_url()?>assets/images/menu-title.png" alt="" /></div>
                            <nav class="navbar navbar-inverse">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="navbarCollapse" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav second-menu">
                                        <li><a href="<?= base_url('about-us');?>"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span><?= $this->lang->line('about_us');?></a></li>
                                        
                                        <li><a href="<?= base_url('treatments');?>"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span><?= $this->lang->line('vaidic_healing');?></a></li>

                                        <li><a href="<?= base_url('facilities');?>"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span><?= $this->lang->line('facilities');?></a></li>

                                        <li><a href="<?= base_url('accommodation');?>"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span><?= $this->lang->line('accommodation');?></a></li>
                                        <li><a href="<?= base_url('gallery');?>"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span><?= $this->lang->line('gallery');?></a></li>
                                        <li><a href="#"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span>Virtual Tour</a></li>
                                        <li><a href="<?= base_url('faq');?>"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span><?= $this->lang->line('faq');?></a></li>
                                        <li><a href="#"><span class="menu-icon"><img src="<?= base_url()?>assets/images/menu/1.png" alt="" /></span>Contact Us</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="col-sm-2 pull-right">
                            <div class="socil-medea-flag">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="head-apply-admi">
                                            <a href="Apply-admission.html" class="myButton">Reservation</a>
                                            +91 - <?= isset($details) && $details !=null ? $details[0]['mobile_number_1'] : '';?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-menu col-sm-2">
                            <select class="form-control language_change">
                                <option value="1" <?= LANGUAGE == 'english' ? 'selected' : '';?>>English</option>
                                <option value="2" <?= LANGUAGE == 'hindi' ? 'selected' : '';?>>Hindi</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <?php
        if (isset($blog_details) && $blog_details !=null) { ?>
            <section class="inner-intro section-padding dark-overlay">
                <div class="container">
                    <div class="inner-text white-text z-index text-center">
                        <h1><?=$blog_details[0]['title']?></h1>
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
                                <li class="breadcrumb-item active"><?=$blog_details[0]['title']?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>
            <section class="section-padding">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-8">
                      <article class="article_wrap">
                        <div class="post-thumbnail"> <img src="<?= base_url(BLOG_IMAGE.$blog_details[0]['image'])?>" alt="image"> </div>
                        <div class="entry-desc">
                          <div class="post-meta">
                            <ul>
                              <li><a href="#"><?=format_date_mdy($blog_details[0]['created_at'])?></a></li>
                              <li>By <a href="#"><?=$blog_details[0]['author']?></a></li>
                            </ul>
                          </div>
                          <div class="entry-content">
                            <p><?=$blog_details[0]['description']?></p>
                          </div>
                        </div>
                      </article>
                    </div>
                    <aside class="col-lg-4">
                      <div class="sidebar">
                        <div class="sidebar-widgets">
                          <div class="widget-title">
                            <h4>Categories</h4>
                          </div>
                          <ul>
                            <li><a href="#">Personal Loan</a></li>
                            <li><a href="#">Home Loan</a></li>
                            <li><a href="#">mortgage Loan</a></li>
                            <li><a href="#">Stratups</a></li>
                            <li><a href="#">Infographics</a></li>
                            <li><a href="#">Tax Benifts</a></li>
                            <li><a href="#">Loans</a></li>
                            <li><a href="#">Business Loans</a></li>
                            <li><a href="#">Loan Against Property</a></li>
                            <li><a href="#">Project Loans</a></li>
                            <li><a href="#">Digital Bank Loan</a></li>
                          </ul>
                        </div>
                      </div>
                    </aside>
                  </div>
                </div>
            </section>  
            <?php 
        }
    ?>
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>
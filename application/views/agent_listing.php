<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Location</h2>
                        <ul>
                            <li><a href="<?=base_url()?>l">Home</a></li>
                            <li>Location</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <section class="about-area ptb-70 bg-f8f8f8">
        <div class="container">
            <div class="about-content">
                <div class="zip-code-view">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <!-- <div class="view-map-btn">
                                <a class="default-btn" id ="buttonLogin">View Location</a>
                                <div id="map_Box_Div">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d118147.68202024497!2d70.75125541081155!3d22.27363079405453!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959c98ac71cdf0f%3A0x76dd15cfbe93ad3b!2sRajkot%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1582176210063!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                </div>
                            </div> -->
                        </div>
                        <?php
                            $agency_connected = array_column($agency_connected, 'agent_id');
                            // echo "<pre>";print_r($agency_connected);
                            
                            
                            if (isset($user_details) && $user_details !=null) {
                                $i = 0;
                                foreach ($user_details as $key => $value) { 

                                    $disabled = (in_array($value['id'], $agency_connected))?'disabled':'';
                                    

                                    ?>    
                                    <div class="col-md-4 col-lg-4 col-sm-12">

                                        <div class="zip-code-view-box text-center">
                                            <div class="user-image mb-3">
                                                <a href="<?=base_url('agent-details/'.$value['id'])?>" >
                                                    <img src="<?=base_url(PROFILE_PICTURE.$value['image'])?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'"  class="img-fluid"
                                                    >
                                                </a>
                                            </div>
                                            <a href="<?=base_url('agent-details/'.$value['id'])?>" >
                                                <p><b><?=$value['agency_name']?></b></p>
                                            </a>
                                            <div class="find-agent-btn">
                                                

                                                <?php
                                                    $login_id = $this->session->userdata('login_id');
                                                    if(isset($login_id) && $login_id != $value['id']){ ?>
                                                        <div class="select-agency-btn">
                                                            <button class="default-btn  select_agency <?=($disabled == 'disabled')?'btn-danger':''?>" <?=$disabled?> data-agent_id="<?=$value['id']?>"> <?=($disabled == 'disabled')?'Connected':'select agency'?></button>
                                                        </div>
                                                    <?php
                                                    }else if(!isset($login_id)){ ?>
                                                        <div class="select-agency-btn">
                                                            <a href="<?=base_url('login')?>" class="default-btn">Select Agency</a>
                                                        </div>
                                                        <?php
                                                    }else{?>
                                                        <div class="select-agency-btn">
                                                            <a href="#" class="default-btn" disabled >It's you</a>
                                                        </div>
                                                        <?php
                                                    }
                                                ?>                                   
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }else{ ?>
                                <div class="col-12 text-center">
                                    <h3>No data found</h3>                                   
                                </div>
                                <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>   
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>
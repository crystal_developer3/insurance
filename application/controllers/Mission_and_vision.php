<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mission_and_vision extends CI_Controller {
	public function index()
	{
		$condition = array('status'=>'1','id'=>5);
		$data['mission_details'] = static_page($condition);

		$condition2 = array('status'=>'1','id'=>6);
		$data['vision_details'] = static_page($condition2);
		
		$this->load->view('mission_and_vision',$data);
	}
}
?>
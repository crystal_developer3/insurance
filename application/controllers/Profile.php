<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Profile extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			if(empty($this->session->userdata('login_id'))){
				redirect('login','refresh');
			}
		}
		
		public function index()
		{
			$data= array();
			$where['id']=$this->session->userdata('login_id');
			$user_details = $this->production_model->get_all_with_where('user','','',$where);
			if($user_details > 0){
				$data['user_details'] = $user_details[0];
			}
			$data['types_of_insurance_data'] = $this->production_model->get_all_with_where('types_of_insurance','','',array('status'=>'1'));
			// echo "<pre>";print_r($data);exit;

			if($this->input->post())
			{
				$data = $this->input->post();
				$types = $this->input->post('types_of_insurance');
				$types = implode(",", $types);
				$data['types_of_insurance'] =$types;
				// echo "<pre>";print_r($data);exit;

				$this->form_validation->set_rules('agency_name', 'agency_name', 'required');
				// $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[user.email]');
				// $this->form_validation->set_rules('phone_number', 'phone_number', 'required|is_unique[user.phone_number]');
				// $this->form_validation->set_rules('zip_code', 'zip_code', 'required');

				if ($this->form_validation->run() == FALSE)
		        {		        	
		            $data = array_merge($data, $data['user_details']);
		        }
		        else
		        {
		        	// $data['email'] = strtolower($data['email']);	        	
				// echo "<pre>";print_r($data);exit;

		          	
					if ($_FILES['image']['name'] !='') {
                        $data['image'] = $this->production_model->image_upload(PROFILE_PICTURE,'image');
                    }		
                    $record = $this->production_model->update_record('user',$data,array('id'=>$this->session->userdata('login_id')));
                    // echo"<pre>".$this->db->last_query(); print_r($record); exit;
					if ($record !='') {
						$data['user_id'] = $record;
						// $send_mail = $this->production_model->mail_send(SITE_TITLE,array($data['email']),'','mail_form/thankyou_page/registration',$data,''); 

						// $admin_email = $this->production_model->get_all_with_where('administrator','','',array()); 
						// $admin_send_mail = $this->production_model->mail_send(SITE_TITLE,array(ADMIN_EMAIL),'','mail_form/admin_send_mail/registration',$data,''); // admin send mail
						// $this->session->set_flashdata('success', 'Thank you for registration please check your mail');
						$this->session->set_flashdata('success', 'Profile Upated..');
						redirect($_SERVER['HTTP_REFERER']);

					}
					else
					{
						$this->session->set_flashdata('error', 'Not Registered....!');
						// redirect($_SERVER['HTTP_REFERER']);
					}
					
				}	
			}
			$this->load->view('profile',$data);
		}		
	}
?>
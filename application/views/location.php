<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Location</h2>
                        <ul>
                            <li><a href="<?=base_url()?>l">Home</a></li>
                            <li>Location</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <section class="about-area ptb-100 bg-f8f8f8">
        <div class="container">
            <div class="about-content">
                <div class="zip-code-add">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 mx-auto">
                            <form id="zip_form" action="<?=base_url('agent-listing')?>" method="post">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="zip_code" id="zip_code" class="form-control" required data-error="Please enter Zip Code" placeholder="Zip Code">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <button type="submit" class="default-btn location-btn check" style="margin-top: 6px;">Submit<span></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d118147.68202024497!2d70.75125541081155!3d22.27363079405453!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959c98ac71cdf0f%3A0x76dd15cfbe93ad3b!2sRajkot%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1582176210063!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </section>   
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
        $("#zip_form").validate({
            rules: {
                'zip_code': {required: true,number:true}, 
                
            },
            messages: {
                'zip_code': {required:"Please enter zip code",number:"Please enter valid zip code"},
                
            }
        }); 

        var zip_form = $( "#zip_form" );
        zip_form.validate();

        $(document).on('click','.check',function(e){
          if(!zip_form.valid()){
            $("#zip_form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });
    </script>
</body>
</html>
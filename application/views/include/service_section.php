<section class="services-area bg-f8f8f8 pb-70">
    <div class="container">
        <div class="services-slides owl-carousel owl-theme">
            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-home-insurance"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Home Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>

                <div class="box-shape">
                    <img src="<?= base_url()?>assets/img/box-shape1.png" alt="image">
                    <img src="<?= base_url()?>assets/img/box-shape2.png" alt="image">
                </div>
            </div>

            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-insurance"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Business Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>

                <div class="box-shape">
                    <img src="<?= base_url()?>assets/img/box-shape1.png" alt="image">
                    <img src="<?= base_url()?>assets/img/box-shape2.png" alt="image">
                </div>
            </div>

            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-health-insurance"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Health Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>

                <div class="box-shape">
                    <img src="<?= base_url()?>assets/img/box-shape1.png" alt="image">
                    <img src="<?= base_url()?>assets/img/box-shape2.png" alt="image">
                </div>
            </div>

            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-travel-insurance"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Travel Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>
            </div>

            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-car-insurance"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Car Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>

                <div class="box-shape">
                    <img src="<?= base_url()?>assets/img/box-shape1.png" alt="image">
                    <img src="<?= base_url()?>assets/img/box-shape2.png" alt="image">
                </div>
            </div>

            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-life-insurance"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Life Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>

                <div class="box-shape">
                    <img src="<?= base_url()?>assets/img/box-shape1.png" alt="image">
                    <img src="<?= base_url()?>assets/img/box-shape2.png" alt="image">
                </div>
            </div>

            <div class="single-services-box">
                <div class="icon">
                    <i class="flaticon-agriculture"></i>

                    <div class="icon-bg">
                        <img src="<?= base_url()?>assets/img/icon-bg1.png" alt="image">
                        <img src="<?= base_url()?>assets/img/icon-bg2.png" alt="image">
                    </div>
                </div>

                <h3><a href="#">Agri Insurance</a></h3>

                <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod incididunt ut incididunt labore et dolore.</p>

                <a href="#" class="read-more-btn">Learn More</a>

                <div class="box-shape">
                    <img src="<?= base_url()?>assets/img/box-shape1.png" alt="image">
                    <img src="<?= base_url()?>assets/img/box-shape2.png" alt="image">
                </div>
            </div>
        </div>
    </div>
</section>
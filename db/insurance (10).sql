-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2020 at 02:15 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `back_image` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `description`, `image`, `back_image`, `created_at`, `modified_at`, `status`) VALUES
(1, 'We\'ve Been Thriving in 37 Years The Area', '<!--<h2>We\'ve Been Thriving in 37 Years The Area</h2>--><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \r\neiusmod tempor incididunt ut labore et dolore magna aliqua. Quis \r\nsuspendisse ultrices gravida. Risus commodo viverra lorem ipsum dolor \r\nsit amet, consectetur adipiscing elit. Lorem sed do eiusmod tempor \r\nincididunt.</p>\r\n\r\n                            <p>Risus commodo viverra lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem sed do eiusmod tempor incididunt.</p>', '75ce873c941b5a1c30ced3ff7aacf36b.jpg', 'ebd82e836650ba0faa8d83458ddc1183.jpg', '2020-01-04 17:26:17', '2020-02-25 17:12:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `mobile_number_1` varchar(15) NOT NULL,
  `mobile_number_2` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `gender` enum('Male','Female','Transgender') NOT NULL,
  `role` enum('Admin','User') NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `white_logo` varchar(255) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `is_enable` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `full_name`, `username`, `email_address`, `mobile_number_1`, `mobile_number_2`, `address`, `gender`, `role`, `profile_photo`, `white_logo`, `salt`, `password`, `is_enable`, `created_at`, `updated_at`) VALUES
(1, 'Insurance', 'Insurance', 'insurance@gmail.com', '022 1000100', '02951 508127', '27 Division St, New York, NY 1002, USA', 'Male', 'Admin', 'fa623f6c05d821325da609a87515f265.png', 'e2c26dc7a499127a0f35e82a9bd96812.png', 'e94049558d21fabbd8124d874355b1ad', 'eb0bc03b321ab5343591e63489b5e475218b531a', '1', '2020-01-15 17:55:50', '2020-02-25 12:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`id`, `title`, `description`, `image`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Summer Holiday', '', '8d9759eb3169d527ed575b760dcb89a5.png', '2020-03-12 19:37:33', '2020-03-12 19:37:33', 1),
(2, 'Summer', '', 'f4ff224420d459df05b11640b8b6ffdf.png', '2020-03-12 19:44:18', '2020-03-12 19:44:18', 1),
(3, 'Welcome Summer', '', 'ba6b352c947c47ec42782f769b1c8434.png', '2020-03-12 19:44:46', '2020-03-12 19:44:46', 1),
(4, 'SB', '', 'a9a16496215fda2b785ef9e12b81419a.png', '2020-03-12 19:45:08', '2020-03-12 19:45:08', 1),
(5, 'Let the summer begin', '', '2e50b3a5dc74fed6d9e91c557c0b18fd.png', '2020-03-12 19:45:57', '2020-03-12 19:45:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bonds`
--

CREATE TABLE `bonds` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonds`
--

INSERT INTO `bonds` (`id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(1, 'Contract Surety Bonds ', '2020-01-18 18:46:19', '2020-03-06 17:40:57', 'contract-surety-bonds-', 1),
(2, 'Judicial Surety Bonds ', '2020-01-18 18:46:19', '2020-03-06 17:40:57', 'judicial-surety-bonds-', 1),
(3, 'Probate Court Surety Bonds', '2020-02-06 20:27:17', '2020-03-06 17:40:57', 'probate-court-surety-bonds', 1),
(4, 'Commercial Surety Bonds', '2020-02-06 20:27:29', '2020-03-06 17:40:57', 'commercial-surety-bonds', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bonds_sub`
--

CREATE TABLE `bonds_sub` (
  `id` int(11) NOT NULL,
  `menu_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonds_sub`
--

INSERT INTO `bonds_sub` (`id`, `menu_id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(1, 4, 'Bonds Dub1', '2020-02-14 17:48:56', '2020-03-06 17:41:28', 'bonds-dub1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bonds_sub_sub`
--

CREATE TABLE `bonds_sub_sub` (
  `id` int(11) NOT NULL,
  `submenu_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `business_structure`
--

CREATE TABLE `business_structure` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_structure`
--

INSERT INTO `business_structure` (`id`, `title`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Corporation', '2020-01-18 18:46:19', '2020-02-17 20:11:12', 1),
(2, 'Partnership', '2020-01-18 18:46:19', '2020-02-17 20:11:20', 1),
(3, 'LLC', '2020-02-17 20:11:28', '2020-02-17 20:11:28', 1),
(4, 'Sole Proprietorship', '2020-02-17 20:11:36', '2020-02-17 20:11:36', 1),
(5, 'Others', '2020-02-17 20:11:43', '2020-02-17 20:11:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Category1', '2020-01-18 18:46:19', '2020-03-04 19:54:54', 1),
(2, 'Category2', '2020-01-18 18:46:19', '2020-03-04 19:54:56', 1),
(3, 'Category3', '2020-02-06 20:27:17', '2020-03-04 19:54:59', 1),
(4, 'Category4', '2020-02-06 20:27:29', '2020-03-04 19:55:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile_number` varchar(15) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `mobile_number`, `message`, `created_at`) VALUES
(1, 'erpa', 'erpa@gmail.com', '7824795497', 'Message', '2020-02-26 19:08:43'),
(2, 'Ggg', 'ggg@gmail.com', '6383486087', 'This is to inform you that', '2020-02-26 19:08:53'),
(3, 'mmg', 'mmg@gmail.com', '7821791494', 'This is message from mmg', '2020-02-26 19:09:18'),
(4, 'egc', 'egc@gmail.com', '9824795495', 'This is message', '2020-02-27 17:48:44');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` varchar(100) NOT NULL,
  `answer` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `created_at`, `modified_at`, `status`) VALUES
(15, 'what is the benefit of insurence ?', '<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</p>', '2020-02-03 19:38:00', '2020-02-22 14:01:16', '1'),
(16, 'What can I prefer the insurance company ?', '<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</p>', '2020-02-22 14:02:11', '2020-02-22 14:02:11', '1');

-- --------------------------------------------------------

--
-- Table structure for table `free_quote`
--

CREATE TABLE `free_quote` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `property_used_for` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `free_quote`
--

INSERT INTO `free_quote` (`id`, `name`, `email`, `mobile_number`, `property_used_for`, `created_at`) VALUES
(2, 'zzz', 'zzz@gmail.com', '7782479549', 'Home Insurance', '2020-03-13 17:36:51'),
(5, 'zz', 'zz@gmail.com', '8724795498', 'Business Insurance', '2020-03-13 17:46:26');

-- --------------------------------------------------------

--
-- Table structure for table `general_contact`
--

CREATE TABLE `general_contact` (
  `id` int(11) NOT NULL,
  `selected_item` varchar(50) NOT NULL,
  `category_id` tinyint(5) NOT NULL,
  `product_id` tinyint(5) NOT NULL,
  `preferred_language` varchar(50) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `business_name` varchar(50) NOT NULL,
  `business_address` mediumtext NOT NULL,
  `address` mediumtext NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `message` mediumtext NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_contact`
--

INSERT INTO `general_contact` (`id`, `selected_item`, `category_id`, `product_id`, `preferred_language`, `name`, `email`, `business_name`, `business_address`, `address`, `mobile_number`, `message`, `created_at`) VALUES
(8, 'Physicians Liability', 2, 1, '3,2,1', 'xyz', 'xyz@gmail.com', '', '', '', '9224795491', '123456', '2020-02-17 20:29:40'),
(13, 'Physicians Liability', 3, 3, '6,3', 'efg', 'edf@gmail.com', 'XYZ', 'volter street', 'volter street', '912411544', '456', '2020-03-11 17:05:09'),
(14, 'Probate Court Surety Bonds', 3, 4, '2,1', 'sduv', 'sduv@gmail.com', 'The Empire', 'sdt street', 'kemroon street', '7783482081', 'The Empire', '2020-03-11 18:17:58');

-- --------------------------------------------------------

--
-- Table structure for table `general_quote`
--

CREATE TABLE `general_quote` (
  `id` int(11) NOT NULL,
  `selected_item` varchar(50) NOT NULL,
  `preferred_language` varchar(50) DEFAULT NULL,
  `quote_type` varchar(50) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `address` mediumtext NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_quote`
--

INSERT INTO `general_quote` (`id`, `selected_item`, `preferred_language`, `quote_type`, `first_name`, `last_name`, `email`, `mobile_number`, `address`, `created_at`) VALUES
(11, 'Umbrella Insurance', '2,3', 'condo', 'Qqt', 'Sty', 'qqt@gmail.com', '8924795497', 'The Streat', '2020-03-07 11:37:41'),
(12, 'Car Insurance', '4,6', 'renter', 'axn', 'yjkl', 'axn@gmail.com', '9429794491', 'Royal tower', '2020-03-11 18:18:55'),
(13, 'Umbrella Insurance', '1,2', 'home/renter/condo', 'opqer', 'sgkl', 'opqr@gmail.com', '897898985', 'Street of the garden.', '2020-03-11 20:31:06');

-- --------------------------------------------------------

--
-- Table structure for table `general_quote_auto`
--

CREATE TABLE `general_quote_auto` (
  `id` int(11) NOT NULL,
  `selected_item` varchar(50) NOT NULL,
  `preferred_language` varchar(50) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `image` mediumtext,
  `have_auto_insurance` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes',
  `accidents_in_last_five_years` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes',
  `accidents_by_anyone_who_was_not_listed` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes',
  `traffic_tickets_in_five_years` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes',
  `duis_in_ten_years` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes',
  `suspensions_or_revocations_in_ten_years` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes',
  `hav_any_of_the_following` tinyint(1) DEFAULT NULL COMMENT '0 - no, 1 -yes ',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_quote_auto`
--

INSERT INTO `general_quote_auto` (`id`, `selected_item`, `preferred_language`, `name`, `email`, `image`, `have_auto_insurance`, `accidents_in_last_five_years`, `accidents_by_anyone_who_was_not_listed`, `traffic_tickets_in_five_years`, `duis_in_ten_years`, `suspensions_or_revocations_in_ten_years`, `hav_any_of_the_following`, `created_at`) VALUES
(12, 'Umbrella Insurance', '1,2', 'WXZ', 'wxz@gmail.com', NULL, 0, 0, 1, 0, 1, 1, 0, '2020-03-09 19:23:31'),
(16, 'Umbrella Insurance', '2,3', 'test', 'test@gmail.com', '05da85027494ae896eed3ffe7c8a34d2.png', 1, NULL, NULL, NULL, NULL, NULL, 1, '2020-03-09 19:49:50'),
(17, 'Umbrella Insurance', '1,2', 'ABC', 'abc@gmail.com', '895a1e4307aba48c8f29515f92e52f9f.png', 1, NULL, NULL, NULL, NULL, NULL, 1, '2020-03-11 19:45:32'),
(18, 'Home Renters Condo Insurance', '4,6', 'rrr', 'rrr@gmail.com', 'b31b8db5a91e8771358df82b18ad255f.pdf', 1, NULL, NULL, NULL, NULL, NULL, 0, '2020-03-11 19:59:33'),
(19, 'Car Insurance', '1,2', 'ctx', 'ctx@gmail.com', NULL, 0, 0, 0, 1, 1, 1, 1, '2020-03-12 19:03:08'),
(20, 'Home Renters Condo Insurance', '2,3', 'wcg', 'wcg@gmail.com', NULL, 0, 1, 0, 1, 0, 1, 0, '2020-03-12 19:09:25'),
(23, 'Home Renters Condo Insurance', '1,3', 'qrt', 'qrt@gmail.com', NULL, 0, 0, 1, 1, 0, 1, 1, '2020-03-12 19:16:49'),
(24, 'Car Insurance', '1,2', 'ssd', 'ssd@gmail.com', NULL, 0, 0, 0, 1, 1, 0, 0, '2020-03-12 19:19:50'),
(25, 'Home Renters Condo Insurance', '1,2', 'ajk', 'ajk@gmail.com', 'ce9a793e7642d0c33cda46eead1713c0.pdf', 1, NULL, NULL, NULL, NULL, NULL, 0, '2020-03-12 19:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title2` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`id`, `title`, `title2`, `description`, `image`, `position`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Enjoy Your Happiness', 'Execute happiness by incredible opportunities', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.', 'ec38ae8cea01e3a2157943321db2c33f.jpg', 1, '2020-01-18 18:46:19', '2020-02-22 13:03:45', '1'),
(2, 'Enjoy Your Happiness', 'Travelling! how thrilling it can be', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.', 'c4ed50390e285e13b56299970863e2a1.jpg', 2, '2020-01-18 18:46:19', '2020-02-22 13:03:44', '1'),
(3, 'Enjoy Your Happiness', 'So you\'re thinking of moving, enjoy the effect', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.', '388a8711b3ee6c1b0412437432cc47bd.jpg', 3, '2020-01-18 18:46:19', '2020-02-22 13:03:43', '1');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` tinyint(3) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `title`, `created_at`, `modified_at`, `status`) VALUES
(1, 'English', '2020-02-05 20:04:28', '2020-02-13 17:06:30', '1'),
(2, 'Chinese', '2020-02-05 20:04:37', '2020-02-05 20:04:52', '1'),
(3, 'Indian', '2020-02-05 20:04:57', '2020-02-05 20:04:57', '1'),
(4, 'korean', '2020-02-05 20:05:04', '2020-02-05 20:05:04', '1'),
(6, 'Spanish', '2020-02-05 20:05:46', '2020-02-14 17:51:23', '1');

-- --------------------------------------------------------

--
-- Table structure for table `life_and_health`
--

CREATE TABLE `life_and_health` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `life_and_health`
--

INSERT INTO `life_and_health` (`id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(1, 'HEALTH MEDICARE', '2020-01-18 18:46:19', '2020-03-06 17:39:40', 'health-medicare', 1),
(2, 'LIFE ANNNITY', '2020-01-18 18:46:19', '2020-03-06 17:39:40', 'life-annnity', 1);

-- --------------------------------------------------------

--
-- Table structure for table `life_and_health_sub`
--

CREATE TABLE `life_and_health_sub` (
  `id` int(11) NOT NULL,
  `menu_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `life_and_health_sub`
--

INSERT INTO `life_and_health_sub` (`id`, `menu_id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(7, 1, 'UnitedHealth', '2020-02-06 19:23:11', '2020-03-06 17:39:59', 'unitedhealth', 1),
(8, 1, 'Kaiser Foundation', '2020-02-06 19:23:23', '2020-03-06 17:39:59', 'kaiser-foundation', 1),
(9, 1, 'Anthem Inc.', '2020-02-06 19:23:34', '2020-03-06 17:39:59', 'anthem-inc.', 1),
(10, 1, 'Humana', '2020-02-06 19:23:52', '2020-03-06 17:39:59', 'humana', 1),
(11, 2, 'Universal Life', '2020-02-06 19:24:05', '2020-03-06 17:39:59', 'universal-life', 1),
(12, 2, 'Term Life', '2020-02-06 19:24:15', '2020-03-06 17:39:59', 'term-life', 1),
(13, 2, 'Permanent Life', '2020-02-06 19:24:27', '2020-03-06 17:39:59', 'permanent-life', 1),
(14, 2, 'Whole Life', '2020-02-06 20:15:39', '2020-03-06 17:39:59', 'whole-life', 1);

-- --------------------------------------------------------

--
-- Table structure for table `life_and_health_sub_sub`
--

CREATE TABLE `life_and_health_sub_sub` (
  `id` int(11) NOT NULL,
  `submenu_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `make`
--

CREATE TABLE `make` (
  `id` int(11) NOT NULL,
  `year_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `make`
--

INSERT INTO `make` (`id`, `year_id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(7, 1, 'Hundai', '2020-02-06 19:23:11', '2020-03-07 12:53:28', 'hundai', 1),
(8, 1, 'Maruti Suzuki', '2020-02-06 19:23:23', '2020-03-07 12:53:44', 'maruti-suzuki', 1),
(9, 1, 'Jaguar', '2020-02-06 19:23:34', '2020-03-07 12:53:51', 'jaguar', 1),
(10, 2, 'Lambargini', '2020-02-06 19:23:52', '2020-03-07 12:53:58', 'lambargini', 1),
(11, 2, 'Ferrari', '2020-02-06 19:24:05', '2020-03-07 12:54:08', 'ferrari', 1),
(14, 3, 'Mercedes', '2020-03-07 15:55:53', '2020-03-07 15:55:53', 'mercedes', 1),
(15, 3, 'Range rover', '2020-03-07 15:56:28', '2020-03-07 15:56:28', 'range-rover', 1);

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE `models` (
  `id` int(11) NOT NULL,
  `year_id` tinyint(5) NOT NULL,
  `make_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`id`, `year_id`, `make_id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(19, 1, 8, 'Desire', '2020-03-07 15:29:26', '2020-03-07 15:29:26', 'desire', 1),
(20, 2, 11, 'SUV', '2020-03-07 15:55:19', '2020-03-07 15:55:19', 'suv', 1),
(21, 3, 15, 'Sedan', '2020-03-07 15:56:55', '2020-03-07 15:56:55', 'sedan', 1),
(22, 2, 11, 'Ferrari Portofino', '2020-03-12 18:08:44', '2020-03-12 18:08:44', 'ferrari-portofino', 1),
(23, 3, 15, 'RANGE ROVER SPORT', '2020-03-12 18:09:27', '2020-03-12 18:09:27', 'range-rover-sport', 1),
(24, 1, 8, 'Nexa', '2020-03-12 18:09:42', '2020-03-12 18:09:42', 'nexa', 1),
(25, 1, 8, 'Alto', '2020-03-12 18:09:51', '2020-03-12 18:09:51', 'alto', 1),
(26, 1, 8, 'Swift', '2020-03-12 18:10:03', '2020-03-12 18:10:03', 'swift', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `title`, `created_at`, `modified_at`, `status`) VALUES
(1, 1, 'Product1', '2020-02-14 17:48:56', '2020-03-04 19:55:43', 1),
(2, 1, 'Product2', '2020-02-14 17:48:56', '2020-03-04 19:55:43', 1),
(3, 1, 'Product3', '2020-02-14 17:48:56', '2020-03-04 19:55:43', 1),
(4, 2, 'Product4', '2020-02-14 17:48:56', '2020-03-04 19:55:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `p_and_c`
--

CREATE TABLE `p_and_c` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p_and_c`
--

INSERT INTO `p_and_c` (`id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(1, 'Personal Line', '2020-01-18 18:46:19', '2020-03-06 17:45:18', 'personal-line', 1),
(2, 'Commercial Line', '2020-01-18 18:46:19', '2020-03-06 17:36:21', 'commercial-line', 1),
(3, 'Small Business', '2020-02-21 18:40:02', '2020-03-06 17:36:21', 'small-business', 1);

-- --------------------------------------------------------

--
-- Table structure for table `p_and_c_sub`
--

CREATE TABLE `p_and_c_sub` (
  `id` int(11) NOT NULL,
  `menu_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p_and_c_sub`
--

INSERT INTO `p_and_c_sub` (`id`, `menu_id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(7, 1, 'Home/Renters/Condo Insurance', '2020-02-06 19:23:11', '2020-03-11 17:30:41', 'home-renters-condo-insurance', 1),
(8, 1, 'Car Insurance', '2020-02-06 19:23:23', '2020-03-11 17:24:55', 'car-insurance', 1),
(9, 1, 'Umbrella Insurance', '2020-02-06 19:23:34', '2020-03-06 17:36:47', 'umbrella-insurance', 1),
(10, 2, 'Professional Liability', '2020-02-06 19:23:52', '2020-03-06 17:36:47', 'professional-liability', 1),
(11, 2, 'Business Line', '2020-02-06 19:24:05', '2020-03-06 17:36:47', 'business-line', 1),
(12, 2, 'Type of Business', '2020-02-06 19:24:15', '2020-03-06 17:36:47', 'type-of-business', 1),
(13, 2, 'Manufacturing Insurance', '2020-02-06 19:24:27', '2020-03-06 17:36:47', 'manufacturing-insurance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `p_and_c_sub_sub`
--

CREATE TABLE `p_and_c_sub_sub` (
  `id` int(11) NOT NULL,
  `submenu_id` tinyint(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `p_and_c_sub_sub`
--

INSERT INTO `p_and_c_sub_sub` (`id`, `submenu_id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(7, 10, 'Physicians Liability', '2020-02-06 19:28:11', '2020-03-06 17:37:10', 'physicians-liability', 1),
(8, 10, 'Lawyers Liability', '2020-02-06 19:28:36', '2020-03-06 17:37:10', 'lawyers-liability', 1),
(9, 10, 'Anesthesiologist Liability ', '2020-02-06 19:28:59', '2020-03-06 17:37:10', 'anesthesiologist-liability-', 1),
(10, 10, 'Optometrist Liability ', '2020-02-06 19:29:11', '2020-03-06 17:37:10', 'optometrist-liability-', 1),
(11, 11, 'Builders Risk', '2020-02-06 19:29:30', '2020-03-06 17:37:10', 'builders-risk', 1),
(12, 11, 'Business Owners Policy', '2020-02-06 19:29:44', '2020-03-06 17:37:10', 'business-owners-policy', 1),
(13, 11, 'Business Liability', '2020-02-06 19:29:57', '2020-03-06 17:37:10', 'business-liability', 1),
(14, 11, 'Commercial Property', '2020-02-06 19:30:22', '2020-03-06 17:37:10', 'commercial-property', 1),
(15, 12, 'Bakeries', '2020-02-06 19:30:44', '2020-03-06 17:37:10', 'bakeries', 1),
(16, 12, 'Bars and Grills', '2020-02-06 19:30:54', '2020-03-06 17:37:10', 'bars-and-grills', 1),
(17, 12, 'Breweries', '2020-02-06 19:31:05', '2020-03-06 17:37:10', 'breweries', 1),
(18, 12, 'Candy and snack shops', '2020-02-06 19:31:20', '2020-03-06 17:37:10', 'candy-and-snack-shops', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE `quote` (
  `id` int(11) NOT NULL,
  `type_of_business` varchar(50) NOT NULL,
  `preferred_language` varchar(50) DEFAULT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `business_name` varchar(50) NOT NULL,
  `business_address` mediumtext NOT NULL,
  `mobile_number` varchar(10) NOT NULL,
  `business_structure_id` tinyint(5) NOT NULL,
  `primary_work` mediumtext NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quote`
--

INSERT INTO `quote` (`id`, `type_of_business`, `preferred_language`, `first_name`, `last_name`, `email`, `business_name`, `business_address`, `mobile_number`, `business_structure_id`, `primary_work`, `created_at`) VALUES
(1, 'Candy And Snack Shops', '1,2,3', 'test', 'Testing', 'test@gmail.com', 'XYZ', 'Triveninagar', '9824795495', 2, '456456', '2020-03-11 17:34:31'),
(2, 'Bakeries', NULL, 'nmj', 'jajaja', 'nmj@gmail.com', 'XYZ', 'Triveninagar', '9824795495', 2, 'Fintostic', '2020-03-11 17:35:43'),
(3, 'Candy And Snack Shops', '1,2', 'blahblah', 'xyz', 'blahblah@gmail.com', 'Kant', 'Triveninagar', '9824795495', 0, 'Primary', '2020-03-11 17:39:26'),
(4, 'Candy And Snack Shops', '1', 'hjkll', 'hjkll', 'hjkll@gmail.com', 'XYZ', 'Triveninagar', '9824795495', 0, '', '2020-03-11 17:52:54'),
(5, 'Candy And Snack Shops', '1,2', 'qwerty', 'poiuy', 'qwerty@gmail.com', 'XYZ', 'Triveninagar', '9824795495', 4, 'zsa', '2020-03-11 18:03:55');

-- --------------------------------------------------------

--
-- Table structure for table `selected_agency`
--

CREATE TABLE `selected_agency` (
  `id` int(11) NOT NULL,
  `agent_id` tinyint(5) NOT NULL,
  `user_id` tinyint(5) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selected_agency`
--

INSERT INTO `selected_agency` (`id`, `agent_id`, `user_id`, `created_at`, `status`) VALUES
(1, 5, 1, '2020-03-05 19:34:17', 1),
(2, 3, 1, '2020-03-05 19:38:13', 1),
(3, 1, 1, '2020-03-05 19:38:17', 1),
(4, 3, 2, '2020-03-11 19:13:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `setting_title` varchar(255) NOT NULL,
  `setting_key` varchar(255) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `is_required` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `setting_title`, `setting_key`, `setting_value`, `is_required`, `created_at`, `updated_at`) VALUES
(1, 'Site Title', 'SITE_TITLE', 'Solid Rock Group insurance', '1', '2020-01-16 19:03:53', '2020-02-21 12:13:34'),
(2, 'From Email', 'FROM_EMAIL', 'insurance@developerwork.in', '1', '2020-01-16 19:03:53', '2020-02-03 12:04:28'),
(3, 'From Email Title', 'FROM_EMAIL_TITLE', 'Insurance', '1', '2020-01-16 19:03:53', '2020-02-03 12:04:35'),
(4, 'Admin Email', 'ADMIN_EMAIL', 'contact@developerwork.in', '1', '2020-01-16 19:03:53', '2020-02-15 07:30:41'),
(5, 'SMTP HOST', 'SMTP_HOST', 'mail.developerwork.in', '1', '2020-01-16 19:03:53', '2020-01-16 13:33:53'),
(6, 'SMTP PORT', 'SMTP_PORT', '587', '1', '2020-01-16 19:03:53', '2020-01-16 13:33:53'),
(7, 'SMTP USER', 'SMTP_USERNAME', 'contact@developerwork.in', '1', '2020-01-16 19:03:53', '2020-01-16 13:33:53'),
(8, 'SMTP PASSWORD', 'SMTP_PASSWORD', 'Developer@123456', '1', '2020-01-16 19:03:53', '2020-01-16 13:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` int(11) NOT NULL,
  `facebook_link` varchar(255) NOT NULL,
  `instagram_link` varchar(255) NOT NULL,
  `twiter_link` varchar(255) NOT NULL,
  `link_in_link` varchar(255) NOT NULL,
  `pinterest_link` varchar(255) NOT NULL,
  `youtube_link` mediumtext NOT NULL,
  `skype_link` mediumtext NOT NULL,
  `google_plus_link` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `facebook_link`, `instagram_link`, `twiter_link`, `link_in_link`, `pinterest_link`, `youtube_link`, `skype_link`, `google_plus_link`, `created_at`, `modified_at`) VALUES
(1, 'https://www.facebook.com', 'https://instagram.com', 'https://twitter.com', 'https://www.linkedin.com', 'https://pinterest.com/', 'https://youtube.com', 'https://skype.com/', 'https://google.com', '2018-12-10 05:31:09', '2020-02-03 19:53:04');

-- --------------------------------------------------------

--
-- Table structure for table `static_page`
--

CREATE TABLE `static_page` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page`
--

INSERT INTO `static_page` (`id`, `title`, `description`, `image`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(1, 'Privacy Policy', '<p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 0px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', '', '2020-01-02 18:24:18', '2020-02-22 13:06:44', 'privacy-policy', '1'),
(2, 'Terms-conditions', '<p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 0px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', '70980dc8a843bb7bfb02916bb9807cd8.jpg', '2020-01-02 18:25:14', '2020-02-22 13:06:31', 'terms-conditions', '1'),
(4, 'Help and Support', '<p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><p style=\"margin-bottom: 12px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 0px; color: rgb(102, 102, 102); line-height: 1.8; font-size: 15px; text-align: justify; font-family: &quot;Open Sans&quot;, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', '', '2020-02-15 10:42:47', '2020-02-22 13:06:55', 'help-and-support', '1'),
(5, 'Mission', '<p style=\"margin-bottom: 12px; line-height: 1.8; text-align: justify;\" open=\"\" sans\",=\"\" sans-serif;\"=\"\"><font color=\"#666666\"><span style=\"font-size: 15px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></font><br></p>', '708bb1f39b3a707338fc802743909d28.jpg', '2020-02-15 10:42:47', '2020-02-22 14:19:15', 'mission', '1'),
(6, 'Vision', '<p style=\"margin-bottom: 12px; line-height: 1.8; text-align: justify;\" open=\"\" sans\",=\"\" sans-serif;\"=\"\"><font color=\"#666666\"><span style=\"font-size: 15px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></font><br></p>', 'ecfc796a28e6957e79157a35657f1051.jpg', '2020-02-15 10:42:47', '2020-02-22 14:19:19', 'vision', '1'),
(7, 'History', '<p style=\"margin-bottom: 12px; line-height: 1.8; text-align: justify;\" open=\"\" sans\",=\"\" sans-serif;\"=\"\"><font color=\"#666666\"><span style=\"font-size: 15px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></font><br></p>', '', '2020-02-15 10:42:47', '2020-02-25 18:46:35', 'history', '1'),
(8, 'Who we are', '<p style=\"margin-bottom: 12px; line-height: 1.8; text-align: justify;\" open=\"\" sans\",=\"\" sans-serif;\"=\"\"><font color=\"#666666\"><span style=\"font-size: 15px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></font><br></p>', '', '2020-02-15 10:42:47', '2020-02-25 18:46:32', 'who-we-are', '1');

-- --------------------------------------------------------

--
-- Table structure for table `types_of_insurance`
--

CREATE TABLE `types_of_insurance` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types_of_insurance`
--

INSERT INTO `types_of_insurance` (`id`, `title`, `description`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Car Insurance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan', '2020-02-26 18:32:39', '2020-02-29 18:21:30', '1'),
(2, 'Homeowners Insurance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan', '2020-02-26 18:33:45', '2020-02-29 18:21:30', '1'),
(3, 'Life Insurance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan', '2020-02-26 18:33:52', '2020-02-29 18:21:30', '1'),
(4, 'Renters Insurance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan', '2020-02-26 18:34:15', '2020-02-29 18:21:30', '1'),
(5, 'Business Insurance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan', '2020-02-26 18:34:23', '2020-02-29 18:21:30', '1'),
(6, 'Workers Compensation Insurance ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan', '2020-02-26 18:34:34', '2020-02-29 18:21:30', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `agency_name` varchar(55) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `phone_number` text NOT NULL,
  `address` mediumtext,
  `zip_code` varchar(25) NOT NULL,
  `types_of_insurance` varchar(50) NOT NULL,
  `about` mediumtext NOT NULL,
  `coverage` mediumtext NOT NULL,
  `service_offer` mediumtext NOT NULL,
  `working_hours` mediumtext NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `facebook_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `linkedin_link` varchar(255) NOT NULL,
  `youtube_link` varchar(255) NOT NULL,
  `image` mediumtext,
  `youtube_video_link` mediumtext NOT NULL,
  `otp` varchar(6) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `agency_name`, `email`, `password`, `phone_number`, `address`, `zip_code`, `types_of_insurance`, `about`, `coverage`, `service_offer`, `working_hours`, `website_url`, `facebook_link`, `twitter_link`, `linkedin_link`, `youtube_link`, `image`, `youtube_video_link`, `otp`, `created_at`, `modified_at`, `status`) VALUES
(1, '', '', 'Test', 'test@gmail.com', '6bae404d05d1eff6094de91ceaa685d903ac411349259164316f873a81682791551c075774feb4de70ba073daf98ce126999223113f30b363bafa1f469c324787slaPEs99Wig8cEQNXY5YXLNSh+dm0U+yFAwNIrQ0Vw=', '9824795495', '102 Grand St, Brooklyn, Ny 11211, America', '360004', '1,2,4,5', '<p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"margin-bottom: 12px; text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px;\"><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Personal Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Commercial Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div></div><p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', '<p><span class=\"fa fa-check\"></span> Minority/Women Owned</p><p><span class=\"fa fa-check\"></span> 10+ Years in Business</p><p><span class=\"fa fa-check\"></span> DA/Handicap Accessible</p><p><span class=\"fa fa-check\"></span> Free Parking</p><p><span class=\"fa fa-check\"></span> Languages: English, Espanol</p>', '<div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"row mx-n3 mb-0\" style=\"margin-right: -1rem !important; margin-left: -0.98rem !important;\"><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Mon</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Tue</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Wed</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Thu</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Fri</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sat</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sun</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd></div></div><div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"top-review-title\"></div></div>', 'http://developerwork.in/insurance/', 'https://www.facebook.com/', 'https://twitter.com/home', 'https://in.linkedin.com/', 'https://youtube.com/', '949a084f203cc32a42489968bc4c9242.jpg', 'https://www.youtube.com/embed/GdIxMsyAM_c', 'F81973', '2020-02-24 07:04:50', '2020-03-11 20:25:04', 1),
(2, '', '', 'The New Era', 'era@gmail.com', '26798583a24fff484eeaa1f68f6b9d595fce56755617e272b6c621adfd6234fdfb99b85bf578cd0b9b3045af985cf999783479e2c4f0343e52c5cc32ef9e3da1Ig7tiipr+v8Tu5ScLhYIyren86VvkHVpPPgs05ZcJwU=', '9826485458', 'Karol Baugh,\r\nNew Delhi 110001.', '110001', '', '', '', '', '', '', '', '', '', '', 'a59a036d39bb04b190612adada8bdd70.jpg', '', 'M07651', '2020-02-29 07:07:27', '2020-03-05 19:49:25', 1),
(3, '', '', 'SSG', 'ssg@gmail.com', '6bae404d05d1eff6094de91ceaa685d903ac411349259164316f873a81682791551c075774feb4de70ba073daf98ce126999223113f30b363bafa1f469c324787slaPEs99Wig8cEQNXY5YXLNSh+dm0U+yFAwNIrQ0Vw=', '8824795495', '102 Grand St, Brooklyn, Ny 11211, America', '360004', '1,2,4,5', '<p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"margin-bottom: 12px; text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px;\"><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Personal Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Commercial Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div></div><p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', '<p><span class=\"fa fa-check\"></span> Minority/Women Owned</p><p><span class=\"fa fa-check\"></span> 10+ Years in Business</p><p><span class=\"fa fa-check\"></span> DA/Handicap Accessible</p><p><span class=\"fa fa-check\"></span> Free Parking</p><p><span class=\"fa fa-check\"></span> Languages: English, Espanol</p>', '<div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"row mx-n3 mb-0\" style=\"margin-right: -1rem !important; margin-left: -0.98rem !important;\"><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Mon</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Tue</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Wed</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Thu</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Fri</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sat</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sun</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd></div></div><div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"top-review-title\"></div></div>', 'http://developerwork.in/insurance/', 'https://www.facebook.com/', 'https://twitter.com/home', 'https://in.linkedin.com/', 'https://youtube.com/', 'ae12f096c5c95b78e90e8e72edad5043.jpg', 'https://www.youtube.com/embed/GdIxMsyAM_c', 'F81973', '2020-02-24 07:04:50', '2020-03-11 20:23:37', 1),
(4, '', '', 'JK', 'jkjk@gmail.com', '6bae404d05d1eff6094de91ceaa685d903ac411349259164316f873a81682791551c075774feb4de70ba073daf98ce126999223113f30b363bafa1f469c324787slaPEs99Wig8cEQNXY5YXLNSh+dm0U+yFAwNIrQ0Vw=', '8824795495', '102 Grand St, Brooklyn, Ny 11211, America', '360001', '1,2,4,5', '<p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"margin-bottom: 12px; text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px;\"><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Personal Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Commercial Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div></div><p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', '<p><span class=\"fa fa-check\"></span> Minority/Women Owned</p><p><span class=\"fa fa-check\"></span> 10+ Years in Business</p><p><span class=\"fa fa-check\"></span> DA/Handicap Accessible</p><p><span class=\"fa fa-check\"></span> Free Parking</p><p><span class=\"fa fa-check\"></span> Languages: English, Espanol</p>', '<div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"row mx-n3 mb-0\" style=\"margin-right: -1rem !important; margin-left: -0.98rem !important;\"><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Mon</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Tue</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Wed</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Thu</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Fri</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sat</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sun</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd></div></div><div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"top-review-title\"></div></div>', 'http://developerwork.in/insurance/', 'https://www.facebook.com/', 'https://twitter.com/home', 'https://in.linkedin.com/', 'https://youtube.com/', 'b42d9924fcd7b516f42b355ee04945e2.jpg', 'https://www.youtube.com/embed/GdIxMsyAM_c', 'F81973', '2020-02-24 07:04:50', '2020-03-11 20:24:01', 1),
(5, '', '', 'sskg', 'sskg@gmail.com', '6bae404d05d1eff6094de91ceaa685d903ac411349259164316f873a81682791551c075774feb4de70ba073daf98ce126999223113f30b363bafa1f469c324787slaPEs99Wig8cEQNXY5YXLNSh+dm0U+yFAwNIrQ0Vw=', '8824795495', '102 Grand St, Brooklyn, Ny 11211, America', '110001', '1,2,4,5', '<p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"margin-bottom: 12px; text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px;\"><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Personal Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div><div class=\"col-md-6 col-lg-6 col-sm-12\" style=\"width: 357.5px;\"><div class=\"insurance-type\" style=\"margin-top: 0px;\"><h4>Commercial Insurance</h4><ul><li>ATV</li><li>Auto Mobile</li><li>Boat</li><li>Builders Risk/Home Construction</li><li>Collectible Auto</li><li>Condo</li><li>Farm</li></ul></div></div></div><p style=\"text-align: justify;\">Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"text-align: justify;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', '<p><span class=\"fa fa-check\"></span> Minority/Women Owned</p><p><span class=\"fa fa-check\"></span> 10+ Years in Business</p><p><span class=\"fa fa-check\"></span> DA/Handicap Accessible</p><p><span class=\"fa fa-check\"></span> Free Parking</p><p><span class=\"fa fa-check\"></span> Languages: English, Espanol</p>', '<div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"row mx-n3 mb-0\" style=\"margin-right: -1rem !important; margin-left: -0.98rem !important;\"><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Mon</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Tue</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Wed</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Thu</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Fri</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">8:30 AM - 5:00 PM</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sat</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd><dt class=\"col-2 col-lg-3 dt\" style=\"width: 86.6458px;\">Sun</dt><dd class=\"col-10 col-lg-9 dd\" style=\"width: 259.979px;\">Closed</dd></div></div><div class=\"top-review\" style=\"margin-top: 17.2292px; color: rgb(213, 19, 30);\"><div class=\"top-review-title\"></div></div>', 'http://developerwork.in/insurance/', 'https://www.facebook.com/', 'https://twitter.com/home', 'https://in.linkedin.com/', 'https://youtube.com/', 'ec3e0b4342668a6b2dc6ab613c65403c.jpg', 'https://www.youtube.com/embed/GdIxMsyAM_c', 'F81973', '2020-02-24 07:04:50', '2020-03-11 20:24:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_review`
--

CREATE TABLE `user_review` (
  `id` int(11) NOT NULL,
  `agent_id` tinyint(5) NOT NULL,
  `user_id` tinyint(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `ratting` tinyint(1) NOT NULL DEFAULT '0',
  `message` mediumtext NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_review`
--

INSERT INTO `user_review` (`id`, `agent_id`, `user_id`, `name`, `email`, `ratting`, `message`, `created_at`, `status`) VALUES
(1, 2, 1, '', '', 0, '', '2020-03-05 17:38:32', 0),
(5, 3, 2, 'xyz', 'xyz@gmail.com', 4, 'Best', '2020-03-11 19:24:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehical`
--

CREATE TABLE `vehical` (
  `id` int(11) NOT NULL,
  `quote_form_id` tinyint(5) NOT NULL,
  `year_id` tinyint(5) NOT NULL,
  `make_id` tinyint(5) NOT NULL,
  `models_id` tinyint(5) NOT NULL,
  `vin` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehical`
--

INSERT INTO `vehical` (`id`, `quote_form_id`, `year_id`, `make_id`, `models_id`, `vin`, `created_at`, `modified_at`, `status`) VALUES
(1, 8, 2, 11, 20, '123', '2020-03-09 16:54:00', '2020-03-09 16:54:00', 1),
(2, 9, 3, 15, 21, '456', '2020-03-09 19:14:08', '2020-03-09 19:14:08', 1),
(3, 10, 2, 11, 20, '123', '2020-03-09 19:21:30', '2020-03-09 19:21:30', 1),
(4, 11, 2, 11, 20, '123', '2020-03-09 19:21:54', '2020-03-09 19:21:54', 1),
(5, 12, 3, 15, 21, '123', '2020-03-09 19:23:31', '2020-03-09 19:23:31', 1),
(6, 13, 2, 11, 20, '123', '2020-03-09 19:46:34', '2020-03-09 19:46:34', 1),
(7, 14, 1, 8, 19, '456', '2020-03-09 19:47:08', '2020-03-09 19:47:08', 1),
(8, 15, 1, 8, 19, '456', '2020-03-09 19:47:52', '2020-03-09 19:47:52', 1),
(9, 16, 1, 8, 19, '456', '2020-03-09 19:49:50', '2020-03-09 19:49:50', 1),
(10, 17, 3, 15, 21, '123', '2020-03-11 19:45:32', '2020-03-11 19:45:32', 1),
(11, 17, 4, 15, 21, '456', '2020-03-11 19:45:32', '2020-03-11 19:45:32', 1),
(12, 18, 2, 11, 20, '456', '2020-03-11 19:59:33', '2020-03-11 19:59:33', 1),
(13, 18, 2, 11, 20, '4787', '2020-03-11 19:59:33', '2020-03-11 20:14:13', 1),
(14, 19, 2, 11, 22, '456', '2020-03-12 19:03:08', '2020-03-12 19:03:08', 1),
(15, 19, 1, 8, 25, '123', '2020-03-12 19:03:08', '2020-03-12 19:03:08', 1),
(16, 20, 1, 8, 25, '969', '2020-03-12 19:09:25', '2020-03-12 19:09:25', 1),
(17, 20, 2, 11, 22, '789', '2020-03-12 19:09:25', '2020-03-12 19:09:25', 1),
(18, 21, 0, 0, 0, 'asa', '2020-03-12 19:13:12', '2020-03-12 19:13:12', 1),
(19, 22, 1, 8, 25, '9889', '2020-03-12 19:16:13', '2020-03-12 19:16:13', 1),
(20, 22, 3, 15, 23, '789', '2020-03-12 19:16:13', '2020-03-12 19:16:13', 1),
(21, 23, 1, 8, 24, '3663', '2020-03-12 19:16:49', '2020-03-12 19:16:49', 1),
(22, 23, 3, 15, 23, '4568', '2020-03-12 19:16:49', '2020-03-12 19:16:49', 1),
(23, 24, 2, 11, 22, '546', '2020-03-12 19:19:50', '2020-03-12 19:19:50', 1),
(24, 24, 2, 11, 22, '698', '2020-03-12 19:19:50', '2020-03-12 19:19:50', 1),
(25, 25, 1, 8, 25, '129', '2020-03-12 19:25:15', '2020-03-12 19:25:15', 1),
(26, 25, 2, 11, 22, '632', '2020-03-12 19:25:15', '2020-03-12 19:25:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE `year` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seo_slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=''active'' , 0=''deactive'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `year`
--

INSERT INTO `year` (`id`, `title`, `created_at`, `modified_at`, `seo_slug`, `status`) VALUES
(1, '1991', '2020-03-07 12:09:43', '2020-03-07 12:09:43', '1991', 1),
(2, '1995', '2020-03-07 12:09:53', '2020-03-07 12:09:53', '1995', 1),
(3, '1992', '2020-03-07 12:54:28', '2020-03-07 12:54:28', '1992', 1),
(4, '1993', '2020-03-07 12:54:32', '2020-03-07 12:54:32', '1993', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonds`
--
ALTER TABLE `bonds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonds_sub`
--
ALTER TABLE `bonds_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonds_sub_sub`
--
ALTER TABLE `bonds_sub_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_structure`
--
ALTER TABLE `business_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `free_quote`
--
ALTER TABLE `free_quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_contact`
--
ALTER TABLE `general_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_quote`
--
ALTER TABLE `general_quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_quote_auto`
--
ALTER TABLE `general_quote_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `life_and_health`
--
ALTER TABLE `life_and_health`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `life_and_health_sub`
--
ALTER TABLE `life_and_health_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `life_and_health_sub_sub`
--
ALTER TABLE `life_and_health_sub_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `make`
--
ALTER TABLE `make`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_and_c`
--
ALTER TABLE `p_and_c`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_and_c_sub`
--
ALTER TABLE `p_and_c_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_and_c_sub_sub`
--
ALTER TABLE `p_and_c_sub_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selected_agency`
--
ALTER TABLE `selected_agency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_page`
--
ALTER TABLE `static_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types_of_insurance`
--
ALTER TABLE `types_of_insurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_review`
--
ALTER TABLE `user_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehical`
--
ALTER TABLE `vehical`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `year`
--
ALTER TABLE `year`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bonds`
--
ALTER TABLE `bonds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bonds_sub`
--
ALTER TABLE `bonds_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bonds_sub_sub`
--
ALTER TABLE `bonds_sub_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_structure`
--
ALTER TABLE `business_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `free_quote`
--
ALTER TABLE `free_quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `general_contact`
--
ALTER TABLE `general_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `general_quote`
--
ALTER TABLE `general_quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `general_quote_auto`
--
ALTER TABLE `general_quote_auto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `life_and_health`
--
ALTER TABLE `life_and_health`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `life_and_health_sub`
--
ALTER TABLE `life_and_health_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `life_and_health_sub_sub`
--
ALTER TABLE `life_and_health_sub_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `make`
--
ALTER TABLE `make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `models`
--
ALTER TABLE `models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `p_and_c`
--
ALTER TABLE `p_and_c`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `p_and_c_sub`
--
ALTER TABLE `p_and_c_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `p_and_c_sub_sub`
--
ALTER TABLE `p_and_c_sub_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `quote`
--
ALTER TABLE `quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `selected_agency`
--
ALTER TABLE `selected_agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `social_links`
--
ALTER TABLE `social_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `static_page`
--
ALTER TABLE `static_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `types_of_insurance`
--
ALTER TABLE `types_of_insurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_review`
--
ALTER TABLE `user_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vehical`
--
ALTER TABLE `vehical`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `year`
--
ALTER TABLE `year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

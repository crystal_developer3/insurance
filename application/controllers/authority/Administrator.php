<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function view($page_number = "") {
        $settings = array(
            "url" => site_url() . "authority/administrator/view/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "id,full_name,email_address,mobile_number_1");
        $data = $this->common_model->get_pagination('administrator', $conditions, $settings);
        if (isset($this->session->user_msg)) {
            $data = array_merge($data, array("success" => $this->session->user_msg));
            $this->session->unset_userdata('user_msg');
        }
        unset($settings, $conditions);
        $this->load->view('authority/administrator/view', $data);
    }

    public function edit($id = "") {
        $this->load->helper("form");
        $data = array("form_title" => 'Edit profile');
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data('administrator', $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/administrator/view");
            }
        } else {
            redirect("authority/administrator/view");
        }
        /* Form Validation */
        $this->form_validation->set_rules('full_name', 'full name', 'required', array('required' => 'Please enter user name'));
        $this->form_validation->set_rules('email_address', 'email addres', 'required|valid_email|is_unique_with_except_record[administrator.email_address.id.' . $data['id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        $this->form_validation->set_rules('mobile_number_1', 'mobile number', 'required|min_length[10]|max_length[15]', array('required' => 'Please enter mobile number'));
        // $this->form_validation->set_rules('mobile_number_2', 'mobile number', 'required|min_length[10]|max_length[15]');

        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $error = false;
            $records = $_POST;
            if (!is_dir(PROFILE_PICTURE)) {
                mkdir(PROFILE_PICTURE);
                @chmod(PROFILE_PICTURE,0777);
            }
            if (isset($_FILES['profile_photo']) && $_FILES['profile_photo']['name'] != "") {
                $config['upload_path'] = PROFILE_PICTURE;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('profile_photo')) {
                    $data['profile_photo_error'] = $this->upload->display_errors();
                    $error = true;
                } else {
                    /* delete old photo */
                    if (!is_null($data['profile_photo'])) {
                        $path = $config['upload_path'] . $data['profile_photo'];
                        if (is_file($path)) {
                            @unlink($path);
                        }
                    }
                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name = $upload_data['file_name'];
                    chmod($config['upload_path'] . $file_name, 0777);
                    $records["profile_photo"] = $file_name;
                }
            } else {
                $records["profile_photo"] = $data['profile_photo'];
            }
            if (isset($_FILES['white_logo']) && $_FILES['white_logo']['name'] != "") {
                $config['upload_path'] = PROFILE_PICTURE;
                $config['allowed_types'] = 'gif|jpg|png|jpeg|"';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('white_logo')) {
                    $error_msg = array('error' => $this->upload->display_errors());
                    $error = true;
                } else {
                    /* delete old photo */
                    if (!is_null($data['white_logo'])) {
                        $path = $config['upload_path'] . $data['white_logo'];
                        if (is_file($path)) {
                            @unlink($path);
                        }
                    }
                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name = $upload_data['file_name'];
                    chmod($config['upload_path'] . $file_name, 0777);
                    $records["white_logo"] = $file_name;
                }
            } else {
                $records["white_logo"] = $data['white_logo'];
            }
            if (!$error) {
                $conditions = array(
                    "where" => array("id" => $data['id']),
                );
                $this->common_model->update_data('administrator', $records, $conditions);
                if ($data['id'] == $this->session->user_info['id']) {
                    $this->common_functions->updatesession($data['id']);
                }
                $data = array_merge($data, $_POST);
                $_POST = array();
                $data = array_merge($data, $_POST);
                $this->session->set_flashdata('success', 'Record updated successful');
                redirect($_SERVER['HTTP_REFERER']); 
            }
        }
        $this->load->view('authority/administrator/add-edit', $data);
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id,profile_photo", "where" => array("id" => intval($id)));
            $user = $this->common_model->select_data('administrator', $conditions);
            if ($user['row_count'] > 0) {
                $photo = $user['data'][0]['profile_photo'];
                if (!is_null($photo)) {
                    $path = PROFILE_PICTURE . $photo;
                    if (is_file($path)) {
                        @unlink($path);
                    }
                }
            }
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data('administrator', $conditions);
            $this->session->user_msg = "Record deleted successfully";
            redirect("authority/administrator/view");
        } else {
            redirect("authority/administrator/view");
        }
    }

    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data('administrator', $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/administrator/view");
            }
        } else {
            redirect("authority/administrator/view");
        }
        $this->load->view('authority/administrator/details', $data);
    }

}

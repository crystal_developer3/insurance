<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = array();
		/* FOR CHECKING REMEMBER ME DETAILS */
        if (get_cookie("REMEMBER")) {
            if (decrypt_cookie(get_cookie("REMEMBER")) == "yes") {
                $data['email'] = decrypt_cookie(get_cookie("USER"));
                $data['password'] = decrypt_cookie(get_cookie("PWD"));
                $data['remember_me'] = "yes";
            }
        }   
        if ($this->input->post())
		{	
			$data = $this->input->post();
			$email = $this->input->post('email');
			$password = $this->input->post('password');	
			$this->form_validation->set_rules('email', 'Please Enter Useremail', 'required');
			$this->form_validation->set_rules('password', 'Please Enter Password', 'required');
			$user = '';
			if ($this->form_validation->run() == FALSE)
	        {
	        	// $this->load->view('login');
	        	// $this->session->set_flashdata('error','Please Enter Useremail and Password...!');
				// redirect($_SERVER['HTTP_REFERER']);
				$data = array_merge($data, $_POST);
	        }
	        else
	        {
				$password = $this->input->post('password');	
				$usrlogin = $this->production_model->email_or_mobile_login('user',$email);
				// $usrlogin = $this->production_model->get_all_with_where('user','','',array('email'=>$email));
				// echo"<pre>"; echo $this->db->last_query(); print_r($usrlogin); exit;
				if ($usrlogin !=null) {
					$usrdbpass = $this->encryption->decrypt($usrlogin[0]['password']);
					if($usrlogin[0]['status'] == '1'){							
						if($password == $usrdbpass)
						{ 
							// cookie start //
							if ($this->input->post("remember_me") == "yes") {
	                            $time = time() + 60 * 60 * 24;
	                            $cookie = array(
	                                'name' => 'USER',
	                                'value' => encrypt_cookie($email),
	                                'expire' => $time,
	                            );
	                            set_cookie($cookie);
	                            $cookie = array(
	                                'name' => 'PWD',
	                                'value' => encrypt_cookie($password),
	                                'expire' => $time,
	                            );
	                            set_cookie($cookie);
	                            $cookie = array(
	                                'name' => 'REMEMBER',
	                                'value' => encrypt_cookie("yes"),
	                                'expire' => $time,
	                            );
	                            set_cookie($cookie);
	                        } else {
	                            delete_cookie("USER");
	                            delete_cookie("PWD");
	                            delete_cookie("REMEMBER");
	                        }
							// cookie end //
							$session = array(
								'login_id' => $usrlogin[0]['id'],
								'username' => $usrlogin[0]['agency_name'],
								'useremail' => $usrlogin[0]['email'],
								'useremobile' => $usrlogin[0]['phone_number'],
							);		
							$this->session->set_userdata($session);		
							$user = 'user';	
							redirect('home');
						}	
						else{
							$this->session->set_flashdata('error','Your password is incorrect...!');
							redirect('login');
						}						
					}
					else{
						$this->session->set_flashdata('error','Your account is not active , please verify your mail or contact admin...!');
						redirect('login');
					}					 
				} 
				else{
					$this->session->set_flashdata('error','User not available...!');
					redirect('login');
				}
			}
			
		}
        // echo"<pre>"; print_r($data); exit;
		$this->load->view('login',$data);
	}
	
	public function Logout()
	{
		$array_items = array('login_id', 'username', 'useremail');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url());		
	}
	public function forgot_password()
	{
		$email = strtolower($this->input->post('UserEmail'));
		$this->form_validation->set_rules('UserEmail', 'UserEmail', 'required');		
		if ($this->form_validation->run() == TRUE)
		{
			$get_record = $this->production_model->get_all_with_where('user','','',array('email'=>$email));
			// echo $pass = $this->encryption->encrypt('123456'); exit;
			// echo"<pre>"; print_r($get_record); exit; 
			if ($get_record !=null){				
				if($email == $get_record[0]['email'])
				{
					$tocken = rand(111111,999999);
					$data = array('tocken'=>$tocken);
					$data['password'] = $this->encryption->decrypt($get_record[0]['password']); 

					// echo"<pre>"; print_r($data); exit;
					/*call reset password link*/
					$update_tocken = array('otp'=>$tocken);
					$record = $this->production_model->update_record('user',$update_tocken,array('email'=>$email));
					$data['email'] = $email;
					// $this->load->view('mail_form/forgot_password/forgot_email', $data);

					// Add Mail Code Here....

					$send_mail = $this->production_model->mail_send(SITE_TITLE.' Forgot Password',array($email),'','mail_form/forgot_password/forgot_email',$data,'');
					// echo"<pre>"; print_r($send_mail); exit;
					// if ($send_mail == 1) {
			            $this->session->set_flashdata('success','Otp send successfully please check your mail-id');
			            $this->load->view('mail_form/forgot_password/check_otp',$data);
			        // }
				}
				else
				{
					$this->session->set_flashdata('error','Your Email '.$email.' is not in our record...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
			else
			{
				$this->session->set_flashdata('error','Your Email '.$email.' is not in our record...!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
		else
		{
			$this->session->set_flashdata('error','Please Enter Email First...!');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}

	function check_otp(){
		$data = $this->input->post();
		// echo"<pre>"; print_r($data); exit;
		$response_array = array();
		$chek_otp_is_valid = $this->production_model->get_all_with_where('user','','',array('email'=>$data['email'],'otp'=>$data['otp']));
		if(isset($chek_otp_is_valid) && $chek_otp_is_valid !=null){
			$response_array['success'] = true;
			$session = array(
				'verified_mail' => $data['email'],
				'verified_otp' => $data['otp'],
			);		
			$this->session->set_userdata($session);
		}
		else{
			$response_array['error'] = false;
			$response_array['message'] = 'Otp is invalid please check your mail';
		}
		echo json_encode($response_array);
	}
	function reset_password(){
		$data = $this->input->post();

		$get_data = $this->input->get();
		$otp = $this->session->userdata('verified_otp');
		$email = $this->session->userdata('verified_mail');
		$data['password'] = isset($data['password']) ? $this->encryption->encrypt($data['password']) : '';
		$data['otp'] = '';
		unset($data['confirm_password']);

		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE)
        {
        	$this->load->view('reset-password');
            // $this->session->set_flashdata('error','Please All The Fields Required Data...!');
			// redirect($_SERVER['HTTP_REFERER']);
        }
        else{
        	$record = $this->production_model->update_record('user',$data,array('email'=>$email,'otp'=>$otp));

	        if ($record == 1) {
	            $this->session->set_flashdata('success', 'Password reset done successfully...!');
	            redirect(base_url('login'));
	        }
	        else
	        {
	            $this->session->set_flashdata('error', 'Please Try After Some time...!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
        }
	}
}
?>

<?php
    $success = $this->session->flashdata('success');
    $error = $this->session->flashdata('error');                    
    if (isset($success))
    {
        ?>
            <div class="col-md-12 text-center offset4 alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?= $success;?>
            </div>  
        <?php
    }                            
    elseif(isset($error))
    {
        ?>
            <div class="col-md-12 text-center alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?= $error;?>
            </div>  
        <?php          
    }
?> 

<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Service</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        	<?php $this->load->view('authority/common/messages')?>
        	
            <div class="row">
	        	<div class="col-md-6">
	        		<?php
	        			if ( isset($details) && $details == null) {
	        			?>
	        				<a href="<?= base_url('authority/service/add');?>" class="btn btn-secondary btn-flat">Add</a>
	        			<?php }
	        		?>
	        	</div>
                <div class="col-md-12" style="margin-top: 1%;">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Title english</th>
                                        <th>Title hindi</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                	<?php
										if (isset($details) && $details !=null){
		                                    foreach ($details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
			                                    <tr>
							                        <td><?= $key+1;?></td>
							                        <td><?= $value['title'];?></td>
							                        <td><?= $value['title_hindi'];?></td>
							                        <td>
							                            <img src="<?= base_url(SERVICE_IMAGE.'thumbnail/').$value['image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
							                        </td>
							                        <td>
										                <a href="<?= base_url('authority/service/edit/'.$id);?>" class="btn bg-gradient-primary btn-flat btn-xs"><i class="fas fa-edit"></i></a>

														<?php 
															if($value['status'] == '1'){
																echo '<span class="btn bg-gradient-success btn-flat btn-xs change-status" data-table="service" data-id="'.$id.'" data-current-status="1"><i class="fa fa-check" aria-hidden="true"></i></span>';
																} else {
																echo '<span class="btn bg-gradient-danger btn-flat btn-xs change-status" data-table="service" data-id="'.$id.'" data-current-status="0"><i class="fa fa-times" aria-hidden="true"></i></span>';
															} 
														?>
							                        </td>
							                    </tr>
			                                <?php } 
			                            }
			                            else{
			                            	?>
			                            		<tr data-expanded="true">
													<td colspan="7" align="center">Records not found</td>
												</tr>
			                            	<?php
			                            }
			                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>

<script type="text/javascript">
	$(document).ready(function () {			
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_status',
				data: post_data,
				async: false,
				beforeSend: function() {
	                $('.loading').css({'display':'block'});
	            },
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('bg-gradient-danger bg-gradient-success');
						if(current_element.hasClass('bg-gradient-success')){
							current_element.html('<i class="fa fa-check" aria-hidden="true"></i>');
							current_element.attr('data-current-status','1');
							} else {
							current_element.html('<i class="fa fa-times" aria-hidden="true"></i>');
							current_element.attr('data-current-status','0');
						}
						} else {
						window.location = window.location.href;
					}
				},
				complete: function() {
	                setTimeout(function() {
	                    $('.loading').css({'display':'none'});
	                }, 100);
	            },
			});
		});
	});
</script>
<?php $this->view('authority/common/footer'); ?>									
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= SITE_TITLE; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/toastr.min.css">
        <!-- summernote -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/summernote-bs4.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/select2.min.css">
        <link rel="stylesheet" href="<?= base_url()?>assets/authority/css/select2-bootstrap4.min.css">

        <!-- New added -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/developer.css">   
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript">
            var BASE_URL = '<?= base_url(); ?>';
        </script>
        <style type="text/css">
            .table-bordered td, .table-bordered th {
                vertical-align: middle;
            }
        </style>
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="loading"></div>
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="<?= base_url('authority/dashboard');?>" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="<?= base_url('authority/dashboard');?>" class="nav-link">Dashboard</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="javascript:void(0)" class="nav-link">Welcome: <?php echo $this->session->user_info['full_name']; ?></a>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->
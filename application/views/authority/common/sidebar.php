<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <?php
                if ($this->session->user_info['profile_photo'] != '') {
                    $path = base_url() . PROFILE_PICTURE . $this->session->user_info['profile_photo'];
                } else {
                    $path = base_url() . 'assets/uploads/default_img.png';
                }
            ?>
            <div class="image">
                <a href="<?= base_url('authority/dashboard');?>">
                    <img src="<?= $path;?>" class="img-circle elevation-2" alt="User Image" width="100">
                </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview <?= get_active_class("my-account")['main_class'];?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            My account
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?= site_url();?>authority/administrator/edit/<?= $this->session->user_info['id']; ?>" class="nav-link <?= get_active_class("profile")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= site_url();?>authority/account/change-password" class="nav-link <?= get_active_class("change-password")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Change password</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('authority/login/logout');?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sign out</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?= get_active_class("home-slider")['main_class'];?>">
                    <a href="<?= base_url('authority/home-slider');?>" class="nav-link">
                        <i class="nav-icon far fa-image"></i>
                        <p>Home slider</p>
                    </a>
                </li>
                <li class="nav-item <?= get_active_class("about")['main_class'];?>">
                    <a href="<?= base_url('authority/about');?>" class="nav-link">
                        <i class="nav-icon far fa-image"></i>
                        <p>About</p>
                    </a>
                </li>
                               
                <li class="nav-item <?= get_active_class("contact-us")['main_class'];?>">
                    <a href="<?= base_url('authority/contact-us');?>" class="nav-link">
                        <i class="nav-icon far fa-user"></i>
                        <p>Contact us</p>
                    </a>
                </li>
                <li class="nav-item <?= get_active_class("user")['main_class'];?>">
                    <a href="<?= base_url('authority/user');?>" class="nav-link">
                        <i class="nav-icon far fa-user"></i>
                        <p>User</p>
                    </a>
                </li>
                <li class="nav-item has-treeview <?= get_active_class("page")['main_class'];?>">
                    <a href="<?= base_url('authority/static-page');?>" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Static Pages</p>
                    </a>
                </li>
                <li class="nav-item <?= get_active_class("social-links")['main_class'];?>">
                    <a href="<?= base_url('authority/social-links');?>" class="nav-link">
                        <i class="nav-icon far fas fa-share-alt"></i>
                        <p>Social links</p>
                    </a>
                </li>
                <li class="nav-item has-treeview <?= get_active_class("faq")['main_class'];?>">
                    <a href="<?= base_url('authority/faq');?>" class="nav-link">
                        <i class="nav-icon fas fa-question"></i>
                        <p>Faq</p>
                    </a>
                </li>

                <li class="nav-item has-treeview <?= get_active_class("types-of-insurance")['main_class'];?>">
                    <a href="<?= base_url('authority/types-of-insurance');?>" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Types Of Insurance</p>
                    </a>
                </li>

                <li class="nav-item has-treeview <?= get_active_class("forms")['main_class'];?>">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="nav-icon fas fa-bars"></i>
                        <p>
                            Forms
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/general-contact');?>" class="nav-link <?= get_active_class("general-contact")['sub_class'];?>">
                                <i class="nav-icon fas fa-address-card"></i>
                                <p>General contact</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/general-quote');?>" class="nav-link <?= get_active_class("general-quote")['sub_class'];?>">
                                <i class="nav-icon fas fa-quote-left"></i>
                                <p>General quote</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/general-quote-auto');?>" class="nav-link <?= get_active_class("general-quote-auto")['sub_class'];?>">
                                <i class="nav-icon fas fa-bus"></i>
                                <p>General quote auto</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/quote');?>" class="nav-link <?= get_active_class("quote")['sub_class'];?>">
                                <i class="nav-icon fas fa-quote-right"></i>
                                <p>Quote</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/free-quote');?>" class="nav-link <?= get_active_class("free-quote")['sub_class'];?>">
                                <i class="nav-icon fas fa-quote-right"></i>
                                <p>Free quote</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/business-structure');?>" class="nav-link <?= get_active_class("business-structure")['sub_class'];?>">
                                <i class="nav-icon fas fa-legal"></i>
                                <p>Business Structure</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/language');?>" class="nav-link <?= get_active_class("language")['sub_class'];?>">
                                <i class="nav-icon fas fa-language"></i>
                                <p>Languages</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/category');?>" class="nav-link <?= get_active_class("category")['sub_class'];?>">
                                <i class="nav-icon fas fa-list-alt"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/product');?>" class="nav-link <?= get_active_class("product")['sub_class'];?>">
                                <i class="nav-icon fas fa-shopping-basket"></i>
                                <p>Product</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/year');?>" class="nav-link <?= get_active_class("year")['sub_class'];?>">
                                <i class="nav-icon fas fa-calendar"></i>
                                <p>Year</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/make');?>" class="nav-link <?= get_active_class("make")['sub_class'];?>">
                                <i class="nav-icon fas fa-industry"></i>
                                <p>Make</p>
                            </a>
                        </li>
                        <li class="nav-item ">   
                            <a href="<?= base_url('authority/models');?>" class="nav-link <?= get_active_class("models")['sub_class'];?>">
                                <i class="nav-icon fas fa-industry"></i>
                                <p>Models</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview <?= get_active_class("p-and-c-menu")['main_class'];?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-bars"></i>
                        <p>
                            P & C
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ">
                            <a href="<?= base_url('authority/p-and-c');?>" class="nav-link <?= get_active_class("p-and-c")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>P & C Main</p>
                            </a>
                            <a href="<?= base_url('authority/p-and-c-sub');?>" class="nav-link <?= get_active_class("p-and-c-sub")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>P & C Sub Menu</p>
                            </a>
                            <a href="<?= base_url('authority/p-and-c-sub-sub');?>" class="nav-link <?= get_active_class("p-and-c-sub-sub")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>P & C sub of sub Menu</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview <?= get_active_class("life-and-health-menu")['main_class'];?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-bars"></i>
                        <p>
                            Life & Health
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ">
                            <a href="<?= base_url('authority/life-and-health');?>" class="nav-link <?= get_active_class("life-and-health")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Life & Health Main</p>
                            </a>
                            <a href="<?= base_url('authority/life-and-health-sub');?>" class="nav-link <?= get_active_class("life-and-health-sub")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Life & Health Sub</p>
                            </a>
                            <a href="<?= base_url('authority/life-and-health-sub-sub');?>" class="nav-link <?= get_active_class("life-and-health-sub-sub")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Life & Health Sub of sub</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview <?= get_active_class("bonds-menu")['main_class'];?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-bars"></i>
                        <p>
                            Bonds
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ">
                            <a href="<?= base_url('authority/bonds');?>" class="nav-link <?= get_active_class("bonds")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Bonds Main</p>
                            </a>
                            <!-- <a href="<?= base_url('authority/bonds-sub');?>" class="nav-link <?= get_active_class("bonds-sub")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Bonds Sub</p>
                            </a>
                            <a href="<?= base_url('authority/bonds-sub-sub');?>" class="nav-link <?= get_active_class("bonds-sub-sub")['sub_class'];?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Bonds sub of sub</p>
                            </a> -->
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?= get_active_class("settings")['main_class'];?>">
                    <a href="<?= base_url('authority/settings');?>" class="nav-link">
                        <i class="nav-icon far fas fa-cog"></i>
                        <p>Settings</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
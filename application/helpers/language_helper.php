<?php
if (!function_exists('language')) {
	function language($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('language','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_language_titles')) {
	function get_language_titles($where_in_array=""){
		$CI = & get_instance();
		$where_in_array = explode(",", $where_in_array);
		$preferred_language = $CI->production_model->get_all_with_where_in('language','id','desc','id',$where_in_array);
        // $preferred_language = array_column($preferred_language, 'title');
		// echo "<pre>";print_r($preferred_language);exit;
        $preferred_language = ucwords(implode(", ", array_column($preferred_language, 'title')));

		if (isset($preferred_language) && $preferred_language !=null) {
			return $preferred_language;
		}
		else{
			return "";
		}
	}
}
if (!function_exists('language_front')) {
	function language_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('language','id','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
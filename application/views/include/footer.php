<!-- Footer -->
<!-- <footer id="footer" class="secondary-bg">
	<div class="container">
	  <div class="row">
	    <div class="col-lg-4">
	      <div class="footer-widgets">
	        <div class="footer-logo"><h1 class="text-uppercase text-light">Logo</h1></div>
	        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized dislike with men.</p>
	        <div class="follow-us">
	            <h5 class="mb-2">Follow Us</h5>
	          <ul>
	          	<?php $social_media_link = get_social_links(); ?>
                <li><a href="<?= $social_media_link !=null ? $social_media_link['facebook_link'] : '';?>" class="fa fa-facebook"></a></li>
                <li><a href="<?= $social_media_link !=null ? $social_media_link['twiter_link'] : '';?>" class="fa fa-twitter"></a></li>
                <li><a href="<?= $social_media_link !=null ? $social_media_link['link_in_link'] : '';?>" class="fa fa-linkedin"></a></li>
                <li><a href="<?= $social_media_link !=null ? $social_media_link['google_plus_link'] : '';?>" class="fa fa-google-plus"></a></li>
                <li><a href="<?= $social_media_link !=null ? $social_media_link['instagram_link'] : '';?>"><i class="fa fa-instagram"></i></a></li>
	          </ul>
	        </div>
	      </div>
	    </div>
	    <div class="col-lg-8">
	      <div class="row">
	        <div class="col-lg-3">
	          <div class="footer-widgets">
	            <h5>Our Products</h5>
	            <ul>
	              <li><a href="#">Business Loan</a></li>
	              <li><a href="#">Personal Loan</a></li>
	              <li><a href="#">Loan Againest Property</a></li>
	              <li><a href="#">Home Loan</a></li>
	              <li><a href="#">Car Loan</a></li>
	              <li><a href="#">Insta Loan</a></li>
	            </ul>
	          </div>
	        </div>
	        <div class="col-lg-4">
	          <div class="footer-widgets">
	            <h5>Usefull Links</h5>
	              <ul>
	                <li><a href="dsa-ragistration.html">Bank DSA Registration 2020</a></li>
	                <li><a href="signin.html">DSA Login</a></li>
	                <li><a href="credit-score.html">Check Credit Score</a></li>
	                <li><a href="about-us.html">About Us</a></li>
	                <li><a href="privacy_policy.html">Privacy Policy</a></li>
	                <li><a href="career.html">Careers</a></li>
	              </ul>
	          </div>
	        </div>
	        <div class="col-lg-5">
	          <div class="footer-widgets">
	            <h5>Contact Information</h5>
	            <ul class="address-widget-list">
	              <li>
	                <i class="fa fa-map-marker" ariea-hidden="true"></i>
	                <p><?=nl2br(get_main_address())?></p>
	              </li>
	              <li>
	                <i class="fa fa-phone" ariea-hidden="true"></i>
	                <p><?=get_main_phone_no1()?></p>
	              </li>
	              <li>
	                <i class="fa fa-envelope" ariea-hidden="true"></i>
	                <p><?=get_main_email_address()?></p>
	              </li>
	            </ul>
	            <div class="footer-subscribe-form">
	              <form action="#">
	                <div class="xzopro-subscribe-form">
	                  <input type="email" name="EMAIL" placeholder="Your email address" required>
	                  <input type="submit" value="Subscribe">
	                </div>
	              </form>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="footer-bottom">
	  <div class="container">
	    <p>&copy; Copyright <?=date('Y')?> Demo All Rights Reserved</p>
	  </div>
	</div>
</footer> -->
<!-- /Footer --> 
<!-- 
<footer id="tg-footer" class="tg-footer tg-haslayout">
	<div class="tg-footerarea">
		<div class="container">
			<div class="m-b10">
				<div class="wt-divider bg-gray-dark"></div>
			</div>
			<div class="row">
				<div class="tg-threecolumns">
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<div class="tg-footercol">
							<strong class="tg-logo">
								<a href="<?=base_url()?>">
									<img src="<?= base_url()?>assets/images/logo.png" alt="image description">
								</a>
							</strong>
							<div class="clearfix"></div>
							<?php
								$condition = array('status'=>'1');
								$about_details = about($condition);
          						if (isset($about_details) && $about_details !=null) {?>

									<p>
									<?=substr(strip_tags($about_details[0]['description']), 0,229);?></p>
									<?php
								}
							?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
						<div class="tg-footercol tg-widget tg-widgetnavigation">
							<div class="tg-widgettitle">
								<h3>Useful Links</h3>
							</div>
							<div class="tg-widgetcontent">
								<ul>
									<li><a href="<?=base_url()?>">Home</a></li>
									<li><a href="<?=base_url('about')?>">Author</a></li>
									<li><a href="<?=base_url('our-books')?>">Our Books</a></li>
									<li><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="tg-footercol">
							<div class="tg-widgettitle">
								<h3>Contact Us</h3>
							</div>
							<ul class="tg-contactinfo">
								<li>
									<i class="icon-apartment"></i>
									<address><?=nl2br(get_main_address())?></address>
								</li>
								<li>
									<i class="icon-phone-handset"></i>
									<span>
										<em><?=get_main_phone_no1()?></em>
										<em><?=get_main_phone_no2()?></em>
									</span>
								</li>
								<li>
									<i class="icon-envelope"></i>
									<span>
										<em><a href="mailto:sukhbirkapoor@hotmail.com">sukhbirkapoor@hotmail.com</a></em>
										<em><a href="mailto:registrar@sikh-uni.ac.uk">registrar@sikh-uni.ac.uk</a></em>
									</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="tg-footerbar">
		<a id="tg-btnbacktotop" class="tg-btnbacktotop" href="javascript:void(0);"><i class="icon-chevron-up"></i></a>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
					<span class="tg-copyright">&copy; 2020 All Rights Reserved By Book Library</span>
				</div>
			</div>
		</div>
	</div>
</footer> -->

<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-sm-3 offset-md-3">
                <div class="single-footer-widget">
                    <h3>Contact Info</h3>
                    <ul class="footer-contact-info">
                        <li><span>Location:</span> <?=nl2br(get_main_address())?></li>
                        <li><span>Email:</span> <a href="mailto:<?=get_main_email_address()?>"><?=get_main_email_address()?></a></li>
                        <li><span>Phone:</span> <a href="tel:<?=get_main_phone_no1()?>"><?=get_main_phone_no1()?> </a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Quick Links</h3>
                    <ul class="footer-quick-links">
                        <li><a href="<?=base_url()?>">Home</a></li>
                        <li><a href="<?=base_url('about')?>">About Us</a></li>
                        <li><a href="<?=base_url('faq')?>">Faq</a></li>
                        <li><a href="<?=base_url('help-and-support')?>">Help & Support</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="<?=base_url('privacy-policy')?>">Privacy Policy</a></li>
                        <li><a href="<?=base_url('terms-and-conditions')?>">Terms & Conditions</a></li>
                        <li><a href="<?=base_url('mission-and-vision')?>">Mission & Vission</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Follow Us</h3>
                    <ul class="footer-social-link">
                    	<?php $social_media_link = get_social_links(); ?>
		                
		                
                        <li><a href="<?= $social_media_link !=null ? $social_media_link['facebook_link'] : '';?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?= $social_media_link !=null ? $social_media_link['twiter_link'] : '';?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?= $social_media_link !=null ? $social_media_link['google_plus_link'] : '';?>"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="<?= $social_media_link !=null ? $social_media_link['link_in_link'] : '';?>"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="copyright-area">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <p>&copy; copyright <?=date('Y')?> All Right Reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>
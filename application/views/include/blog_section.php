<section id="blog" class="section-padding gray-bg">
  <div class="container">
    <?php 
        $ci = & get_instance();
        $link = $ci->router->fetch_class();
        if(isset($link) && $link =='home'){ ?>
          <div class="section-header text-center">
            <div class="section-title">Banking News</div>
            <h2>Latest Finance and Business News from Loan eCapical</h2>
          </div>
        <?php 
        }
    ?>
    <div class="row">
      <?php
        if (isset($blog_details) && $blog_details !=null) {
          // echo "<pre>";print_r($blog_details);exit;
          
          foreach ($blog_details as $key => $value) { ?>  
            <article class="col-lg-4">
              <div class="post-wrap">
                <div class="post-img"> 
                  <a href="<?=base_url('blog_detail/'.$value['seo_slug'])?>">
                    <img src="<?= base_url(BLOG_IMAGE.$value['image'])?>" alt="image">
                  </a> 
                </div>
                <h4><a href="#"><?=$value['title']?></a></h4>
                <div class="post-meta">
                  <ul>
                    <li><a href="#"><?=format_date_mdy($value['created_at'])?></a></li>
                    <li>By <a href="#"><?=$value['author']?></a></li>
                  </ul>
                </div>
                <p><?=substr(strip_tags($value['description']), 0,120)?></p>
              </div>
            </article>
            <?php 
          }
        }
      ?>
    </div>
  </div>
</section>
<!-- /Blog-->
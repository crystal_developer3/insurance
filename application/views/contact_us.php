<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg3">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Contact Us</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>Contact Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Area -->
    <section class="contact-area ptb-100">
        <div class="container">
            <div class="section-title">
                <h2>Drop us Message for any Query</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="contact-form">
                        <form id="form" method="post">
                            <div class="row">
                                <?php $this->load->view('include/messages');?>
                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Name *">
                                        <?= form_error("name", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email *">
                                        <?= form_error("email", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile number *">
                                        <?= form_error("mobile_number", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" id="message" cols="30" rows="6" placeholder="Your Message *"></textarea>
                                        <?= form_error("message", "<label class='error'>", "</label>");?>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="default-btn check">Send Message <span></span></button>
                                    <!-- <div id="msgSubmit" class="h3 text-center hidden"></div> -->
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="basic-info f1 mt-50 clearfix">
                        <h2 class="c-form-title text-uppercase c-4 mb-50">find us here</h2>
                        <div class="contact-widget-info c-page">
                            <div class="contact-info-2">
                                <ul>
                                    <li>
                                        <h5><span class="fa fa-map-marker" aria-hidden="true"></span> Address</h5>
                                        <p><?=nl2br(get_main_address())?></p>
                                    </li>
                                    <li>
                                        <h5><span class="fa fa-phone" aria-hidden="true"></span> Phone</h5>
                                        <p><?=get_main_phone_no1()?></p>
                                    </li>
                                    <li>
                                        <h5><span class="fa fa-envelope" aria-hidden="true"></span> Email</h5>
                                        <p><?=get_main_email_address()?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="bg-map"><img src="<?=base_url('assets/img/bg-map.png')?>" alt="image"></div>
    </section>
    <!-- End Contact Area -->
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
    <script>
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                'name': {required: true}, 
                'email': {required: true,email:true}, 
                'mobile_number': {required: true,minlength:10}, 
                'message': {required: true}, 
            },
            messages: {
                'name': {required:"Please enter name"}, 
                'email': {required:"Please enter email",email:"Please enter valid email"}, 
                'mobile_number': {required:"Please enter mobile number",minlength:"Enter minimum 10 digits mobile number"}, 
                'message': {required:"Please type your message"}, 
            }
        });  
        var form = $( "#form" );
        form.validate();

        $(document).on('click','.check',function(e){
          if(!form.valid()){
            $("#form").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass();
            });
          }
        });
     
    </script>
</body>
</html>
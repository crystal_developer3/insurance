<?php
	function static_page($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('static_page','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
?>
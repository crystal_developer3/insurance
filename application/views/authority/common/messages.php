<!-- jQuery -->
<script src="<?= base_url()?>assets/authority/js/jquery.min.js"></script>
<?php
    $success = $this->session->flashdata('success');
    $error = $this->session->flashdata('error');                    
    if (isset($success))
    {
        ?>  
            <script>
                $(function() {
                    toastr.success('<?= $success;?>');
                });
            </script>
        <?php
    }                            
    elseif(isset($error))
    {
        ?>
            <script>
                $(function() {
                    toastr.error('<?= $error;?>');
                });
            </script>
        <?php          
    }
?> 

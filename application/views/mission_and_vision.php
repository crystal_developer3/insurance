<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2><?=(isset($mission_details) && !empty($mission_details))?$mission_details[0]['title']:''?> & <?=(isset($vision_details) && !empty($vision_details))?$vision_details[0]['title']:''?></h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li><?=(isset($mission_details) && !empty($mission_details))?$mission_details[0]['title']:''?> & <?=(isset($vision_details) && !empty($vision_details))?$vision_details[0]['title']:''?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Area -->
    <section class="events-area ptb-100 pb-70">
        <div class="container">
            <?php 
                if(isset($mission_details) && !empty($mission_details)){?>
                    <div class="single-events-box">
                        <div class="events-box">
                            <div class="events-image">
                                <div class="image bg1" style="background-image:url(<?=base_url(STATIC_PAGE_IMAGE.$mission_details[0]['image'])?>)">
                                    <img src="<?=base_url(STATIC_PAGE_IMAGE.$mission_details[0]['image'])?>" alt="image">
                                </div>
                            </div>

                            <div class="events-content">
                                <div class="content">
                                    <h3><?=$mission_details[0]['title']?></h3>
                                    <p><?=$mission_details[0]['description']?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            ?>
            
            <?php 
                if(isset($vision_details) && !empty($vision_details)){?>
                    <div class="single-events-box">
                        <div class="events-box">
                            <div class="events-image">
                                <div class="image bg2" style="background-image:url(<?=base_url(STATIC_PAGE_IMAGE.$vision_details[0]['image'])?>)">
                                    <img src="<?=base_url(STATIC_PAGE_IMAGE.$vision_details[0]['image'])?>" alt="image">
                                </div>
                            </div>

                            <div class="events-content">
                                <div class="content">
                                    <h3><?=$vision_details[0]['title']?></h3>
                                    <p><?=$vision_details[0]['description']?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </section>
    <!-- End Contact Area -->
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>
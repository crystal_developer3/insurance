<?php
if (!function_exists('year')) {
	function year($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('year','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('year_front')) {
	function year_front($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('year','id','asc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_year_title')) {
		function get_year_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('year', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('make')) {
	function make($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('make','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_make_title')) {
		function get_make_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('make', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}
if (!function_exists('models')) {
	function models($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('models','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
}
if (!function_exists('get_models_title')) {
		function get_models_title($id) {
		    $CI = & get_instance();
		    $conditions = array("where"=>array("id"=>$id));
		    $info = $CI->common_model->select_data('models', $conditions);
		    // echo "<pre>";print_r($info);exit;
		    if ($info['row_count'] > 0) {
		        return $info['data'][0]['title'];
		    } else {
		        return '';
		    }
		}
	}

?>
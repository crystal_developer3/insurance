<section class="services-boxes-area bg-f8f8f8">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-box">
                    <div class="image">
                        <img src="<?= base_url()?>assets/img/featured-services-image/1.jpg" alt="image">
                    </div>

                    <div class="content">
                        <h3><a href="#">Shop Multiple Companies</a></h3>
                        <p>Lorem ipsum dolor sit amet, adipiscing consectetur gravida elit</p>

                        <div class="icon">
                            <img src="<?= base_url()?>assets/img/icon1.png" alt="image">
                        </div>
                        <div class="shape">
                            <img src="<?= base_url()?>assets/img/umbrella.png" alt="image">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-box">
                    <div class="image">
                        <img src="<?= base_url()?>assets/img/featured-services-image/2.jpg" alt="image">
                    </div>

                    <div class="content">
                        <h3><a href="#">Get The Best Price</a></h3>
                        <p>Lorem ipsum dolor sit amet, adipiscing consectetur gravida elit</p>
                        
                        <div class="icon">
                            <img src="<?= base_url()?>assets/img/icon2.png" alt="image">
                        </div>
                        <div class="shape">
                            <img src="<?= base_url()?>assets/img/umbrella.png" alt="image">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                <div class="single-box">
                    <div class="image">
                        <img src="<?= base_url()?>assets/img/featured-services-image/3.jpg" alt="image">
                    </div>

                    <div class="content">
                        <h3><a href="#">Local Service</a></h3>
                        <p>Lorem ipsum dolor sit amet, adipiscing consectetur gravida elit</p>
                        
                        <div class="icon">
                            <img src="<?= base_url()?>assets/img/icon3.png" alt="image">
                        </div>
                        <div class="shape">
                            <img src="<?= base_url()?>assets/img/umbrella.png" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
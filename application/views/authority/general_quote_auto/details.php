<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('authority/dashboard');?>">Home</a></li>
                        <li class="breadcrumb-item active">Quote</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <?php $this->load->view('authority/common/messages');?>
            <div class="row">
                
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($name) && $name !=null){ ?>
                                                                <tr>
                                                                    <td><b>Name:</b></td>
                                                                    <td><?= $name?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                        
                                                        <?php
                                                            if(isset($email) && $email !=null){ ?>
                                                                <tr>
                                                                    <td><b>Email Address:</b></td>
                                                                    <td><?=$email?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($phone_number) && $phone_number !=null){ ?>
                                                                <tr>
                                                                    <td><b>Mobile Number:</b></td>
                                                                    <td><?=$phone_number?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($address) && $address !=null){ ?>
                                                                <tr>
                                                                    <td><b>Address:</b></td>
                                                                    <td><?= isset($address) && $address !=null ? $address : '';?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($preferred_language) && $preferred_language !=null){
                                                                $preferred_language = get_language_titles($preferred_language);               
                                                                ?>
                                                                <tr>
                                                                    <td><b>Preferred language:</b></td>
                                                                    <td><?= $preferred_language?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <?php
                                                            if(isset($image) && $image !=null){
                                                                      
                                                                 $file_extension = strtolower(substr(strrchr($image, "."), 1)); 
                                                                 $docexts = array("pdf","csv","txt","html","doc","docz","xls","xlsx","ppt","pptx"); 
                                                                 $imgexts = array("png","jpeg","jpg");        
                                                                ?>
                                                                <tr>
                                                                    <td><b>Document:</b></td>
                                                                    <td>
                                                                        <?php if (in_array($file_extension, $imgexts)){ ?>
                                                                            
                                                                            <div class="text-center">
                                                                                <a href="<?= base_url(GENERAL_QUOTE_DOCS).$image?>">
                                                                                    <img class="profile-user-img img-fluid img-circle" src="<?= base_url(GENERAL_QUOTE_DOCS).$image?>" alt="profile picture">

                                                                                </a>
                                                                            </div>
                                                                        <?php }elseif (in_array($file_extension, $docexts)) { ?>
                                                                                <a href="<?= base_url(GENERAL_QUOTE_DOCS).$image?>">Open Document</a>
                                                                            <?php
                                                                        } ?>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($selected_item) && $selected_item !=null){              
                                                                ?>
                                                                <tr>
                                                                    <td><b>Selected item:</b></td>
                                                                    <td><?= $selected_item?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                        <?php
                                                            if(isset($created_at) && $created_at !=null){ ?>
                                                                <tr>
                                                                    <td><b>Contact date:</b></td>
                                                                    <td><?= format_date_Mdy_time($created_at)?></td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <h3>vehicals</h3>
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 10px">No</th>
                                                            <th style="width: 20%;">Year</th>
                                                            <th style="width: 20%;">Make</th>
                                                            <th style="width: 25%;">Model</th>
                                                            <th style="width: 25%;">VIN</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody class="data-response">
                                                        <?php
                                                            if (isset($vehical_data) && $vehical_data !=null){
                                                                foreach ($vehical_data  as $key => $value) {
                                                                    $id = $value['id'];
                                                                ?>
                                                                    <tr>
                                                                        <td><?= $key+1;?></td>
                                                                        <td><?= get_year_title($value['year_id']);?></td>
                                                                        <td><?= get_make_title($value['make_id']);?></td>
                                                                        <td><?= get_models_title($value['models_id']);?></td>
                                                                        <td><?= $value['vin'];?></td>
                                                                    </tr>
                                                                <?php } 
                                                            }
                                                            else{
                                                                ?>
                                                                    <tr data-expanded="true">
                                                                        <td colspan="7" align="center">Records not found</td>
                                                                    </tr>
                                                                <?php
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary card-outline">                                    
                        <!-- /.card-header -->
                        <div class="card-body" style="padding-bottom: 0.20rem;">
                            <div class="tab-content">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body table-responsive no-padding">
                                                <table id="mytable" class="table table-bordred">
                                                    <tbody>
                                                        <tr>
                                                            <td><b>Do you currently have auto insurance? :</b></td>
                                                            <td><?=($have_auto_insurance !=null)?(($have_auto_insurance)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td><b>Accidents (regardless of fault) in the last 5 years? :</b></td>
                                                            <td><?=($accidents_in_last_five_years !=null)?(($accidents_in_last_five_years)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td><b>Accidents (regardless of fault) by anyone who was not listed on your insurance policy in the last 5 years? :</b></td>
                                                            <td><?=($accidents_by_anyone_who_was_not_listed !=null)?(($accidents_by_anyone_who_was_not_listed)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td><b>Traffic Tickets in the last 5 years? :</b></td>
                                                            <td><?=($traffic_tickets_in_five_years !=null)?(($traffic_tickets_in_five_years)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td><b>DUIs in the last 10 years? :</b></td>
                                                            <td><?=($duis_in_ten_years !=null)?(($duis_in_ten_years)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                              
                                                        <tr>
                                                            <td><b>Suspensions or Revocations in the last 10 years? :</b></td>
                                                            <td><?=($suspensions_or_revocations_in_ten_years !=null)?(($suspensions_or_revocations_in_ten_years)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                                        
                                                
                                                        <tr>
                                                            <td><b>Have any drivers had any of the following? :</b></td>
                                                            <td><?=($hav_any_of_the_following !=null)?(($hav_any_of_the_following)?'Yes':'No'):'Not attampt'?></td>
                                                        </tr>
                                                          
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>
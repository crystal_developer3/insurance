<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body class="tg-home tg-homeone">
    <!--Wrapper Start-->
    <section class="hero-section full-screen gray-light-bg">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-md-7 col-lg-6 col-xl-8 d-none d-lg-block">
                    <div class="bg-cover vh-100 ml-n3 gradient-overlay" style="background: url(<?=base_url('assets/img/achievements-img2.jpg')?>);">
                        
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div class="contact-form">
                        <h2 class="text-center pt-4">Verify OTP</h2>
                        <?php $this->load->view('authority/common/messages');?>
                        <form id="form" method="post" action="">
                            <fieldset>
                                <input type="hidden" name="user_id" value="<?=(isset($user_id) && !empty($user_id))?$user_id:''?>">
                                <div class="form-group">
                                        <label>OTP <span>*</span></label>
                                        <input type="otp" name="otp" id="otp" placeholder="Enter OTP" class="form-control">
                                 </div>   
                                <div class="col-lg-12 col-md-12 text-center">
                                    <button type="submit" class="default-btn">Go</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>    
                </div>
            </div>
        </div>
    </section>
      
        
    
<?php $this->load->view('include/footer_js');?>
<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            'otp': {required: true}, 
        },
        messages: {
            'otp': "Please enter otp", 
        }
    });    
</script>
</body>
</html>
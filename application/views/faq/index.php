<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <?php $this->load->view('include/header_js');?>  
</head>

<body>
    <?php $this->load->view('include/header');?>
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-bg2">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Frequently Asked Questions</h2>
                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li>
                            <li>FAQ</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Area -->
    <section class="faq-area ptb-20">
        <div class="container p-0">
            <div class="row m-0">
                <div class="col-lg-12 col-md-12 p-0">
                    <div class="faq-accordion">
                        <h2>Get Every Single Answers There if you want</h2>
                        <div class="panel-group" id="accordionSingle" role="tablist" aria-multiselectable="true">
                            <?php
                                if (isset($details) && $details !=null) {
                                    $i = 0;
                                    foreach ($details as $key => $value) { ?> 
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading<?=$i?>">
                                              <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" href="#collapseItemOpen<?=$i?>" aria-expanded="false" aria-controls="collapseItemOpen<?=$i?>" class="collapsed">
                                                  <?=$value['question']?>
                                                </a>
                                              </h4>
                                            </div>
                                            <div id="collapseItemOpen<?=$i?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$i?>">
                                              <div class="panel-body">
                                                <?=$value['answer']?>
                                              </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                }
                            ?>

                            <!-- <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapseItemOpenTwo" aria-expanded="false" aria-controls="collapseItemOpenOne" class="collapsed">
                                      Collapsible Single Item Open on Load
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseItemOpenTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                    raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                  </div>
                                </div>
                            </div>
                          
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapseItemOpenThree" aria-expanded="false" aria-controls="collapseItemOpenOne" class="collapsed">
                                      Collapsible Single Item Open on Load
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseItemOpenThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                    raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                  </div>
                                </div>
                            </div>
                          
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapseItemOpenFour" aria-expanded="false" aria-controls="collapseItemOpenOne" class="collapsed">
                                      Collapsible Single Item Open on Load
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseItemOpenFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                    raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                  </div>
                                </div>
                            </div>
                              
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#collapseItemOpenFive" aria-expanded="false" aria-controls="collapseItemOpenOne" class="collapsed">
                                      Collapsible Single Item Open on Load
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseItemOpenFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                    on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                    raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                  </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Area -->
    <?php $this->load->view('include/footer');?>
    <?php $this->load->view('include/footer_js');?>
</body>
</html>
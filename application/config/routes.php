<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['authority'] = 'authority/login';
$route['authority/login'] = 'authority/login';
$route['authority/login/forgot-password'] = 'authority/login/forgot_password';
/* ACCOUNT */
$route['authority/account/change-password'] = 'authority/account/changepassword';

$route['blog_detail/(:any)'] = 'blog/blog_details/$1';
$route['agent-details/(:any)'] = 'agent_details/index/$1';
$route['authority/reviews/delete'] = 'authority/reviews/delete';
$route['authority/reviews/(:any)'] = 'authority/reviews/index/$1';
$route['quote-form/free-quote'] = 'quote_form/free_quote';
$route['quote_form/free_quote'] = 'quote_form/free_quote';
$route['quote-form/(:any)'] = 'quote_form/index/$1';
$route['quote_form/(:any)'] = 'quote_form/index/$1';
$route['general-quote-form/(:any)'] = 'general_quote_form/index/$1';
$route['general-contact-form/(:any)'] = 'general_contact_form/index/$1';
$route['general_quote_form_auto/get_make_list'] = 'general_quote_form_auto/get_make_list';
$route['general_quote_form_auto/get_models_list'] = 'general_quote_form_auto/get_models_list';
$route['general-quote-form-auto/second'] = 'general_quote_form_auto/second';
$route['general_quote_form_auto/(:any)'] = 'general_quote_form_auto/index/$1';
$route['general-quote-form-auto/(:any)'] = 'general_quote_form_auto/index/$1';





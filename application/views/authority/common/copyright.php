<footer class="main-footer">
    <strong>Copyright &copy; <?= date("Y"); ?> <a href="<?= site_url(); ?>authority/dashboard"><?= SITE_TITLE; ?></a>.</strong> All rights
    reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="<?= base_url()?>assets/authority/js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url()?>assets/authority/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url()?>assets/authority/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url()?>assets/authority/js/demo.js"></script>
<script src="<?= base_url()?>assets/authority/js/bs-custom-file-input.min.js"></script>
<!-- Toastr -->
<script src="<?= base_url()?>assets/authority/js/toastr.min.js"></script>
<!-- Summernote -->
<script src="<?= base_url()?>assets/authority/js/summernote-bs4.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url()?>assets/authority/js/select2.full.min.js"></script>

<!-- New added -->
<script type="text/javascript" src="<?= base_url(); ?>assets/authority/js/address.js"></script>
<script src="<?= base_url(); ?>assets/validation/jquery.validate.js"></script>
<script src="<?= base_url(); ?>assets/validation/common.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<script>
	$(document).ready(function() {
        bsCustomFileInput.init();
        $('.select2').select2();        
		$('.loading').css({'display':'none'});
        // $(".filter_date").datepicker({
        //     dateFormat: 'dd-mm-yy',
        //     changeMonth: true,
        //     changeYear: true,
        //     todayHighlight: true,
        //     autoclose: true, 
        // });
        // $(".current_date").datepicker({
        //     dateFormat: 'dd-mm-yy',
        //     changeMonth: true,
        //     changeYear: true,
        //     todayHighlight: true,
        //     autoclose: true, 
        //     minDate: 0,
        // });
        $('#select_all').on('click', function(e) {
            if($(this).is(':checked',true)) {
                $(".chk_all").prop('checked', true);
            }
            else {
                $(".chk_all").prop('checked',false);
            }
        });  

        $(document).on('change','#year_id',function(){
            var year_id = $(this).val();
            $.ajax({
              type: "POST",
              data:{year_id:year_id},
              url: BASE_URL + 'authority/make/get_make_list',
              async: false,
              success: function (response) {
                var response = JSON.parse(response);
                if (response.Success) {
                    $('#make_id').find('option').remove();
                    $.each(response.data, function(i, value) {
                        $('#make_id').append($('<option>').text(value.title).attr('value', value.id));
                    });
                 }else{
                    // $(function() {
                    //     toastr.options = {
                    //     positionClass : "toast-bottom-right"
                    //     };
                    //     toastr.error(response.message);
                    //     setTimeout(function() {
                    //       window.location.reload(); 
                    //     }, 1000);
                    // });
                }
              },
            });
        });
    });
</script>
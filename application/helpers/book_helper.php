<?php
	function books($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where('books','id','desc',$where);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
	function books_front_home($where = array()){
		$CI = & get_instance();
		$info = $CI->production_model->get_all_with_where_limit('books','id','desc',$where,4,0);
		if (isset($info) && $info !=null) {
			return $info;
		}
		else{
			return array();
		}
	}
	
?>